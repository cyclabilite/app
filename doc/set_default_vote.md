# Mettre à jour le vote par défaut

Voici comment mettre à jour le vote par défaut. Le vote par défaut est configuré par un script `rules_to_apply.py`

## Envoyer le script `rules_to_apply.py` sur le serveur

```
ordi__$ scp /chemin_sur_ordi/rules_to_apply.py user@vcyclabilites.champs-libres.coop:/chemin_sur_serveur/
```

où 

- `user`: votre nom d'utilisateur sur le serveur
- `/chemin_sur_ordi/rules_to_apply.py`: le chemin sur votre ordi de votre script `rules_to_apply.py`
- `/chemin_sur_serveur/`: le chemin où vous voulez envoyer le fichier

## Se connecter au server

La connection se fait via `ssh`.

```
ordi__$ ssh user@vcyclabilites.champs-libres.coop
```

## Aller dans le dossier `docker-compose` de l'installation

Pour l'ADAV :

```
serveur$ cd docker-compose/cyclab-adav
```

## Vérifier que le fichier `docker-compose.yml` soit bien configuré

Le fichier `docker-compose.yml` paramètre les différents conteneur `docker`. De ce fichier, vous devez éditer la sous-partie `volumes` de la  partie `scripts`. Il faut que votre fichier `rules_to_apply.py` soit bien monté à l'emplacement `scripts/set_default_vote/rules_to_apply.py`.

``` yml
    scripts:
        image: cyclabnpc_scripts
        links:
          - db
        volumes:
          - chemin_serveur_vers/rules_to_apply.py:/scripts/set_default_vote/rules_to_apply.py
          - ./data:/osm/data
```

## Lancer le conteneur `scripts`

```
serveur$ docker-compose run scripts bash
```

## Dans le cconteneur lancer la procedure

```
scripts$ cd /scripts/set_default_vote/
scripts$ python exe.py
```