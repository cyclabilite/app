# Recalcul du vote (selon le mode choisi)

## Se connecter au server

La connection se fait via `ssh`.

```
ordi__$ ssh user@vcyclabilites.champs-libres.coop
```

## Aller dans le dossier `docker-compose` de l'installation

Pour l'ADAV :

```
serveur$ cd docker-compose/cyclab-adav
```

## Rentrer dans le conteneur `web` et y lancer la commande Symfony `vote:recompute-avg`

```
serveur$ docker-compose exec web bash
cnt_web$ php -d max_execution_time=600 -d memory_limit=-1  bin/console   vote:recompute-avg
```