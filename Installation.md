Memo about installation
========================

Database Migration
-------------------

If you get "SQLSTATE[42704]: Undefined object: 7 ERROR: type "hstore" does not exist at character 478"

Please create extension Hstore as superuser before running the migrate script :

```sql
CREATE EXTENSION hstore;
```

