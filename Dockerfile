FROM php:7.4-fpm

MAINTAINER <info@champs-libres.coop>

RUN apt-get update && apt-get -y install postgresql-server-dev-all zip git

RUN docker-php-ext-install pdo_pgsql
RUN docker-php-ext-install opcache

RUN apt-get -y install libonig-dev

RUN apt-get -y install libicu-dev

RUN docker-php-ext-install intl

RUN apt-get update &&  apt-get -y install libxml2 libxml2-dev

RUN docker-php-ext-install xml

# remove unused packages
RUN apt-get remove -y libicu-dev \
   && apt-get autoremove -y

# php configuration
RUN printf "\n[Date]\ndate.timezone = Europe/Brussels\n" >> /usr/local/etc/php/php.ini
RUN printf "\nping.path = /ping\npm.status_path = /status\n" >> /usr/local/etc/php-fpm.conf

WORKDIR /var/www/cyclab
COPY . /var/www/cyclab

RUN echo "Copy parameters"
ADD _docker/cyclab_app/parameters.yml /var/www/cyclab/app/config/parameters.yml
ADD _docker/cyclab_app/parameters.php /var/www/cyclab/app/config/parameters.php

RUN echo "Install composer"
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN echo "Do composer install"
# RUN composer install --no-scripts

# ADD _docker/cyclab_app/entrypoint.sh /entrypoint.sh
# RUN chmod u+x /entrypoint.sh

# VOLUME ["/var/www/cyclab/web/js", "/var/www/cyclab/web/css", "/var/www/cyclab/web/bundles", "/var/www/cyclab/app/logs" , "/var/www/cyclab/app/config/", "/var/www/cyclab/web/help", "/var/www/cyclab/web/db_dump"]

# CMD ["/entrypoint.sh"]
