Cyclabilité
===========

Concept
-------

Logiciel qui sert à "noter" la "cyclabilité" des rues. La cyclabilité est une note qui indique si une rue est adaptée à l'usage du vélo. Cette notion est subjective. Les cyclistes sont classés afin d'adapté la notion en fonction du type de cycliste.


Besoin d'aide ?
---------------

- envoyer des emails (pour nos contacts voir le site de Champs-Libres)
- téléphoner (idem voir le site de Champs-Libres)
- rapport des bugs : https://framagit.org/Cyclabilite/app/issues
- tchatter avec nous sur riot : https://riot.im/app/#/room/#cyclabilite:matrix.org


Installation
------------

Vous pouvez installer à l'aide de docker-compose ( https://docs.docker.com/compose/ ) ou
sans. Faites l'un ou l'autre mais pas les deux. On vous conseille d'utiliser docker-compose.


Installation avec docker-compose
================================

Lancer la db :

```
$ docker-compose up db
```

Charger le conteneur de scripts. Ce conteneur permet de lancer le script
qui initialise la db. Il contient l'outil osmosis, les scripts import.py
(qui permet d'initialiser les donnes) et le script update.py qui permet
de mettre à jour les données à partir d'un nouvel export osm.

```
$ docker-compose run --rm scripts /bin/bash
```

Dans ce container vous pouvez utiliser osmosis, si vous voulez faire ne
charger qu'une zone de la carte. Le fichier `.osm.pbf` doit se trouver
dans le répertoir `/fixtures/`


```
$ osmosis --read-pbf file=belgium-latest.osm.pbf --bb left=4.48 right=4.72 top=50.7575 bottom=50.58 completeWays=yes --write-pbf file=lln.osm.pbf
```

Attention à bien mettre completeWays=yes sinon lors de l'import ça va foirer.

```
# Lancer import
$ cd /scripts/imports
$ python import.py lln.osm.pbf
```

Installation sans docker-compose
-------------

*Minimum requirements*

- A postgresql >= 9.1 + postgis >= 2.0 database
- php 5.5
- an Unix system (Linux, Mac Os)


Note that we never test the installation on Windows.
If you try and manage to install the project on Windows please give us a feedback.

*Prepare the database*

Create a postgresql database. If you want to call the db `cyclab_db` and the db user `cyclab_user` :

```bash
createuser -P cyclab_user
createdb uello_db -E UTF8 -O cyclab_user
```

Enable hstore:


```sql
CREATE EXTENSION hstore;
```

Enable postgis:

```sql
CREATE EXTENSION postgis;
```

Give the property of the tables `geometry_columns` and `spatial_ref_sys` to the user that will use the db.
If this user is `cyclab_user`:

```sql
ALTER TABLE geometry_columns OWNER TO cyclab_user;
ALTER TABLE spatial_ref_sys OWNER TO cyclab_user;
```

*Install the app and dependencies*

```bash
git clone git@git.champs-libres.coop:Cyclabilite.git # use the stable branch for test and prod
cd Cyclabilite
#get composer.phar
curl -sS https://getcomposer.org/installer | php
#install symfony + dependencies
php composer.phar install
```

At the end of the install's process, you will be prompted for the following informations :

```yaml
parameters:
    database_driver:   pdo_pgsql
    database_host:     127.0.0.1
    database_port:     5432
    database_name:     databasename
    database_user:     username
    database_password: password

    mailer_transport:  smtp
    mailer_host:       127.0.0.1
    mailer_user:       ~
    mailer_password:   ~

    locale:            fr
    secret:            ThisTokenIsNotSoSecretChangeIt

    persona_audience: localhost
```

In case of error you can edit the parameters file `/app/config/parameters.yml`.

*Prepare the database'schema*

```sh
php app/console doctrine:migrations:migrate
```

*Import OSM data to the db*

```bash
cd import/Cyclabilite
python import.py
```

créer le fichier pbf :
osmosis --read-pgsql  database=test_osmosis user=osm password=osm outPipe.0=pg --dd inPipe.0=pg outPipe.0=dd --write-xml inPipe.0=dd file=test.osm


*Configure web server and permissions*

The web server should point to the `Cyclabilite/web` directory. For test and prod environments, the software will be reachable by running `http://localhost/path/to/Cyclabilite/web/app_dev.php`.

You may also use the embedded php server : `php app/console server:run`.

The directories `app/cache` and `app/logs` must be writeable by the apache user *AND* the user you will use to run php app/console commands.

See [the "setting up permissions" in the Symfony documentation](http://symfony.com/doc/current/book/installation.html#configuration-and-setup) for doing this.

Development
============

Tests
-----

Some tests are available :
- import / Cyclabilite / set_default_vote_test.py
  - test some functions of the set_default_vote.py
  - run : `python set_default_vote_test.py`
- src / CL / Cyclabilite / VoteBundle / Tests
  - warning curl must be enable for php :
    - `sudo apt-get install php5-curl`
  - test for the symfony app
  - run : `php bin/phpunit -c app`

CSS / JS
--------

For compiling CSS (SASS) and JS :

```
yarn run encore dev --watch
```


Serveur smtp for Dev
====================

```
$ sudo python -m smtpd -n -c DebuggingServer localhost:25
```

Docker image
============

(seul la doc avec docker-compose est mise à jour)

Construire les images :
- docker build -t cyclab-app . (dans /)
- docker build -t cyclab-db . (dans /\_docker/cyclab_db))
- docker build -t cyclab-db-dump . (dans /\_docker/cyclab_db_dump)

Lancer les images :
- docker run -d -P --name cyclab-db cyclab-db
- docker run -d --link cyclab-db:db --name cyclab-app --env "ADMIN_PASSWORD=pass"  --env "SECRET=i_am_not_secret"  cyclab-app
- docker run -d --link cyclab-db:db --name cyclab-app --env "ADMIN_PASSWORD=pass"  --env "SECRET=i_am_not_secret" --env "AVG_CLASS=CL\Cyclabilite\VoteBundle\ComputeAverage\ComputeExpertAverage" --env "AVG_CLASS_ARGS=[false, false]" cyclab-app

- docker run -d -p "8000:80" -v `pwd`/_docker/nginx/nginx.conf:/etc/nginx/nginx.conf  --link cyclab-app:web --volumes-from cyclab-app  --name cyclab-nginx nginx
- docker run --volumes-from cyclab-app --link cyclab-db:db  --name cyclab-db-dump cyclab-db-dump

Serveur :

$ docker volume create --name pgsql_data2
$ docker run -v pgsql_data2:/pgsql_data --env PGDATA=/pgsql_data -d -P --name cyclab-db2 mdillon/postgis
$ docker run -d --link cyclab-db2:db --name cyclab-app2 --env "ADMIN_PASSWORD=pass"  --env "SECRET=i_amsdklsdlkret" --env "AVG_CLASS=CL\Cyclabilite\VoteBundle\ComputeAverage\ComputeExpertAverage" --env "AVG_CLASS_ARGS=[false, false]" cyclabilite/app
$ docker run -d --link cyclab-app2:fpm --name cyclab-nginx2 -p 80:80 -v /home/ubuntu/cyclab-nginx/nginx.conf:/etc/nginx/nginx.conf --volumes-from cyclab-app2 nginx
$ docker run -d --volumes-from cyclab-app2 --link cyclab-db2:db  --name cyclab-db-dump2 cyclabilite/db-dump
<s

Rules to apply
===============

docker-compose run --rm  scripts python /scripts/set_default_vote/exe.py

(+ faire un volumes pour rules_to_apply.py)


Mettre en production 
====================

Vous devez mettre sur le serveur les images de `ẁeb` (a le php) et de `nginx` (a le css / js).

Pour l'image `web`
-----------------

Le repository proposé par gitlab est utilisé.

Faire un `docker-compose build` qui créear l'image `registry.gitlab.com/cyclabilite/app`

Ensuite faire

```
$ docker login registry.gitlab.com
$ docker push registry.gitlab.com/cyclabilite/app
```

Mettre à jour vos containeurs :

```
docker-compose up -d
```

Pour l'image `nginx`
--------------------

Pour cela faire un `docker-compose build` ca créera les images ``cyclab-npc_nginx``

Ensuite sauvegarder l'image de la manière suivante :

```
docker save cyclab-npc_nginx > /tmp/img_cyclab_npc_nginx
```

Envoyer le fichier de l'image (`/tmp/img_cyclab_npc_nginx`) sur le serveur à l'aide de scp

Charger l'image :

```
docker load < /tmp/img_cyclab_npc_nginx
```

Mettre à jour vos containeurs :

```
docker-compose up -d
```