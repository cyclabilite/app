
CREATE SEQUENCE nodes_seq START 1;

CREATE TABLE nodes (
   id                integer PRIMARY KEY DEFAULT nextval('nodes_seq'),
   id_osm            integer NOT NULL,
   in_use            boolean,
   osmosis_data_1    char(12),
   osmosis_data_2    char(12)
);

CREATE SEQUENCE ways_seq START 1;

CREATE TABLE ways (
   id                integer PRIMARY KEY DEFAULT nextval('ways_seq'),
   id_osm            integer NOT NULL,
   in_use            boolean,
   name    	     char(255),
   highway           char(255)
);

CREATE TABLE way_nodes (
   node_id	     integer REFER

CREATE SEQUENCE segments_seq START 1;

CREATE TABLE segments (
   id             integer PRIMARY KEY DEFAULT nextval('segments_seq'),
   way_id        integer NOT NULL,
   node_start_id   integer NOT NULL,
   node_end_id    integer NOT NULL
);

CREATE SEQUENCE intersection_paths_seq START 1;

CREATE TABLE intersection_paths (
   id             integer PRIMARY KEY DEFAULT nextval('segments_seq'),
   node_id        integer NOT NULL,
   segment_start_id   integer NOT NULL,
   segment_stop_id    integer NOT NULL
);
