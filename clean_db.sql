DROP TABLE IF EXISTS nodes, ways, segments, intersection_path;
DROP SEQUENCE IF EXISTS nodes_seq, ways_seq, segments_seq, intersection_path_seq;