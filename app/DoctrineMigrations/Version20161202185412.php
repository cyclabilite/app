<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161202185412 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE cyclab_profile DROP CONSTRAINT fk_ae5f597a76ed395');
        $this->addSql('ALTER TABLE cyclab_profile ADD CONSTRAINT FK_C5E94D29A76ED395 FOREIGN KEY (user_id) REFERENCES cyclab_fos_users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE cyclab_profile DROP CONSTRAINT FK_C5E94D29A76ED395');
        $this->addSql('ALTER TABLE cyclab_profile ADD CONSTRAINT fk_ae5f597a76ed395 FOREIGN KEY (user_id) REFERENCES cyclab_users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
