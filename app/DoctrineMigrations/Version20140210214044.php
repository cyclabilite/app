<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * This create the basic schema for :
 * - cyclab_segments
 * - cyclab_intersection_path
 * - cyclab_intersections
 */
class Version20140210214044 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != "postgresql",
            "Migration can only be executed safely on 'postgresql'."
        );

        //create sequences
        $this->addSql(""
            . "CREATE SEQUENCE cyclab_intersection_paths_id_seq "
            ."INCREMENT BY 1 MINVALUE 1 START 1");
        $this->addSql(""
            . "CREATE SEQUENCE cyclab_segments_id_seq "
            . "   INCREMENT BY 1 MINVALUE 1 START 1");

            //create table intersection_paths
        $this->addSql(""
            . "CREATE TABLE cyclab_intersection_paths ("
            . "id BIGINT NOT NULL DEFAULT nextval('cyclab_intersection_paths_id_seq'), "
            . "node_id BIGINT DEFAULT NULL, "
            . "segmentStart_id BIGINT DEFAULT NULL, "
            . "segmentEnd_id BIGINT DEFAULT NULL, "
            . "vote_default integer DEFAULT 3,"
            . "vote_average float DEFAULT NULL,"
            . "vote_number integer DEFAULT 0 NOT NULL,"
            . "PRIMARY KEY(id))");
        $this->addSql("CREATE INDEX IDX_28646481460D9FD7 ON cyclab_intersection_paths (node_id)");
        $this->addSql("CREATE INDEX IDX_28646481B07A3B52 ON cyclab_intersection_paths (segmentStart_id)");
        $this->addSql("CREATE INDEX IDX_286464817A2CED74 ON cyclab_intersection_paths (segmentEnd_id)");

        //create table segments
        $this->addSql(""
            . "CREATE TABLE cyclab_segments ("
            . "id BIGINT NOT NULL DEFAULT nextval('cyclab_segments_id_seq'),"
            . "way_id BIGINT DEFAULT NULL, "
            . "nodeStart_id BIGINT DEFAULT NULL, "
            . "nodeEnd_id BIGINT DEFAULT NULL, "
            . "geom geometry(LineString,4326), "
            . "vote_default integer DEFAULT 3,"
            . "vote_average float DEFAULT NULL,"
            . "vote_number integer DEFAULT 0 NOT NULL,"
            . "PRIMARY KEY(id))");

        $this->addSql("COMMENT ON COLUMN cyclab_segments.geom IS '(DC2Type:linestring)'");

        $this->addSql("CREATE INDEX IDX_26CEDB298C803113 ON cyclab_segments (way_id)");
        $this->addSql("CREATE INDEX IDX_26CEDB29490B3FC2 ON cyclab_segments (nodeStart_id)");
        $this->addSql("CREATE INDEX IDX_26CEDB297682F146 ON cyclab_segments (nodeEnd_id)");

        $this->addSql("ALTER TABLE cyclab_intersection_paths ADD CONSTRAINT FK_cyclab_intersection_paths_node_id FOREIGN KEY (node_id) REFERENCES nodes (id) NOT DEFERRABLE INITIALLY IMMEDIATE");
        $this->addSql("ALTER TABLE cyclab_intersection_paths ADD CONSTRAINT FK_28646481B07A3B52 FOREIGN KEY (segmentStart_id) REFERENCES cyclab_segments (id) NOT DEFERRABLE INITIALLY IMMEDIATE");
        $this->addSql("ALTER TABLE cyclab_intersection_paths ADD CONSTRAINT FK_286464817A2CED74 FOREIGN KEY (segmentEnd_id) REFERENCES cyclab_segments (id) NOT DEFERRABLE INITIALLY IMMEDIATE");
        $this->addSql("ALTER TABLE cyclab_segments ADD CONSTRAINT FK_cyclab_segments_way_id FOREIGN KEY (way_id) REFERENCES ways (id) NOT DEFERRABLE INITIALLY IMMEDIATE");
        $this->addSql("ALTER TABLE cyclab_segments ADD CONSTRAINT FK_cyclab_segments_nodeStart_id FOREIGN KEY (nodeStart_id) REFERENCES nodes (id) NOT DEFERRABLE INITIALLY IMMEDIATE");
        $this->addSql("ALTER TABLE cyclab_segments ADD CONSTRAINT FK_cyclab_segments_nodeEnd_id FOREIGN KEY (nodeEnd_id) REFERENCES nodes (id) NOT DEFERRABLE INITIALLY IMMEDIATE");

        $this->addSql("CREATE INDEX cyclab_segment_geom ON cyclab_segments using GIST (geom)");

        $this->addSql("CREATE OR REPLACE VIEW cyclab_intersections AS "
            . "SELECT nodes.id, "
            . "nodes.version, "
            . "nodes.user_id, "
            . "nodes.tstamp, "
            . "nodes.changeset_id, "
            . "nodes.tags, "
            . "nodes.geom, "
            . "nodes.ways_nbr, "
            . "nodes.vote_average, "
            . "nodes.vote_number, "
            . "nodes.vote_default "
            . "FROM nodes WHERE nodes.ways_nbr > 1;");
    }

    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");

        $this->addSql("DROP VIEW IF EXISTS cyclab_intersections");

        $this->addSql("DROP INDEX cyclab_segment_geom");

        $this->addSql("ALTER TABLE cyclab_segments DROP CONSTRAINT FK_26CEDB298C803113");
        $this->addSql("ALTER TABLE cyclab_intersection_paths DROP CONSTRAINT FK_28646481460D9FD7");
        $this->addSql("ALTER TABLE cyclab_segments DROP CONSTRAINT FK_26CEDB29490B3FC2");
        $this->addSql("ALTER TABLE cyclab_segments DROP CONSTRAINT FK_26CEDB297682F146");
        $this->addSql("ALTER TABLE cyclab_intersection_paths DROP CONSTRAINT FK_28646481B07A3B52");
        $this->addSql("ALTER TABLE cyclab_intersection_paths DROP CONSTRAINT FK_286464817A2CED74");
        $this->addSql("DROP SEQUENCE cyclab_intersection_paths_id_seq CASCADE");
        $this->addSql("DROP SEQUENCE cyclab_segments_id_seq CASCADE");
        $this->addSql("DROP TABLE cyclab_intersection_paths");
        $this->addSql("DROP TABLE cyclab_segments");
    }
}
