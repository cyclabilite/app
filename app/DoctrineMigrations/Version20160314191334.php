<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160314191334 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql("ALTER TABLE cyclab_segments ADD COLUMN averageData json NOT NULL DEFAULT '{}'");
        $this->addSql("ALTER TABLE cyclab_intersection_paths ADD COLUMN averageData json NOT NULL DEFAULT '{}'");
        $this->addSql("ALTER TABLE nodes ADD COLUMN averageData json NOT NULL DEFAULT '{}'");
        $this->addSql("CREATE OR REPLACE VIEW cyclab_intersections AS "
            . "SELECT nodes.id, "
            . "nodes.version, "
            . "nodes.user_id, "
            . "nodes.tstamp, "
            . "nodes.changeset_id, "
            . "nodes.tags, "
            . "nodes.geom, "
            . "nodes.ways_nbr, "
            . "nodes.vote_average, "
            . "nodes.vote_number, "
            . "nodes.vote_default, "
            . "nodes.averagedata "
            . "FROM nodes WHERE nodes.ways_nbr > 1;");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql("ALTER TABLE cyclab_segments DROP COLUMN averageData");
        $this->addSql("ALTER TABLE cyclab_intersection_paths DROP COLUMN averageData");
        $this->addSql("ALTER TABLE nodes DROP COLUMN averageData");
        $this->addSql("CREATE OR REPLACE VIEW cyclab_intersections AS "
            . "SELECT nodes.id, "
            . "nodes.version, "
            . "nodes.user_id, "
            . "nodes.tstamp, "
            . "nodes.changeset_id, "
            . "nodes.tags, "
            . "nodes.geom, "
            . "nodes.ways_nbr, "
            . "nodes.vote_average, "
            . "nodes.vote_number, "
            . "nodes.vote_default "
            . "FROM nodes WHERE nodes.ways_nbr > 1;");
    }
}
