<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210208162753 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('
        CREATE OR REPLACE VIEW ways_all AS (
            SELECT id, name, highway, linestring, version, user_id,  vote_default, vote_number, vote_average FROM ways 
        ) UNION ALL  (
            SELECT id, name, highway, linestring, version, user_id,  vote_default, vote_number, vote_average FROM deleted_ways 
        );');
    
        // this up() migration is auto-generated, please modify it to your needs

    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP VIEW ways_all;');
    }
}
