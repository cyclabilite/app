<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180408194752 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql('ALTER TABLE cyclab_fos_users DROP COLUMN locked;');
        $this->addSql('ALTER TABLE cyclab_fos_users DROP COLUMN expires_at;');
        $this->addSql('ALTER TABLE cyclab_fos_users DROP COLUMN credentials_expire_at;');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('ALTER TABLE cyclab_fos_users ADD COLUMN locked BOOLEAN NOT NULL DEFAULT FALSE;');
        $this->addSql('ALTER TABLE cyclab_fos_users ADD COLUMN expires_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL;');
        $this->addSql('ALTER TABLE cyclab_fos_users ADD COLUMN credentials_expire_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL;');
    }
}
