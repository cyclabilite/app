<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161118190729 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE cyclab_users ADD COLUMN label_canonical VARCHAR(180)');
        $this->addSql('UPDATE cyclab_users SET label_canonical = lower(label)');

        // adding id to the label when having same label_canonical (that must be unique)
        $this->addSql('UPDATE cyclab_users SET label = concat(label, id), label_canonical = concat(label_canonical, id) WHERE label_canonical = ANY (SELECT label_canonical FROM cyclab_users GROUP BY label_canonical HAVING count(*) > 1) AND label != label_canonical;');

        $this->addSql("INSERT INTO cyclab_fos_users (
    id, username, username_canonical, email, email_canonical,
    enabled, salt, password, locked, roles,
    label, gender, birthyear, creationdate, isexpert,
    last_login
    )
    ( SELECT
        id, label, lower(label), email, lower(email),
        TRUE, md5(random()::text), md5(random()::text), islocked, 'a:0:{}',
        label, gender, birthyear, creationdate, isexpert,
        lastlogin FROM cyclab_users
    )");

        $this->addSql("SELECT  setval('cyclab_fos_users_id_seq', nextval('cyclab_users_id_seq'))");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('ALTER TABLE cyclab_users DROP COLUMN label_canonical');
    }
}
