<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Add tables for the deleted elements :
 * - deleted_nodes
 * - deleted_ways
 * - deleted_cyclab_segments
 * - deleted_cyclab_intersection_paths
 */
class Version20140422134018 extends AbstractMigration
{
   public function up(Schema $schema)
   {
      $this->addSql("CREATE TABLE deleted_nodes (LIKE nodes INCLUDING DEFAULTS)");
      $this->addSql("CREATE TABLE deleted_ways (LIKE ways INCLUDING DEFAULTS)");
      $this->addSql("CREATE TABLE deleted_cyclab_segments (LIKE cyclab_segments INCLUDING DEFAULTS)");
      $this->addSql("CREATE TABLE deleted_cyclab_intersection_paths (LIKE cyclab_intersection_paths INCLUDING DEFAULTS)");

      $this->addSql("ALTER TABLE deleted_ways ADD COLUMN resilience_treated boolean DEFAULT False;");
   }

   public function down(Schema $schema)
   {
      $this->addSql("DROP TABLE deleted_nodes");
      $this->addSql("DROP TABLE deleted_ways");
      $this->addSql("DROP TABLE deleted_cyclab_segments");
      $this->addSql("DROP TABLE deleted_cyclab_intersection_paths");
   }
}
