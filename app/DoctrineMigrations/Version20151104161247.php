<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use CL\Cyclabilite\VoteBundle\Entity\VoteSegment;

/**
 * Converts all the ways votes into multiple segments votes and remove the
 * postgresql table for ways_vote
 */
class Version20151104161247 extends AbstractMigration implements ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        if ($container === NULL) {
            throw new \RuntimeException('Container is not provided. This migration '
                . 'need container to set a default center');
        }
        $this->container = $container;
    }

    /**
     * Converts all the ways votes into multiple segments votes
     *
     * @param Schema $schema
     */
    public function preUp(Schema $schema)
    {
        echo 'Converts all the vote for ways into vote segments - DISABLED';
        echo 'The class CL\Cyclabilite\VoteBundle\Entity\VoteWay does not exist
            anymore';

        #echo 'Converts all the vote for ways into vote segments';

        #$em = $this->container->get('doctrine.orm.entity_manager');
        #$voteWays = $em->getRepository('CLCyclabiliteVoteBundle:VoteWay')->findAll();

        #foreach($voteWays as $voteWay) {
        #    $way = $voteWay->getWay();
        #    $waySegments = $way->getSegments();
        #
        #    foreach($waySegments as $waySegment) {
        #        $newVote = new VoteSegment();
        #        $newVote->setValue($voteWay->getValue());
        #        $newVote->setDatetimeVote($voteWay->getDatetimeVote());
        #        $newVote->setDatetimeCreation($voteWay->getDatetimeCreation());
        #        $newVote->setClient($voteWay->getClient());
        #        $newVote->setUser($voteWay->getUser());
        #        $newVote->setProfile($voteWay->getProfile());
        #        $newVote->setDirection($voteWay->getDirection());
        #
        #        $newVote->setSegment($waySegment);
        #
        #        $em->persist($newVote);
        #    }
        #}
        #
        #$em->flush();
    }

    public function up(Schema $schema)
    {
        $this->addSql("DROP INDEX cyclab_way_vote_way_id;");
        $this->addSql("DROP TABLE cyclab_way_vote CASCADE;");
    }

    public function down(Schema $schema)
    {
        $this->addSql("CREATE TABLE cyclab_way_vote ( "
            . "way_id BIGINT, "
            . "direction direction DEFAULT 'both', "
            . "CONSTRAINT FK_cyclab_way_vote_way_id_fkey FOREIGN KEY (way_id) REFERENCES ways (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION "
            . ") INHERITS (cyclab_abstract_vote);");

        $this->addSql("CREATE INDEX cyclab_way_vote_way_id "
            . "ON cyclab_way_vote USING btree (way_id);");
    }
}
