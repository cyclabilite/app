<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161205181811 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE public.cyclab_abstract_vote DROP CONSTRAINT cyclab_abstract_vote_user_id_fkey;');
        $this->addSql('ALTER TABLE public.cyclab_abstract_vote ADD CONSTRAINT cyclab_abstract_vote_fos_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.cyclab_fos_users (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION;');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('ALTER TABLE public.cyclab_abstract_vote DROP CONSTRAINT cyclab_abstract_vote_fos_user_id_fkey;');
        $this->addSql('ALTER TABLE public.cyclab_abstract_vote ADD CONSTRAINT cyclab_abstract_vote_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.cyclab_users (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION;');
    }
}
