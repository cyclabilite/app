<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * add the tables for the vote :
 * - abstract table cyclab_vote
 * - cyclab_segment_vote
 * - cyclab_way_vote
 * - cyclab_intersection_path_vote
 */
class Version20140227151637 extends AbstractMigration
{
   public function up(Schema $schema)
   {
      $this->addSql("CREATE TYPE direction AS ENUM('forward', 'backward', 'both')");

      $this->addSql("CREATE SEQUENCE cyclab_votes_id_seq "
             . "INCREMENT 1 MINVALUE 1  MAXVALUE 9223372036854775807  "
             . "START 1  CACHE 1;");

      $this->addSql("CREATE TABLE cyclab_abstract_vote (   
         id BIGINT DEFAULT nextval('cyclab_votes_id_seq') NOT NULL PRIMARY KEY,   
         vote_value int DEFAULT 2,   
         user_id int REFERENCES cyclab_users (id),   
         profile_id int REFERENCES cyclab_profile (id),   
         datetime_vote timestamp(0) DEFAULT NULL,   
         datetime_creation timestamp(0),   
         client varchar(255),   
         localisation geometry(point, 4326),   
         details hstore   
         )");

      $this->addSql("CREATE TABLE cyclab_segment_vote (  "
             . "segment_id BIGINT REFERENCES cyclab_segments (id), "
             . "direction direction DEFAULT 'both' "
             . ") INHERITS (cyclab_abstract_vote);");

      $this->addSql("CREATE INDEX cyclab_segment_vote_segment_id ON cyclab_segment_vote "
           . "using btree (segment_id);");

      $this->addSql("CREATE TABLE cyclab_way_vote ( "
             . "way_id BIGINT, "
             . "direction direction DEFAULT 'both', "
             . "CONSTRAINT FK_cyclab_way_vote_way_id_fkey FOREIGN KEY (way_id) REFERENCES ways (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION "
             . ") INHERITS (cyclab_abstract_vote);");

      $this->addSql("CREATE INDEX cyclab_way_vote_way_id "
        . "ON cyclab_way_vote USING btree (way_id);");

      $this->addSql("
CREATE TABLE cyclab_intersection_path_vote
(
-- Inherited from table cyclab_abstract_vote:  id bigint NOT NULL DEFAULT nextval('cyclab_votes_id_seq'::regclass),
-- Inherited from table cyclab_abstract_vote:  vote_value integer DEFAULT 2,
-- Inherited from table cyclab_abstract_vote:  user_id integer,
-- Inherited from table cyclab_abstract_vote:  profile_id integer,
-- Inherited from table cyclab_abstract_vote:  datetime_vote timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
-- Inherited from table cyclab_abstract_vote:  datetime_creation timestamp(0) without time zone,
-- Inherited from table cyclab_abstract_vote:  client character varying(255),
-- Inherited from table cyclab_abstract_vote:  localisation geometry(Point,4326),
-- Inherited from table cyclab_abstract_vote:  details hstore,
  intersection_path_id bigint,
  CONSTRAINT cyclab_intersection_path_vote_intersection_id_fkey FOREIGN KEY (intersection_path_id)
      REFERENCES cyclab_intersection_paths (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
INHERITS (cyclab_abstract_vote)
"
      );

      $this->addSql("
CREATE INDEX cyclab_intersection_path_vote_node_id
  ON cyclab_intersection_path_vote
  USING btree
  (intersection_path_id);
   "
      );
   }

   public function down(Schema $schema)
   {
      $this->addSql("DROP INDEX cyclab_intersection_path_vote_node_id");
      $this->addSql("DROP TABLE cyclab_intersection_path_vote");
      $this->addSql("DROP INDEX cyclab_way_vote_way_id;");
      $this->addSql("DROP TABLE cyclab_way_vote CASCADE;");
      $this->addSql("DROP INDEX cyclab_segment_vote_segment_id");
      $this->addSql("DROP TABLE cyclab_segment_vote CASCADE");
      $this->addSql("DROP TABLE cyclab_abstract_vote CASCADE");
      $this->addSql("DROP SEQUENCE cyclab_votes_id_seq");

      $this->addSql("DROP TYPE direction");
   }
}
        
        
        
