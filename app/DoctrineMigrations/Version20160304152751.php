<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160304152751 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql("ALTER TABLE cyclab_users ADD COLUMN isexpert boolean NOT NULL DEFAULT false;");
        $this->addSql("ALTER TABLE cyclab_abstract_vote ADD COLUMN is_expert_vote boolean NOT NULL DEFAULT false;");
        // this up() migration is auto-generated, please modify it to your needs

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql("ALTER TABLE cyclab_users DROP COLUMN isexpert;");
        $this->addSql("ALTER TABLE cyclab_abstract_vote DROP COLUMN is_expert_vote;");
        // this down() migration is auto-generated, please modify it to your needs

    }
}
