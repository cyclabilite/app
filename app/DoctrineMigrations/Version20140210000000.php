<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * CREATE DB FOR OSMOSIS PGSNAPSHOT V 0.6
 * (Database creation script for the simple PostgreSQL schema)
 */
class Version20140210000000 extends AbstractMigration
{
   public function up(Schema $schema)
   {
      /*-----------------------------------*/
      /***** pgsnapshot_schema_0.6.sql *****/
      /*-----------------------------------*/

      //-- Drop all tables if they exist.
      $this->addSql("DROP TABLE IF EXISTS actions;");
      $this->addSql("DROP TABLE IF EXISTS users;");
      $this->addSql("DROP TABLE IF EXISTS nodes;");
      $this->addSql("DROP TABLE IF EXISTS ways;");
      $this->addSql("DROP TABLE IF EXISTS way_nodes;");
      $this->addSql("DROP TABLE IF EXISTS relations;");
      $this->addSql("DROP TABLE IF EXISTS relation_members;");
      $this->addSql("DROP TABLE IF EXISTS schema_info;");

      //-- Drop all stored procedures if they exist.
      $this->addSql("DROP FUNCTION IF EXISTS osmosisUpdate();");

      //-- Create a table which will contain a single row defining the current schema version.
      $this->addSql("CREATE TABLE schema_info ("
         . " version integer NOT NULL"
         . " );");

      //-- Create a table for users.
      $this->addSql("CREATE TABLE users ("
         . " id int NOT NULL,"
         . " name text NOT NULL"
         . " );");

      //-- Create a table for nodes.
      $this->addSql("CREATE TABLE nodes ("
         . " id bigint NOT NULL,"
         . " version int NOT NULL,"
         . " user_id int NOT NULL,"
         . " tstamp timestamp without time zone NOT NULL,"
         . " changeset_id bigint NOT NULL,"
         . " tags hstore"
      . " );");

      //-- Add a postgis point column holding the location of the node.
      $this->addSql("SELECT AddGeometryColumn('nodes', 'geom', 4326, 'POINT', 2);");


      //-- Create a table for ways.
      $this->addSql("CREATE TABLE ways ("
         . " id bigint NOT NULL,"
         . " version int NOT NULL,"
         . " user_id int NOT NULL,"
         . " tstamp timestamp without time zone NOT NULL,"
         . " changeset_id bigint NOT NULL,"
         . " tags hstore,"
         . " nodes bigint[]"
      . " );");


      //-- Create a table for representing way to node relationships.
      $this->addSql("CREATE TABLE way_nodes ("
         . " way_id bigint NOT NULL,"
         . " node_id bigint NOT NULL,"
         . " sequence_id int NOT NULL"
      . " );");


      //-- Create a table for relations.
      $this->addSql("CREATE TABLE relations ("
         . " id bigint NOT NULL,"
         . " version int NOT NULL,"
         . " user_id int NOT NULL,"
         . " tstamp timestamp without time zone NOT NULL,"
         . " changeset_id bigint NOT NULL,"
         . " tags hstore"
      . " );");

      //-- Create a table for representing relation member relationships.
      $this->addSql("CREATE TABLE relation_members ("
         . " relation_id bigint NOT NULL,"
         . " member_id bigint NOT NULL,"
         . " member_type character(1) NOT NULL,"
         . " member_role text NOT NULL,"
         . " sequence_id int NOT NULL"
      . " );");


      //-- Configure the schema version.
      $this->addSql("INSERT INTO schema_info (version) VALUES (6);");


      //-- Add primary keys to tables.
      $this->addSql("ALTER TABLE ONLY schema_info ADD CONSTRAINT pk_schema_info PRIMARY KEY (version);");

      $this->addSql("ALTER TABLE ONLY users ADD CONSTRAINT pk_users PRIMARY KEY (id);");

      $this->addSql("ALTER TABLE ONLY nodes ADD CONSTRAINT pk_nodes PRIMARY KEY (id);");

      $this->addSql("ALTER TABLE ONLY ways ADD CONSTRAINT pk_ways PRIMARY KEY (id);");

      $this->addSql("ALTER TABLE ONLY way_nodes ADD CONSTRAINT pk_way_nodes PRIMARY KEY (way_id, sequence_id);");

      $this->addSql("ALTER TABLE ONLY relations ADD CONSTRAINT pk_relations PRIMARY KEY (id);");

      $this->addSql("ALTER TABLE ONLY relation_members ADD CONSTRAINT pk_relation_members PRIMARY KEY (relation_id, sequence_id);");


      //-- Add indexes to tables.
      $this->addSql("CREATE INDEX idx_nodes_geom ON nodes USING gist (geom);");

      $this->addSql("CREATE INDEX idx_way_nodes_node_id ON way_nodes USING btree (node_id);");

      $this->addSql("CREATE INDEX idx_relation_members_member_id_and_type ON relation_members USING btree (member_id, member_type);");


      //-- Cluster tables by geographical location.
      $this->addSql("CLUSTER nodes USING idx_nodes_geom;");


      //-- Create the function that provides "unnest" functionality while remaining compatible with 8.3.
      $this->addSql("CREATE OR REPLACE FUNCTION unnest_bbox_way_nodes() RETURNS void AS $$"
      . " DECLARE"
         . " previousId ways.id%TYPE;"
         . " currentId ways.id%TYPE;"
         . " result bigint[];"
         . " wayNodeRow way_nodes%ROWTYPE;"
         . " wayNodes ways.nodes%TYPE;"
      . " BEGIN"
         . " FOR wayNodes IN SELECT bw.nodes FROM bbox_ways bw LOOP"
            . " FOR i IN 1 .. array_upper(wayNodes, 1) LOOP"
               . " INSERT INTO bbox_way_nodes (id) VALUES (wayNodes[i]);"
            . " END LOOP;"
         . " END LOOP;"
      . " END;"
      . " $$ LANGUAGE plpgsql;");


      //-- Create customisable hook function that is called within the replication update transaction.
      $this->addSql("CREATE FUNCTION osmosisUpdate() RETURNS void AS $$"
      . " DECLARE"
      . " BEGIN"
      . " END;"
      . " $$ LANGUAGE plpgsql;");

      /*------------------------------------------*/
      /***** pgsnapshot_schema_0.6_action.sql *****/
      /*------------------------------------------*/

      //-- Add an action table for the purpose of capturing all actions applied to a database.
      //-- The table is populated during application of a changeset, then osmosisUpdate is called,
      //-- then the table is cleared all within a single database transaction.
      //-- The contents of this table can be used to update derivative tables by customising the
      //-- osmosisUpdate stored procedure.

      //-- Create a table for actions.
       $this->addSql("CREATE TABLE actions (
         data_type character(1) NOT NULL,
         action character(1) NOT NULL,
         id bigint NOT NULL
      );");

      //-- Add primary key.
      $this->addSql("ALTER TABLE ONLY actions ADD CONSTRAINT pk_actions PRIMARY KEY (data_type, id);");

      /*----------------------------------------------*/
      /***** pgsnapshot_schema_0.6_linestring.sql *****/
      /*----------------------------------------------*/

      //-- Add a postgis GEOMETRY column to the way table for the purpose of storing the full linestring of the way.
      $this->addSql("SELECT AddGeometryColumn('ways', 'linestring', 4326, 'GEOMETRY', 2);");

      //-- Add an index to the bbox column.
      $this->addSql("CREATE INDEX idx_ways_linestring ON ways USING gist (linestring);");

      //-- Cluster table by geographical location.
      $this->addSql("CLUSTER ways USING idx_ways_linestring;");
   }

   public function down(Schema $schema)
   {
      /*-----------------------------------*/
      /***** pgsnapshot_schema_0.6.sql *****/
      /*-----------------------------------*/

      $this->addSql("DROP TABLE IF EXISTS actions;");
      $this->addSql("DROP TABLE IF EXISTS users;");
      $this->addSql("DROP TABLE IF EXISTS nodes;");
      $this->addSql("DROP TABLE IF EXISTS ways;");
      $this->addSql("DROP TABLE IF EXISTS way_nodes;");
      $this->addSql("DROP TABLE IF EXISTS relations;");
      $this->addSql("DROP TABLE IF EXISTS relation_members;");
      $this->addSql("DROP TABLE IF EXISTS schema_info;");

      //-- Drop all stored procedures if they exist.
      $this->addSql("DROP FUNCTION IF EXISTS osmosisUpdate();");
   }
}
