<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160401120411 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql("ALTER TABLE cyclab_intersection_path_vote 
            ADD CONSTRAINT u_user_profile_ipath_dtvote_dtcreation
                UNIQUE (user_id, profile_id, intersection_path_id, datetime_vote, datetime_creation);");
            
        $this->addSql("ALTER TABLE cyclab_segment_vote
            ADD CONSTRAINT u_user_profile_seg_dtvote_dtcreation
                UNIQUE (user_id, profile_id, segment_id, datetime_vote, datetime_creation, direction);");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql("ALTER TABLE cyclab_intersection_path_vote DROP CONSTRAINT u_user_profile_ipath_dtvote_dtcreation;");
        $this->addSql("ALTER TABLE cyclab_segment_vote DROP CONSTRAINT u_user_profile_seg_dtvote_dtcreation;");
    }
}
