<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160630165255 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("ALTER TABLE deleted_nodes ADD COLUMN averageData json NOT NULL DEFAULT '{}'");
        $this->addSql("ALTER TABLE deleted_cyclab_intersection_paths ADD COLUMN averageData json NOT NULL DEFAULT '{}'");
        $this->addSql("ALTER TABLE deleted_cyclab_segments ADD COLUMN averageData json NOT NULL DEFAULT '{}'");
        $this->addSql("CREATE TABLE deleted_cyclab_segment_vote (  "
            . "segment_id BIGINT, "
            . "direction direction DEFAULT 'both' "
            . ") INHERITS (cyclab_abstract_vote);");
        $this->addSql("CREATE TABLE deleted_cyclab_intersection_path_vote ( "
            . "intersection_path_id BIGINT "
            . ") INHERITS (cyclab_abstract_vote);");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql("ALTER TABLE deleted_nodes DROP COLUMN averageData");
        $this->addSql("ALTER TABLE deleted_cyclab_intersection_paths DROP COLUMN averageData");
        $this->addSql("ALTER TABLE deleted_cyclab_segments DROP COLUMN averageData");
        $this->addSql("DROP TABLE deleted_cyclab_segment_vote");
        $this->addSql("DROP TABLE deleted_cyclab_intersection_path_vote");
    }
}
