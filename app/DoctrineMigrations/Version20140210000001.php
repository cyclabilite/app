<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Mise a jour de la DB OSMOSIS (ways et nodes) pour le projet cyclab
 */
class Version20140210000001 extends AbstractMigration
{
   public function up(Schema $schema)
   {
      $this->addSql("ALTER TABLE ways "
         . "ADD COLUMN name VARCHAR(255) DEFAULT NULL, "
         . "ADD COLUMN highway VARCHAR(255) DEFAULT NULL, "
         . "ADD COLUMN vote_default integer DEFAULT 3, "
         . "ADD COLUMN vote_number integer DEFAULT 0 NOT NULL, "
         . "ADD COLUMN vote_average float DEFAULT NULL");

      $this->addSql("ALTER TABLE nodes "
         . "ADD COLUMN ways_nbr int DEFAULT 0, "
         . "ADD COLUMN vote_default integer DEFAULT 3, "
         . "ADD COLUMN vote_number integer DEFAULT 0 NOT NULL, "
         . "ADD COLUMN vote_average float DEFAULT NULL");  

      $this->addSql("CREATE INDEX node_geom ON nodes using GIST (geom)"); 

      $this->addSql("CREATE INDEX nodes_ways_nbr_index "
         . "ON nodes USING Btree (ways_nbr)  WHERE ways_nbr > 1 ;");
   }

   public function down(Schema $schema)
   {
      $this->addSql("DROP INDEX nodes_ways_nbr_index");
      
      $this->addSql("DROP INDEX node_geom");

      $this->addSql("ALTER TABLE ways "
         . "DROP COLUMN name, "
         . "DROP COLUMN highway, "
         . "DROP COLUMN vote_default, "
         . "DROP COLUMN vote_number, "
         . "DROP COLUMN vote_average");

      $this->addSql("ALTER TABLE nodes "
         . "DROP COLUMN ways_nbr, "
         . "DROP COLUMN vote_default, "
         . "DROP COLUMN vote_number, "
         . "DROP COLUMN vote_average");
   }
}