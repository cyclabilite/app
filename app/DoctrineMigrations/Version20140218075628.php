<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Add cyclab_users and cyclab_profile
 */
class Version20140218075628 extends AbstractMigration
{
   public function up(Schema $schema)
   {
      // this up() migration is auto-generated, please modify it to your needs
      $this->abortIf($this->connection->getDatabasePlatform()->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");
     
      //create sequences
      $this->addSql("CREATE SEQUENCE cyclab_profile_id_seq INCREMENT BY 1 MINVALUE 1 START 1");
      $this->addSql("CREATE SEQUENCE cyclab_users_id_seq INCREMENT BY 1 MINVALUE 1 START 1");
     
      //create table profile
      $this->addSql("CREATE TABLE cyclab_profile ("
         . "id INT NOT NULL, "
         . "label VARCHAR(250) NOT NULL, "
         . "user_id INT DEFAULT NULL, "
         . "daily_ride boolean DEFAULT TRUE, "
         . "rapidity_vs_security int DEFAULT 0 NOT NULL, "
         . "active boolean DEFAULT TRUE, "
         . "PRIMARY KEY(id))");

      $this->addSql("CREATE INDEX IDX_AE5F597A76ED395 ON cyclab_profile (user_id)");

      $this->addSql("CREATE TABLE cyclab_users ("
         . "id INT NOT NULL, "
         . "label VARCHAR(255) NOT NULL, "
         . "email VARCHAR(255) NOT NULL, "
         . "lastLogin TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, "
         . "creationDate TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, "
         . "isLocked BOOLEAN NOT NULL DEFAULT FALSE, "
         . "gender VARCHAR(6) DEFAULT 'female', "
         . "birthYear INT DEFAULT 0, "
         . "PRIMARY KEY(id))");

      
      $this->addSql("ALTER TABLE cyclab_profile ADD CONSTRAINT FK_AE5F597A76ED395 FOREIGN KEY (user_id) REFERENCES cyclab_users (id) NOT DEFERRABLE INITIALLY IMMEDIATE");
     
      //create an index on lower email, which will be searched by persona
      $this->addSql("CREATE INDEX IDX_users_email ON cyclab_users (LOWER(email))");     
   }

   public function down(Schema $schema)
   {
      // this down() migration is auto-generated, please modify it to your needs
      $this->abortIf($this->connection->getDatabasePlatform()->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");

      $this->addSql("ALTER TABLE cyclab_profile DROP CONSTRAINT FK_AE5F597A76ED395");
      $this->addSql("DROP INDEX IDX_AE5F597A76ED395");

      $this->addSql("DROP SEQUENCE cyclab_profile_id_seq CASCADE");
      $this->addSql("DROP SEQUENCE cyclab_users_id_seq CASCADE");
      $this->addSql("DROP TABLE cyclab_profile CASCADE");
      $this->addSql("DROP TABLE cyclab_users CASCADE");
   }
}