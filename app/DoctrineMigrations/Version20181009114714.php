<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181009114714 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('CREATE OR REPLACE VIEW nodes_all AS (SELECT id, geom, vote_default, vote_number, vote_average, averagedata FROM nodes) UNION ALL (SELECT id, geom, vote_default, vote_number, vote_average, averagedata  FROM deleted_nodes);');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP VIEW nodes_all;');
    }
}
