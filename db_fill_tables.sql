INSERT INTO nodes (id_osm, in_use, osmosis_data_1, osmosis_data_2) VALUES (11, TRUE, 'blop', 'blap'); /* 1 */
INSERT INTO nodes (id_osm, in_use, osmosis_data_1, osmosis_data_2) VALUES (21, FALSE, 'blop', 'blap'); /* 2 */
INSERT INTO nodes (id_osm, in_use, osmosis_data_1, osmosis_data_2) VALUES (23, TRUE, 'blop', 'blap'); /* 3 */
INSERT INTO nodes (id_osm, in_use, osmosis_data_1, osmosis_data_2) VALUES (24, TRUE, 'blop', 'blap'); /* 4 */
INSERT INTO nodes (id_osm, in_use, osmosis_data_1, osmosis_data_2) VALUES (25, TRUE, 'blop', 'blap'); /* 5 */
INSERT INTO nodes (id_osm, in_use, osmosis_data_1, osmosis_data_2) VALUES (30, TRUE, 'blop', 'blap'); /* 6 */
INSERT INTO nodes (id_osm, in_use, osmosis_data_1, osmosis_data_2) VALUES (32, TRUE, 'blop', 'blap'); /* 7 */
INSERT INTO nodes (id_osm, in_use, osmosis_data_1, osmosis_data_2) VALUES (33, TRUE, 'blop', 'blap'); /* 8 */
INSERT INTO nodes (id_osm, in_use, osmosis_data_1, osmosis_data_2) VALUES (34, TRUE, 'blop', 'blap'); /* 9 */
INSERT INTO nodes (id_osm, in_use, osmosis_data_1, osmosis_data_2) VALUES (35, TRUE, 'blop', 'blap'); /* 10 */

INSERT INTO ways (id_osm, in_use, osmosis_data_1, osmosis_data_2) VALUES (12, TRUE, 'blop', 'blap'); /* 1 */
INSERT INTO ways (id_osm, in_use, osmosis_data_1, osmosis_data_2) VALUES (22, FALSE, 'blop', 'blap'); /* 2 */
INSERT INTO ways (id_osm, in_use, osmosis_data_1, osmosis_data_2) VALUES (27, TRUE, 'blop', 'blap'); /* 3 */
INSERT INTO ways (id_osm, in_use, osmosis_data_1, osmosis_data_2) VALUES (28, FALSE, 'blop', 'blap'); /* 4 */
INSERT INTO ways (id_osm, in_use, osmosis_data_1, osmosis_data_2) VALUES (29, TRUE, 'blop', 'blap'); /* 5 */
INSERT INTO ways (id_osm, in_use, osmosis_data_1, osmosis_data_2) VALUES (31, TRUE, 'blop', 'blap'); /* 6 */
INSERT INTO ways (id_osm, in_use, osmosis_data_1, osmosis_data_2) VALUES (36, FALSE, 'blop', 'blap'); /* 7 */
INSERT INTO ways (id_osm, in_use, osmosis_data_1, osmosis_data_2) VALUES (37, TRUE, 'blop', 'blap'); /* 8 */
INSERT INTO ways (id_osm, in_use, osmosis_data_1, osmosis_data_2) VALUES (38, TRUE, 'blop', 'blap'); /* 9 */
INSERT INTO ways (id_osm, in_use, osmosis_data_1, osmosis_data_2) VALUES (39, TRUE, 'blop', 'blap'); /* 10 */

/* ajout de tous les semgents */
INSERT INTO segments (way_id, node_start_id, node_end_id) VALUES (5, 8, 5); /* 1 */
INSERT INTO segments (way_id, node_start_id, node_end_id) VALUES (5, 5, 9); /* 2 */
INSERT INTO segments (way_id, node_start_id, node_end_id) VALUES (3, 1, 5); /* 3 */
INSERT INTO segments (way_id, node_start_id, node_end_id) VALUES (1, 8, 1); /* 4 */
INSERT INTO segments (way_id, node_start_id, node_end_id) VALUES (1, 1, 9); /* 5 */
INSERT INTO segments (way_id, node_start_id, node_end_id) VALUES (6, 8, 10); /* 6 */
INSERT INTO segments (way_id, node_start_id, node_end_id) VALUES (3, 10, 1); /* 7 */
INSERT INTO segments (way_id, node_start_id, node_end_id) VALUES (9, 10, 9); /* 8 */

/* description d'un seul carrefour (celui du milieu) */
INSERT INTO intersection_paths (node_id, segment_start_id, segment_stop_id) VALUES (1,3,5);
INSERT INTO intersection_paths (node_id, segment_start_id, segment_stop_id) VALUES (1,3,7);
INSERT INTO intersection_paths (node_id, segment_start_id, segment_stop_id) VALUES (1,3,4);
INSERT INTO intersection_paths (node_id, segment_start_id, segment_stop_id) VALUES (1,5,3);
INSERT INTO intersection_paths (node_id, segment_start_id, segment_stop_id) VALUES (1,5,7);
INSERT INTO intersection_paths (node_id, segment_start_id, segment_stop_id) VALUES (1,5,4);
INSERT INTO intersection_paths (node_id, segment_start_id, segment_stop_id) VALUES (1,7,5);
INSERT INTO intersection_paths (node_id, segment_start_id, segment_stop_id) VALUES (1,7,3);
INSERT INTO intersection_paths (node_id, segment_start_id, segment_stop_id) VALUES (1,7,4);
INSERT INTO intersection_paths (node_id, segment_start_id, segment_stop_id) VALUES (1,4,5);
INSERT INTO intersection_paths (node_id, segment_start_id, segment_stop_id) VALUES (1,4,7);
INSERT INTO intersection_paths (node_id, segment_start_id, segment_stop_id) VALUES (1,4,3);