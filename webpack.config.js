var Encore = require('@symfony/webpack-encore');

Encore
    // the project directory where all compiled assets will be stored
    .setOutputPath('web/build/')

    // the public path used by the web server to access the previous directory
    .setPublicPath('/build')

    // will create public/build/app.js and public/build/app.css
    .addEntry('app', './assets/js/app.js')

    // will create public/build/admin_vote_report.js
    .addEntry('admin_vote_report', './assets/js/admin_vote_report.js')

    // will create public/build/admin_trivial_check.js
    .addEntry('admin_trivial_check', './assets/js/admin_trivial_check.js')

    // will create public/build/admin_trivial_prepare.js
    .addEntry('admin_trivial_prepare', './assets/js/admin_trivial_prepare.js')

    // entry for forms
    .addEntry('style', './assets/js/style.js')

    .addEntry('leaflet', './node_modules/leaflet/dist/leaflet.js')

    // allow legacy applications to use $/jQuery as a global variable
    .autoProvidejQuery()

    // enable source maps during development
    .enableSourceMaps(!Encore.isProduction())

    // empty the outputPath dir before each build
    .cleanupOutputBeforeBuild()

    // show OS notifications when builds finish/fail
    //.enableBuildNotifications()

    // create hashed filenames (e.g. app.abc123.css)
    // .enableVersioning()

    // allow sass/scss files to be processed
    .enableSassLoader()
;

// export the final configuration
module.exports = Encore.getWebpackConfig();
