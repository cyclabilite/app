def exe_rules(cur, rel_vote, and_tag_value_vote):
    # VOTE 5/5

    ## RIEN

    # VOTE 4/5
    rel_vote(cur, [("network", "ncn"), ("network", "rcn"), ("network", "lcn")], 4)


    and_tag_value_vote(cur, [("highway", "pedestrian")], 4)
    and_tag_value_vote(cur, [("highway", "living_street")], 4)
    and_tag_value_vote(cur, [("highway", "share_busway")], 4)
    and_tag_value_vote(cur, [("maxspeed", "30")], 4)
    and_tag_value_vote(cur, [("zone:maxspeed", "FR:30")], 4)
    and_tag_value_vote(cur, [("maxspeed", "20")], 4)

    ## OK : VOIES VERTES
    and_tag_value_vote(cur, [("highway", "path")], 3)
    and_tag_value_vote(cur, [
        [("highway", "path")],
        [
            ("bicycle","yes"),
            ("bicycle", "designated")
        ],
        [
            ("surface","paved"),
            ("surface","compacted"),
            ("surface","asphalt"),
            ("surface","concrete"),
            ("surface","sett"),
            ("surface","paving_stones"),
            ("surface","concrete:lanes "),
            ("surface","concrete:plates ")
        ]], 4)



    and_tag_value_vote(cur, [("highway", "cycleway")], 3)
    and_tag_value_vote(cur, [("cycleway", "lane")], 3)
    and_tag_value_vote(cur, [("cycleway", "opposite")], 3)
    and_tag_value_vote(cur, [("cycleway", "opposite_lane")], 4)
    and_tag_value_vote(cur, [("cycleway", "track")], 4)
    and_tag_value_vote(cur, [("cycleway", "opposite_track")], 4)
    and_tag_value_vote(cur, [("cycleway", "share_busway")], 4)
    and_tag_value_vote(cur, [("cycleway:right", "share_busway")], 4)
    and_tag_value_vote(cur, [("cycleway:left", "share_busway")], 4)

    and_tag_value_vote(cur, [("highway", "service"),[("bicycle","yes"),("bicycle","designated")]], 4)

    # VOTE 3/5

    and_tag_value_vote(cur, [("highway", "footway")], 3)
    and_tag_value_vote(cur, [("highway", "footway"),[("bicycle","yes"),("bicycle","designated")]], 3)

    ## PAR DEFAUT

    # VOTE 2/5
    and_tag_value_vote(cur, [("surface", "cobblestone")], 3)


    # VOTE 1/5

    and_tag_value_vote(cur, [("maxspeed", "70")], 3)
    and_tag_value_vote(cur, [("maxspeed", "80")], 3)
    and_tag_value_vote(cur, [("maxspeed", "90")], 3)
    and_tag_value_vote(cur, [("maxspeed", "100")], 3)
    and_tag_value_vote(cur, [("maxspeed", "110")], 3)
    and_tag_value_vote(cur, [("maxspeed", "120")], 3)
    and_tag_value_vote(cur, [("maxspeed", "130")], 3)

    # VOTE 0/5 -> mis a 1

    and_tag_value_vote(cur, [("highway", "motorway")], 3)
    and_tag_value_vote(cur, [("highway", "motorway_link")], 3)
    and_tag_value_vote(cur, [("highway", "trunk")], 3)
    and_tag_value_vote(cur, [("highway", "trunk_link")], 3)
    and_tag_value_vote(cur, [("highway", "bus_guideway")], 3)
    and_tag_value_vote(cur, [("highway", "raceway")], 3)

    and_tag_value_vote(cur, [("bicycle", "no")], 3)
    and_tag_value_vote(cur, [("highway", "steps")], 3)
    and_tag_value_vote(cur, [("access", "no")], 3)
    and_tag_value_vote(cur, [("access", "delivery")], 3)
    and_tag_value_vote(cur, [("access", "official")], 3)
    and_tag_value_vote(cur, [("access", "private")], 3)
    and_tag_value_vote(cur, [("service", "parking_aisle")], 3)
    and_tag_value_vote(cur, [("service", "driveway")], 3)
    and_tag_value_vote(cur, [("service", "alley")], 3)
    and_tag_value_vote(cur, [("service", "spur")], 3)
    and_tag_value_vote(cur, [("service", "yard")], 3)
    and_tag_value_vote(cur, [("service", "drive-through")], 3)
    and_tag_value_vote(cur, [("service", "crossover")], 3)
    and_tag_value_vote(cur, [("service", "parking")], 3)
    and_tag_value_vote(cur, [("service", "emergency_access")], 3)
    and_tag_value_vote(cur, [("service", "irrigation")], 3)
    and_tag_value_vote(cur, [("golf_cart", "yes")], 3)
