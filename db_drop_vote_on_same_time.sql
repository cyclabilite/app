﻿
DELETE FROM cyclab_intersection_path_vote WHERE id IN
	(SELECT MIN(id) FROM cyclab_intersection_path_vote
	GROUP BY user_id, profile_id, intersection_path_id, datetime_vote, vote_value HAVING COUNT(*) > 1);

-- DELETE FROM cyclab_intersection_path_vote WHERE id IN
--	(SELECT MIN(id) FROM cyclab_intersection_path_vote
--	GROUP BY user_id, profile_id, intersection_path_id, datetime_vote HAVING COUNT(*) > 1);

-- SELECT * FROM cyclab_segment_vote;

DELETE FROM cyclab_segment_vote WHERE id IN
	(SELECT MIN(id) FROM cyclab_segment_vote
	GROUP BY user_id, profile_id, segment_id, datetime_vote, direction, vote_value HAVING COUNT(*) > 1);


-- DELETE FROM cyclab_segment_vote WHERE id IN
--	(SELECT MIN(id) FROM cyclab_segment_vote
--	GROUP BY user_id, profile_id, segment_id, datetime_vote, direction, vote_value HAVING COUNT(*) > 1);
