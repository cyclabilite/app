<?php

$container->setParameter('locale', (isset($_ENV['LOCALE'])) ? $_ENV['LOCALE'] : 'fr' );

$container->setParameter('mailer_transport', (isset($_ENV['MAILER_TRANSPORT'])) ? $_ENV['MAILER_TRANSPORT'] : 'smtp');
$container->setParameter('mailer_host', (isset($_ENV['MAILER_HOST'])) ? $_ENV['MAILER_HOST'] : 'localhost');
$container->setParameter('mailer_user', (isset($_ENV['MAILER_USER'])) ? $_ENV['MAILER_USER'] : 'cyclabilite');
$container->setParameter('mailer_password', (isset($_ENV['MAILER_PASSWORD'])) ? $_ENV['MAILER_PASSWORD'] : '~');
$container->setParameter('mailer_port', (isset($_ENV['MAILER_PORT'])) ? $_ENV['MAILER_PORT'] : '~');

if (isset($_ENV['AVG_CLASS'])) {
    $container->setParameter('calculate_average_class', $_ENV['AVG_CLASS']);
}

if (isset($_ENV['AVG_CLASS_ARGS'])) {
    $container->setParameter('calculate_average_class_arguments', $_ENV['AVG_CLASS_ARGS']);
}
