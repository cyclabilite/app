#!/bin/bash

#immediatly exit if a command fails:
set -e

echo "Clearing cache"
php /var/www/cyclab/app/console --env=prod cache:clear

echo "Do doctrine:migrations:migrate"
php /var/www/cyclab/app/console  doctrine:migrations:migrate --no-interaction --env=prod

#prepare assets
echo "Do prepare assets (assets:install)"
php /var/www/cyclab/app/console --env=prod assets:install

echo "Do prepare assets (assetic:dump)"
php /var/www/cyclab/app/console --env=prod assetic:dump

chown www-data:www-data /var/www/cyclab/app/cache/prod -R
chown www-data:www-data /var/www/cyclab/app/logs/ -R #nécessaire ?

echo "Exec php-fpm"
php-fpm
