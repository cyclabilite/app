Docker cyclabilite db
=====================

This image must be used with [cyclabilite].

This image extends the [postgresql official repo](https://registry.hub.docker.com/_/postgres/) and add required postgresql extensions to database.

The username, passwords, and database names is documented in the postgresql official repo.