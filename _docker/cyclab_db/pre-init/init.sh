dropdb -U postgres postgres
createdb -U postgres postgres --encoding='utf-8'
mkdir /init/
curl https://cyclabilite.champs-libres.coop/db_dump/db_schema.sql > /init/db_schema.sql
psql -f /init/db_schema.sql -U postgres -d postgres
curl https://cyclabilite.champs-libres.coop/db_dump/db_cyclab_users.sql > /init/db_cyclab_users.sql
psql -U postgres -d postgres -c "COPY cyclab_fos_users (id, username, username_canonical, email, email_canonical, password, enabled, salt,  last_login, confirmation_token,  roles, gender, birthyear, creationdate, isexpert) FROM '/init/db_cyclab_users.sql'"
curl https://cyclabilite.champs-libres.coop/db_dump/db_data.sql > /init/db_data.sql
psql  -v ON_ERROR_STOP=1 -f /init/db_data.sql -U postgres -d postgres
