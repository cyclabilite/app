import subprocess
import os
import time

from db_dump_var import db_port, db_host, db_user, db_pwd, db_name

print("DB_DUMP")
print("start on " + time.strftime("%b %d %Y %H:%M:%S"))
start_t = time.time()
start_c = time.clock()

os.environ['PGPASSWORD'] = db_pwd
rep_db_dump_path = '/var/www/cyclab/web/db_dump/'

filenames = ['db_schema.sql', 'db_data.sql', 'db_cyclab_users.sql']

if(os.path.isfile(rep_db_dump_path + 'timestamp.txt')):
    if(os.path.isfile(rep_db_dump_path + 'timestamp_old.txt')):
        timestamp_old_file = open(rep_db_dump_path + 'timestamp_old.txt', 'r')
        timestamp_old = int(timestamp_old_file.read())
        timestamp_old_file.close()

        for filename in filenames :
             if os.path.isfile(rep_db_dump_path + str(timestamp_old) + '_' + filename):
                 os.remove(rep_db_dump_path + str(timestamp_old) + '_' + filename)

    timestamp_file = open(rep_db_dump_path + 'timestamp.txt', 'r')
    timestamp = int(timestamp_file.read())
    timestamp_file.close()

    os.rename(rep_db_dump_path + 'timestamp.txt', rep_db_dump_path + 'timestamp_old.txt')

    for filename in filenames :
        if os.path.isfile(rep_db_dump_path + filename):
            os.rename(rep_db_dump_path + filename,  rep_db_dump_path + str(timestamp) + '_' + filename)

print(' - db_schema.sql creation')
subprocess.call(['pg_dump', '-U', db_user,'-p', db_port, '-h', db_host, '--schema-only', '-E', 'UTF8', '--create', '-f', \
   (rep_db_dump_path + 'db_schema.sql'), db_name], env=os.environ)

print(' - db_data.sql creation')
subprocess.call(['pg_dump', '-U', db_user,'-p', db_port, '-h', db_host, '--data-only',
   '--exclude-table=cyclab_fos_users', '--exclude-table=cyclab_users',
   '-E',  'UTF8', '-f',  (rep_db_dump_path + 'db_data.sql'), db_name], env=os.environ)

print(' - db_without_cyclab_users.sql creation')
subprocess.call([
   'pg_dump', '-U', db_user, '-p', db_port, '-h', db_host,
   '--exclude-table-data=cyclab_users',
   '--exclude-table-data=cyclab_fos_users', \
   '-E', 'UTF8', '-f', (rep_db_dump_path + 'db_without_cyclab_users.sql'), db_name], env=os.environ)

print(' - db_cyclab_users.sql creation')
stdout_users_file = open(rep_db_dump_path + "db_cyclab_users.sql", 'w+')
subprocess.call(['psql', '-U', db_user,'-p', db_port, '-h', db_host ,'-c', \
   "COPY (SELECT id, username, username_canonical, 'email@email.com', 'email@email.com', 'xxx',  enabled, salt,  last_login, confirmation_token,  roles, gender, birthyear, creationdate, isexpert FROM cyclab_fos_users) TO stdout;", '-d', db_name], \
   stdout= stdout_users_file, env=os.environ)
stdout_users_file.close()

timestamp = int(time.time())
timestamp_file = open(rep_db_dump_path + 'timestamp.txt', 'w')
timestamp_file.write(str(timestamp))
timestamp_file.close()


print("done !")
print(" - time taken (t): " + str(time.time() - start_t))
print(" - time taken (c): " + str(time.clock() - start_c))
print("")

# psql db_name < db_schema.sql
# psql db_name < db_cyclab_users.sql
# psql db_name < db_data.sql
