import os

db_port = os.getenv('DB_PORT_5432_TCP_PORT', '5432')
db_host = os.getenv('DB_PORT_5432_TCP_ADDR', 'db')
db_user = os.getenv('DB_USER', 'postgres')
db_pwd = os.getenv('DB_PASSWORD', 'postgres')
db_name = 'postgres'  # os.getenv('DB_NAME', 'postgres')

f = open('/db_dump_script/db_dump_var.py', 'w')
f.write('db_port = "%s"\n' % (db_port))
f.write('db_host = "%s"\n' % (db_host))
f.write('db_user = "%s"\n' % (db_user))
f.write('db_pwd = "%s"\n' % (db_pwd))
f.write('db_name = "%s"\n' % (db_name))
f.close()
