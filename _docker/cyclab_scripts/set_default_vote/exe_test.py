import unittest
from exe_test import compute_condition, get_condition_for_tuple


class TestSetDefaultVote(unittest.TestCase):
    def test_get_condition_for_tuple(self):
        self.assertEqual(
            get_condition_for_tuple(("highway", "path")), "(w.tags->'highway'='path')")

    def test_compute_condition(self):
        ret = compute_condition([
            [
                [("highway", "pedestrian"), ("maxspeed", "30")],
                [("highway", "path"), ("maxspeed", "30")]
            ],
            [
                [("cycleway", "opposite"), ("cycleway", "opposite_lane"), ("cycleway", "track")],
                [("cycleway", "lane"), ("cycleway", "opposite_track"), ("cycleway", "share_busway")],
            ],
            [
                [("maxspeed", "70")],
                [("maxspeed", "80")],
                [("maxspeed", "90")],
                [("maxspeed", "100")],
                [("maxspeed", "110")],
                [("maxspeed", "120")]
            ]])
        self.assertEqual(ret, "((((w.tags->'highway'='pedestrian') and (w.tags->'maxspeed'='30')) or ((w.tags->'highway'='path') and (w.tags->'maxspeed'='30'))) and (((w.tags->'cycleway'='opposite') and (w.tags->'cycleway'='opposite_lane') and (w.tags->'cycleway'='track')) or ((w.tags->'cycleway'='lane') and (w.tags->'cycleway'='opposite_track') and (w.tags->'cycleway'='share_busway'))) and (((w.tags->'maxspeed'='70')) or ((w.tags->'maxspeed'='80')) or ((w.tags->'maxspeed'='90')) or ((w.tags->'maxspeed'='100')) or ((w.tags->'maxspeed'='110')) or ((w.tags->'maxspeed'='120'))))")

    def test_compute_condition2(self):
        ret = compute_condition([
            [
                [("highway", "pedestrian"), ("maxspeed", "30")],
                [("highway", "path"), ("maxspeed", "30")]
            ],
            [
                [("cycleway", "opposite"), ("cycleway", "opposite_lane"), ("cycleway", "track")],
                [("cycleway", "lane"), ("cycleway", "opposite_track"), ("cycleway", "share_busway")],
            ],
            [
                [("maxspeed", "70")],
                [("maxspeed", "80")],
                [("maxspeed", "90")],
                [("maxspeed", "100")],
                [("maxspeed", "110")],
                [("maxspeed", "120")]
            ]], False)
        self.assertEqual(ret, "((((w.tags->'highway'='pedestrian') or (w.tags->'maxspeed'='30')) and ((w.tags->'highway'='path') or (w.tags->'maxspeed'='30'))) or (((w.tags->'cycleway'='opposite') or (w.tags->'cycleway'='opposite_lane') or (w.tags->'cycleway'='track')) and ((w.tags->'cycleway'='lane') or (w.tags->'cycleway'='opposite_track') or (w.tags->'cycleway'='share_busway'))) or (((w.tags->'maxspeed'='70')) and ((w.tags->'maxspeed'='80')) and ((w.tags->'maxspeed'='90')) and ((w.tags->'maxspeed'='100')) and ((w.tags->'maxspeed'='110')) and ((w.tags->'maxspeed'='120'))))")

if __name__ == '__main__':
    unittest.main()
