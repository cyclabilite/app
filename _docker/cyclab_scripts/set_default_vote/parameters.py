import os

db_port = os.getenv('DB_PORT_5432_TCP_PORT', '5432')
db_host = os.getenv('DB_PORT_5432_TCP_ADDR', 'db')
db_user = os.getenv('DB_USER', 'postgres')
db_pwd = os.getenv('DB_PASSWORD', 'postgres')
db_name = db_user
