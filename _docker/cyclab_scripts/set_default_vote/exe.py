 # -*- coding: utf-8 -*-
import psycopg2
import time
import parameters as p
from rules_to_apply import exe_rules
import sys


def compute_hidden_intersections(cur):
    """
    Compute the hidden intersections :
        - intersection only between hidden segments (with a default_vote of 0)
    """
    print("## START computing the hidden intersections")
    start_t = time.time()
    start_c = time.clock()
    cur.execute("\
        CREATE TEMP TABLE tmp_node_id_with_default_vote ON COMMIT DROP AS \
        SELECT i.id  AS id, GREATEST(MAX(ss.vote_default), MAX(es.vote_default)) as max_default_vote \
        FROM cyclab_intersections AS i \
        LEFT JOIN cyclab_segments AS ss ON ss.nodestart_id = i.id \
        LEFT JOIN cyclab_segments AS es ON es.nodeend_id = i.id \
        GROUP BY i.id;")

    cur.execute("\
        UPDATE cyclab_intersections as i \
        SET vote_default = tmp.max_default_vote \
        FROM tmp_node_id_with_default_vote AS tmp \
        WHERE tmp.id = i.id \
        AND tmp.max_default_vote = 0;")
    print("# END computing the hidden intersections")
    print("# TIME (t): " + str(time.time() - start_t))
    print("# TIME (c): " + str(time.clock() - start_c))
    sys.stdout.flush()
    print("")


def rel_vote(cur, rel_tag_value, vote, condition_link="or"):
    """
    idem que and_tag_value_vote mais pour les ways et semgents qui
    appartiennent a une relation selectionnee par rel_tag_value
    et condition_link
    """
    print("## START REL " + str(rel_tag_value) + " -> " + str(vote))
    start_t = time.time()
    start_c = time.clock()

    condition = "((r.tags->'" + rel_tag_value[0][0] + "') = '" \
        + rel_tag_value[0][1] + "'"

    for (tag, value) in rel_tag_value[1:]:
        condition += " " + condition_link + " (r.tags->'" + tag + "') = '" \
            + value + "'"
    condition += ")"

    cur.execute("UPDATE cyclab_segments AS s SET vote_default = " + str(vote)
                + " FROM relation_members AS rw, relations AS r "
                + " WHERE s.way_id = rw.member_id AND r.id = rw.relation_id "
                + "AND " + condition)

    cur.execute("UPDATE ways as w SET vote_default = " + str(vote)
                + " FROM relation_members AS rw, relations "
                + " AS r WHERE w.id = rw.member_id AND r.id = rw.relation_id "
                + " AND " + condition)

    print("# END REL " + str(rel_tag_value) + " -> " + str(vote))
    print("# TIME (t): " + str(time.time() - start_t))
    print("# TIME (c): " + str(time.clock() - start_c))
    sys.stdout.flush()
    print("")

    
def get_condition_for_tuple(t):
    """
        Pour un couple (key, value) stocké en db hstore, retoure
        la conditions (w.tags->'key'='value')

        (see Tests)
    """
    return "(w.tags->'%s'='%s')" % (t[0], t[1])


def compute_condition(tag_value_array, and_level=True):
    """
        Transforme un tableau de clés/valeurs (se trouvant stocké dans en db dans
        du hstore) en un condition pour postgresql. Le tableau est interprété
        de manière récursive en séparant les élements par des 'and' ou des 
        'or' en alternant à chaque niveau (voir tests qui sont plus parlants)

        (see Tests)
    """
    tag_value_array_0 = tag_value_array[0]

    if(type(tag_value_array_0) == list):
        condition = compute_condition(tag_value_array_0, not and_level)
    else:
        condition = get_condition_for_tuple(tag_value_array_0)

    for tag_value_array_i in tag_value_array[1:]:
        if(type(tag_value_array_i) == list):
            condition_i = compute_condition(tag_value_array_i, not and_level)
        else:
            condition_i = get_condition_for_tuple(tag_value_array_i)

        condition_link = 'and'
        if not and_level:
            condition_link = 'or'
        condition += " %s %s" % (condition_link, condition_i)
    return "(%s)" % (condition)


def and_tag_value_vote(cur, tag_value_array, vote, and_for_first_level=True):
    """
    met $vote dans vote_default pour les tables cyclab_segments et ways tels
    que
        - ways a les tags/value dans tag_value_array (si condition_link="and"
            c'est un and sinon un or)
        - segments appartient a un ways ci dessus
    """
    print("# START WAY " + str(tag_value_array) + " -> " + str(vote))
    start_t = time.time()
    start_c = time.clock()

    condition = compute_condition(tag_value_array, and_for_first_level)
    print("# CONDITION :")
    print("{}".format(condition))

    cur.execute("UPDATE cyclab_segments AS s SET vote_default = " + str(vote)
                + " FROM ways as w  WHERE s.way_id = w.id  and " + condition)

    cur.execute("UPDATE ways as w SET vote_default = " + str(vote)
                + " WHERE " + condition)

    print("# END WAY " + str(tag_value_array) + " -> " + str(vote))
    print("# TIME (t): {}".format(time.time() - start_t))
    print("# TIME (c): {}".format(time.clock() - start_c))
    print("")
    sys.stdout.flush()

if __name__ == '__main__':
    global_start_t = time.time()
    global_start_c = time.clock()

    print("## START DB CONNECTION")
    if p.db_pwd:
        conn = psycopg2.connect(
            database=p.db_name, user=p.db_user, password=p.db_pwd, host=p.db_host)
    else:
        conn = psycopg2.connect(database=p.db_name, user=p.db_user, host=p.db_host)
    cur = conn.cursor()
    print("# END db connection")
    sys.stdout.flush()
    print("")

    exe_rules(cur, rel_vote, and_tag_value_vote)
    compute_hidden_intersections(cur)

    cur.close()
    conn.commit()
    conn.close()
    print("END")
    print("TOTAL TIME (t): " + str(time.time() - global_start_t))
    print("TOTAL TIME (c): " + str(time.clock() - global_start_c))
