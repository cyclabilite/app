import parameters as p
import time
import psycopg2

print "DB CONNECTION"
if p.db_pwd :
  conn = psycopg2.connect(database=p.db_name, user=p.db_user, password=p.db_pwd, host=p.db_host)
else :
  conn = psycopg2.connect(database=p.db_name, user=p.db_user, host=p.db_host)
cur = conn.cursor()
cur2 = conn.cursor()
print "DB CONNECTION END"
print ""


sql = "SELECT id, ST_AsText(linestring) as linestring, tags->'name' as name FROM deleted_ways WHERE resilience_treated = false AND name IS NOT NULL"

cur.execute(sql) 
for row in cur :
   deleted_way_id = row[0]
   deleted_way_linestring = row[1] 
   deleted_way_name = row[2]


   sql = "SELECT w.id, w.nodes as new_id FROM ways w JOIN deleted_ways dw ON ST_DWithin(w.linestring, dw.linestring, 0.02) AND w.tags->'name' = dw.tags->'name' AND dw.id = " + str(deleted_way_id)
   cur2.execute(sql)
   new_ways_id = []
   new_ways_nodes = []
   for row2 in cur2 :
      new_ways_id.append(int(row2[0]))
      new_ways_nodes.append(row2[1])
   if len(new_ways_id) > 0 :
      print '- way  ' + str(deleted_way_id) +  ' -'
      
      sql = "SELECT DISTINCT dw.id as old_id, dw.nodes as old_nodes FROM deleted_ways dw JOIN ways w ON ST_DWithin(w.linestring, dw.linestring, 0.02) AND w.tags->'name' = dw.tags->'name' AND w.id = any(ARRAY" + str(new_ways_id) + ")"
      cur2.execute(sql)

      deleted_ways_id = []
      deleted_ways_nodes = []
      for row2 in cur2 :
         deleted_ways_id.append(int(row2[0]))
         deleted_ways_nodes.append(row2[1])
      print "deleted ways : " + str(deleted_ways_id)
      print "new ways : " + str(new_ways_id)
      print ""
      print "deleted ways - nodes :" + str(deleted_ways_nodes)
      print "new ways - nodes:" + str(new_ways_nodes)
      print ""
      print ""

   """
   print type(deleted_way_linestring)

   print deleted_way_linestring

   print type(deleted_way_tags)

   print deleted_way_tags
   """

"""
print "RESILIENCE cyclab_way_vote"
sql = "SELECT * FROM cyclab_way_vote v JOIN deleted_ways w ON v.way_id = w.id"
cur.execute(sql)
for row in cur :
   


print "RESILIENCE cyclab_intersection_path_vote"
sql = "SELECT * FROM cyclab_intersection_path_vote v JOIN deleted_cyclab_intersection_paths p ON v.intersection_path_id = p.id"
cur.execute(sql)
for row in cur :


print "RESILIENCE cyclab_segment_vote"
sql = "SELECT * FROM cyclab_segment_vote v JOIN deleted_cyclab_segments s ON v.segment_id = s.id"
cur.execute(sql)
for row in cur :
"""


