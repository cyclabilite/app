# -*- coding: utf-8 -*-
import sys
import parameters as p
import time
import subprocess
from xml.etree import ElementTree
import psycopg2
import db_fct

conn = psycopg2.connect(
    database=p.db_name, user=p.db_user,
    password=p.db_pwd, host=p.db_host,
    port=p.db_port)

cur = conn.cursor()


def update_segment(
    cur, way_id, nodestart_id, nodeend_id, nodes_point_list,
    db_current_segments
        ):
    """ Update a segment :
        - update the geometry if the segment still exist after the osmosis update
            ( it is the case if a segment in db_current_segments with the
            same nodestart_id and nodeend_id is founded ;
            when a segment is updated, it is removed form db_current_segments )
        - add the segment if it do not exist after the osmosis update

    Keyword arguments:
    cur -- The postgresql cursor
    way_id --  Id of the way containing the segment (after the update)
    nodestart_id -- Id of the fist node of the segment (after the update)
    nodeend_id --  Id of the last node of the segment (after the update)
    nodes_point_list -- List of the node of the segment (after the update)
    db_current_segments -- list of the segments that are in the db (to be updated)

    at the end db_current_segments contains the list of the segment that were
    not updated (to remove)
    """
    segement_position = -1
    for i in range(0, len(db_current_segments)):
        if (db_current_segments[i][1] == nodestart_id and db_current_segments[i][2] == nodeend_id) or \
            (db_current_segments[i][1] == nodeend_id and db_current_segments[i][2] == nodestart_id) :
            segement_position = i
            break
    if segement_position >= 0:
        db_current_segments.pop(i)
        cur.execute(
            "UPDATE cyclab_segments SET geom = %s WHERE id = %s" %
            (db_fct.sql_line(nodes_point_list), way_id))
    else:
        db_fct.db_add_segment(
            cur, way_id, nodestart_id, nodeend_id, current_segment)

print('BUFFERING NODES')
cur.execute("SELECT id, geom, ways_nbr FROM nodes")
buff_nodes = {}

for row in cur:
    buff_nodes[row[0]] = (row[1], row[2])
print('FIN')

cur.execute("SELECT id, nodes, tags FROM ways WHERE id = 34833028")

cur2 = conn.cursor()

for row in cur:
    way_id = row[0]
    way_nodes = row[1]
    way_tags = row[2]

    db_current_segments = []
    cur2.execute(
        "SELECT id, nodestart_id, nodeend_id FROM cyclab_segments WHERE way_id = " + str(way_id))
    for row in cur2:
        db_current_segments.append(row)

    current_segment = [buff_nodes[way_nodes[0]][0]]
    nodestart_id = way_nodes[0]
    for n in way_nodes[1:]:
        current_segment.append(buff_nodes[n][0])
        nodeend_id = n
        if buff_nodes[n][1] > 1:  # debut d'un segment
            #print(
            update_segment(
                cur2, way_id, nodestart_id, nodeend_id, current_segment,
                db_current_segments)
            current_segment = [buff_nodes[n][0]]
            nodestart_id = n
    if len(current_segment) > 1:
        #print(
        update_segment(
            cur2, way_id, nodestart_id, nodeend_id, current_segment,
            db_current_segments)

    segment_id_to_del = [int(s[0]) for s in db_current_segments]
    
    print segment_id_to_del
    
    if segment_id_to_del:
        save_and_delete_interpath_and_their_votes(cur2, log, segment_id_to_del)
log.end_current_task()

cur2.close()
