"""
Functions used for sql resquest
"""

def sql_point(lon,lat) :
   """
   Return a string that describe a point for a sql request

   Keyword arguments:
   lon -- the longitude of the point 
   lat -- the latitude of the point
   """
   return  "ST_GeometryFromText('POINT(" + str(lon) + " " + str(lat) + ")', 4326)"

def sql_line(postgis_points) :
   """
   Return a string that describe a line for a sql request

   Keyword arguments:
   postgis_points -- an array of sql postgis_points (type geometry)
   """
   if not postgis_points :
      return "ST_MakeLine(ARRAY[])::geometry"

   ret = "ST_MakeLine(ARRAY["
   for p in postgis_points :
      ret += "'" + str(p) + "'::geometry, "
   return ret[:-2] + "])"

def secure_string(s) :
   """
   Return a string that can be used for a sql request :
   - ' are replaced by ''

   Keyword arguments:
   s -- the string to secude
   """
   return s.replace("'","''")

def db_add_segment(cur, way_id, nodestart_id, nodeend_id, nodes_point_list) :
   """Insert a segment into the DB

   Keyword arguments:
   cur -- a psycopg2 cursor
   way_id -- the id of the way
   nodestart_id -- the id of the starting node
   nodeend_id -- the id of the ending node
   nodes_point_list -- the list of the nodes id that represent the segment
   """
   sql = "INSERT INTO cyclab_segments (way_id, nodestart_id, nodeend_id, geom) VALUES (%s, %s, %s, %s);" % \
      (way_id,nodestart_id,nodeend_id, sql_line(nodes_point_list))
   #print sql
   cur.execute(sql)