import os

db_user = 'postgres'
db_host = 'db'
db_port = '5432'
db_pwd = 'postgres'
db_name = 'postgres'

if 'DB_HOST' in os.environ:
    db_host = os.environ['DB_HOST']

print('pg user: {}'.format(db_user))
print('pg host: {}'.format(db_host))
print('pg pwd : {}'.format(db_pwd))
#print(db_pwd)
print('pg name: {}'.format(db_name))
