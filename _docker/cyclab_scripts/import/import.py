import psycopg2
import time
import subprocess
import sys
import db_fct
import parameters as p

if len(sys.argv) <= 1:
    print "usage : python import.py path_to_osm_pbf_file [path to osmosis executable]"
    sys.exit()

osm_pbf_input = sys.argv[1]

if len(sys.argv) >= 3:
    osmosis_exec = sys.argv[2]
else:
    osmosis_exec = "osmosis"

print "START"
start_t = time.time()
start_c = time.clock()
print ""

print "DB CONNECTION"
if p.db_pwd:
    conn = psycopg2.connect(database=p.db_name, user=p.db_user, password=p.db_pwd, host=p.db_host)
else:
    conn = psycopg2.connect(database=p.db_name, user=p.db_user, host=p.db_host)
cur = conn.cursor()
print "END DB CONNECTION"
print ""

print "REMOVING CONSTRAINT"

cur.execute("ALTER TABLE cyclab_intersection_paths DROP CONSTRAINT IF EXISTS FK_cyclab_intersection_paths_node_id")
cur.execute("ALTER TABLE cyclab_segments DROP CONSTRAINT IF EXISTS FK_cyclab_segments_way_id")
cur.execute("ALTER TABLE cyclab_segments DROP CONSTRAINT IF EXISTS FK_cyclab_segments_nodeStart_id")
cur.execute("ALTER TABLE cyclab_segments DROP CONSTRAINT IF EXISTS FK_cyclab_segments_nodeEnd_id")
# cur.execute("ALTER TABLE cyclab_way_vote DROP CONSTRAINT FK_cyclab_way_vote_way_id_fkey")


cur.close()
conn.commit()

print "IMPORT OSMOSIS - WAYS"
subprocess.call([
    osmosis_exec, "--read-pbf", osm_pbf_input,
    "--tf", "accept-ways", "highway=*",
    "--tf", "reject-ways", "highway=motorway,motorway_link,trunk,trunk_link,bus_guideway,raceway,steps",
    "--tf", "reject-ways", "bicycle=no",
    "--tf", "reject-ways", "access=private",
    "--tf", "reject-relations", "--used-node", "outPipe.0=highway",
    "--read-pbf", osm_pbf_input, "--tf", "reject-nodes",
    "--tf", "reject-ways", "--tf", "accept-relations", "network=lcn,rcn,ncn", "outPipe.0=cycle_network",
    "--merge", "inPipe.0=highway", "inPipe.1=cycle_network",
    "--write-pgsql", "host=" + p.db_host,
    "database=" + p.db_name, "user=" + p.db_user, "password=" + p.db_pwd])

end_osmosis_t = time.time()
end_osmosis_c = time.clock()
print "END IMPORT OSMOSIS - WAYS"
print "time (t): " + str(end_osmosis_t - start_t)
print "time (c): " + str(end_osmosis_c - start_c)
print ""

print "DB CONNECTION"
if p.db_pwd:
    conn = psycopg2.connect(database=p.db_name, user=p.db_user, password=p.db_pwd, host=p.db_host)
else:
    conn = psycopg2.connect(database=p.db_name, user=p.db_user, host=p.db_host)
cur = conn.cursor()
print "END DB CONNECTION"
print ""

print "RE-ADDING CONSTRAINT"
cur.execute("ALTER TABLE cyclab_intersection_paths ADD CONSTRAINT FK_cyclab_intersection_paths_node_id FOREIGN KEY (node_id) REFERENCES nodes (id) NOT DEFERRABLE INITIALLY IMMEDIATE")
cur.execute("ALTER TABLE cyclab_segments ADD CONSTRAINT FK_cyclab_segments_way_id FOREIGN KEY (way_id) REFERENCES ways (id) NOT DEFERRABLE INITIALLY IMMEDIATE")
cur.execute("ALTER TABLE cyclab_segments ADD CONSTRAINT FK_cyclab_segments_nodeStart_id FOREIGN KEY (nodeStart_id) REFERENCES nodes (id) NOT DEFERRABLE INITIALLY IMMEDIATE")
cur.execute("ALTER TABLE cyclab_segments ADD CONSTRAINT FK_cyclab_segments_nodeEnd_id FOREIGN KEY (nodeEnd_id) REFERENCES nodes (id) NOT DEFERRABLE INITIALLY IMMEDIATE")
# cur.execute("ALTER TABLE cyclab_way_vote ADD CONSTRAINT FK_cyclab_way_vote_way_id_fkey FOREIGN KEY (way_id) REFERENCES ways (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION")

print "TMP NODES TABLE CREATION"
cur.execute("CREATE TEMPORARY TABLE tmp_nodes (\
   id BIGINT NOT NULL, \
   ways_nbr BIGINT NOT NULL, \
   PRIMARY KEY(id)); \
   \
   INSERT INTO tmp_nodes (id, ways_nbr) \
   SELECT n.id AS id, COUNT(w.id) AS ways_nbr \
      FROM nodes AS n \
      JOIN ways as w ON n.id = ANY(w.nodes) \
      GROUP BY n.id \
      HAVING  COUNT(w.id) > 1;")

end_tmp_nodes_creation_t = time.time()
end_tmp_nodes_creation_c = time.clock()
print "END TMP NODES TABLE CREATION"
print "time (t): " + str(end_tmp_nodes_creation_t - end_osmosis_t)
print "time (c): " + str(end_tmp_nodes_creation_c - end_osmosis_c)
print ""


print "UPDATING NODES TABLE"
cur.execute("UPDATE nodes AS n \
   SET ways_nbr = tmp.ways_nbr \
   FROM tmp_nodes as tmp WHERE n.id = tmp.id AND tmp.ways_nbr > 1")

end_updating_nodes_way_nbr_t = time.time()
end_updating_nodes_way_nbr_c = time.clock()
print "END UPDATING NODES TABLE"
print "time (t): " + str(end_updating_nodes_way_nbr_t - end_tmp_nodes_creation_t)
print "time (c): " + str(end_updating_nodes_way_nbr_c - end_tmp_nodes_creation_c)
print ""


print "BUFFERING NODES"
cur.execute("SELECT id, geom, ways_nbr FROM nodes")
buff_nodes = {}

for row in cur:
    buff_nodes[row[0]] = (row[1], row[2])

end_buffering_nodes_t = time.time()
end_buffering_nodes_c = time.clock()
print "END BUFFERING NODES"
print "time (t): " + str(end_buffering_nodes_t - end_updating_nodes_way_nbr_t)
print "time (c): " + str(end_buffering_nodes_c - end_updating_nodes_way_nbr_c)
print ""

print "ADDING SEGMENTS"
cur.execute("SELECT id, nodes, tags FROM ways")
cur2 = conn.cursor()

for row in cur:
    way_id = row[0]
    way_nodes = row[1]
    way_tags = row[2]
    current_segment = [buff_nodes[way_nodes[0]][0]]
    nodestart_id = way_nodes[0]
    for n in way_nodes[1:]:
        current_segment.append(buff_nodes[n][0])
        nodeend_id = n
        if buff_nodes[n][1] > 1:
            db_fct.db_add_segment(cur2, way_id, nodestart_id, nodeend_id, current_segment)
            current_segment = [buff_nodes[n][0]]
            nodestart_id = n
    if len(current_segment) > 1:
        db_fct.db_add_segment(cur2, way_id, nodestart_id, nodeend_id, current_segment)

end_create_cyclab_segments_t = time.time()
end_create_cyclab_segments_c = time.clock()
print "END ADDING SEGMENTS"
print "time (t): " + str(end_create_cyclab_segments_t - end_buffering_nodes_t)
print "time (c): " + str(end_create_cyclab_segments_c - end_buffering_nodes_c)
print ""

cur.close()
cur2.close()
conn.commit()
conn.close()
print "END"
print "TOTAL TIME (t): " + str(time.time() - start_t)
print "TOTAL TIME (c): " + str(time.clock() - start_c)
