# -*- coding: utf-8 -*-
import sys
import parameters as p
import time
import subprocess
from xml.etree import ElementTree
import psycopg2
import db_fct


class TaskTimeLogger:
    """Compute and print the computation time taken by a section of the code"""

    def __init__(self):
        print('START')
        print('')
        self.global_start_t = time.time()
        self.global_start_c = time.clock()

    def end(self):
        print('END')
        print("time (t): {}".format(time.time() - self.global_start_t))
        print("time (c): {}".format(time.clock() - self.global_start_c))
        print('')

    def start(self, task_name):
        self.task_start_t = time.time()
        self.task_start_c = time.clock()
        self.task_name = task_name
        print('STARTING {}'.format(task_name))

    def end_current_task(self):
        task_end_t = time.time()
        task_end_c = time.clock()
        print('{} END'.format(self.task_name))
        print("time (t): {}".format(task_end_t - self.task_start_t))
        print("time (c)): {}".format(task_end_c - self.task_start_c))
        print('')


class CyclabDB:
    """Code for dealing with a cyclab db"""

    def __init__(self, p, log):
        """p is the parameters"""
        self.p = p
        self.log = log

    def connect(self):
        self.log.start('DB CONNECTION')
        if self.p.db_pwd:
            self.conn = psycopg2.connect(
                database=self.p.db_name, user=self.p.db_user,
                password=self.p.db_pwd, host=self.p.db_host,
                port=self.p.db_port)
        else:
            self.conn = psycopg2.connect(
                database=self.p.db_name, user=self.p.db_user,
                host=self.p.db_host, port=self.p.db_port)
        self.log.end_current_task()
        self.cur = self.conn.cursor()
        return self.cur

    def new_cursor(self):
        return self.conn.cursor()

    def disconnect(self):
        self.cur.close()
        self.conn.commit()
        self.conn.close()


def save_and_delete_segments_and_their_votes(cur, log, deleted_segment_id):
    """Save (in a database called 'deleted_*') the segments having in
    delete, their associated intersection_paths and the votes for the deleted
    entities.

    Keyword arguments:
    cur -- a postgresql cursor
    log -- a logger
    deleted_segment_id -- a list of the id of the segments to delete
    """

    # pas besoin de partir des deleteds_nodes car fait a partir des segments
    if deleted_segment_id:
        log.start('COMPUTE DELETED INTERSECTION PATH')

        sql = "SELECT id FROM cyclab_intersection_paths \
            WHERE segmentstart_id = any(ARRAY" + str(deleted_segment_id) + ") \
            OR segmentend_id = any(ARRAY" + str(deleted_segment_id) + ")   ;"
        cur.execute(sql)
        deleted_intersection_path_id = [int(row[0]) for row in cur]
        log.end_current_task()

        save_and_delete_interpath_and_their_votes(
            cur, log, deleted_intersection_path_id)

    log.start('SAVE AND DELETE SEGMENTS AND THEIR VOTES')
    if deleted_segment_id:
        cur.execute("INSERT INTO deleted_cyclab_segment_vote(id, vote_value, user_id, profile_id, datetime_vote, datetime_creation, client, localisation, details, is_expert_vote, segment_id, direction) \
            SELECT id, vote_value, user_id, profile_id, datetime_vote, datetime_creation, client, localisation, details, is_expert_vote, segment_id, direction FROM cyclab_segment_vote WHERE segment_id = any(ARRAY" + str(deleted_segment_id) + ");")
        cur.execute("DELETE FROM cyclab_segment_vote \
            WHERE segment_id = any(ARRAY" + str(deleted_segment_id) + ");")

        cur.execute("INSERT INTO deleted_cyclab_segments \
            SELECT * FROM cyclab_segments WHERE id = any(ARRAY" + str(deleted_segment_id) + ");")
        cur.execute("DELETE FROM cyclab_segments \
            WHERE id = any(ARRAY" + str(deleted_segment_id) + ");")
    log.end_current_task()


def save_and_delete_interpath_and_their_votes(cur, log, deleted_intersection_path_id):
    """Save (in a database called 'deleted_*') the intersection_paths and
    their associated votes and delete them for the database.

    Keyword arguments:
    cur -- the postgresql cursor
    log -- the logger
    deleted_intersection_path_id -- a list of the intersections_paths id to delete
    """
    log.start('SAVE AND DELETE INTERSECTION PATHS AND THEIR VOTES')
    if deleted_intersection_path_id:
        cur.execute("INSERT INTO deleted_cyclab_intersection_path_vote(id, vote_value, user_id, profile_id, datetime_vote, datetime_creation, client, localisation, details, is_expert_vote, intersection_path_id) \
            SELECT id, vote_value, user_id, profile_id, datetime_vote, datetime_creation, client, localisation, details, is_expert_vote, intersection_path_id FROM cyclab_intersection_path_vote WHERE intersection_path_id = any(ARRAY" + str(deleted_intersection_path_id) + ");")
        cur.execute("DELETE FROM cyclab_intersection_path_vote \
            WHERE intersection_path_id = any(ARRAY" + str(deleted_intersection_path_id) + ");")

        cur.execute("INSERT INTO deleted_cyclab_intersection_paths \
           SELECT * FROM cyclab_intersection_paths WHERE id = any(ARRAY" + str(deleted_intersection_path_id) + ");")
        cur.execute("DELETE FROM cyclab_intersection_paths \
           WHERE id = any(ARRAY" + str(deleted_intersection_path_id) + ");")
    log.end_current_task()


def save_omosis_deleted_entities(cur, log, deleted_nodes, deleted_ways):
    """Save all the entities that will be deleted by osmosis : the nodes,
    the ways, the associated intersection_paths, the associated segments and
    their associated votes

    For the associated elements, the elements are deleted form their original
    table. For the nodes and ways, osmosis will do it

    Keyword arguments:
    cur -- the postgres cursor
    deleted_nodes -- an array of deleted nodes id
    deleted_ways -- an array of the deleted ways id
    """
    log.start('COMPUTE DELETED SEGMENTS')
    sql = "SELECT id FROM cyclab_segments WHERE way_id = any(ARRAY" + str(deleted_ways) + ");"
    cur.execute(sql)
    deleted_segment_id = [int(row[0]) for row in cur]
    log.end_current_task()

    log.start('COPY DELETED NODES')
    sql = "INSERT INTO deleted_nodes SELECT * FROM nodes WHERE id = any(ARRAY" + str(deleted_nodes) + ");"
    cur.execute(sql)
    log.end_current_task()

    log.start('COPY DELETED WAYS')
    """ SINON VONT ETRE SUPPRIMER PAR OSMOSIS """
    sql = "INSERT INTO deleted_ways SELECT * FROM ways WHERE id = any(ARRAY" + str(deleted_ways) + ");"
    cur.execute(sql)
    log.end_current_task()

    save_and_delete_segments_and_their_votes(cur, log, deleted_segment_id)


def exe_osmosis_update(log):
    """Start the osmosis update procedure

    Keyword arguments:
    - log -- osmosis updating
    """
    log.start('OSMOSIS UPDATING')
    subprocess.call([
        osmosis_exec, "--read-xml-change", tmp_osc_filename,
        "--write-pgsql-change", "host={}:{}".format(p.db_host, p.db_port),
        "database={}".format(p.db_name), "user={}".format(p.db_user),
        "password={}".format(p.db_pwd)])
    log.end_current_task()


def update_segment(
    cur, way_id, nodestart_id, nodeend_id, nodes_point_list,
    db_current_segments
        ):
    """ Update a segment :
        - update the geometry if the segment still exist after the osmosis update
            ( it is the case if a segment in db_current_segments with the
            same nodestart_id and nodeend_id is founded ;
            when a segment is updated, it is removed form db_current_segments )
        - add the segment if it do not exist after the osmosis update

    Keyword arguments:
    cur -- The postgresql cursor
    way_id --  Id of the way containing the segment (after the update)
    nodestart_id -- Id of the fist node of the segment (after the update)
    nodeend_id --  Id of the last node of the segment (after the update)
    nodes_point_list -- List of the node of the segment (after the update)
    db_current_segments -- list of the segments that are in the db (to be updated)

    at the end db_current_segments contains the list of the segment that were
    not updated (to remove)
    """
    segement_position = -1
    for i in range(0, len(db_current_segments)):
        if (db_current_segments[i][1] == nodestart_id and db_current_segments[i][2] == nodeend_id) or \
            (db_current_segments[i][1] == nodeend_id and db_current_segments[i][2] == nodestart_id) :
            segement_position = i
            break
    if segement_position >= 0:
        db_current_segments.pop(i)
        cur.execute(
            "UPDATE cyclab_segments SET geom = %s WHERE id = %s" %
            (db_fct.sql_line(nodes_point_list), way_id))
    else:
        db_fct.db_add_segment(
            cur, way_id, nodestart_id, nodeend_id, current_segment)


"""
- problème : déplacer tous les votes dans une colonne deleted
"""

log = TaskTimeLogger()
db = CyclabDB(p, log)

tmp_pbf_filename = "TMP.pbf"
tmp_osc_filename = "TMP-diff.osc"

if len(sys.argv) <= 1:
    print "usage : python update.py path_to_osm_pbf_file [path to osmosis]"
    sys.exit()

osm_pbf_input = sys.argv[1]

if len(sys.argv) >= 3:
    osmosis_exec = sys.argv[2]
else:
    osmosis_exec = "osmosis"


if __name__ == '__main__':

    log.start('OSM FILE CREATION')
    print('CREATION DE ' + tmp_osc_filename + ' non faite\n')

    subprocess.call([
        osmosis_exec, "--read-pgsql", "database={}".format(p.db_name),
        "user={}".format(p.db_user), "password={}".format(p.db_pwd),
        "host={}:{}".format(p.db_host, p.db_port), "outPipe.0=pg", "--dd",
        "inPipe.0=pg", "outPipe.0=dd", "--write-pbf", "inPipe.0=dd",
        "file={}".format(tmp_pbf_filename)])

    subprocess.call([
        osmosis_exec, "--read-pbf", osm_pbf_input, "--tf", "accept-ways",
        "highway=*", "--tf", "reject-relations", "--used-node",
        "--read-pbf", tmp_pbf_filename, "--derive-change", "--write-xml-change",
        "file={}".format(tmp_osc_filename)])

    log.end_current_task()

    log.start('BUFFERING TMP OSC FILE')
    nodes = {}
    nodes['delete'] = []
    nodes['modify'] = []
    nodes['create'] = []
    ways = {}
    ways['delete'] = []
    ways['modify'] = []
    ways['create'] = []

    action = ''

    for (event, elem) in ElementTree.iterparse(tmp_osc_filename, events=['start']):
        if elem.tag == 'delete' or elem.tag == 'modify' or elem.tag == 'create':
            action = elem.tag
        else:
            if elem.tag == 'node':
                nodes[action].append(int(elem.attrib['id']))
            elif elem.tag == 'way':
                ways[action].append(int(elem.attrib['id']))

    print(len(nodes['delete']))
    print(len(nodes['modify']))
    print(len(nodes['create']))
    print(len(ways['delete']))
    print(len(ways['modify']))
    print(len(ways['create']))
    log.end_current_task()

    cur = db.connect()

    # TO RESTORE
    save_omosis_deleted_entities(cur, log, nodes['delete'], ways['delete'])

    # SQL [DELETE FROM nodes WHERE id = ?]; ERROR: update or delete on table "nodes" violates foreign key constraint "fk_cyclab_segments_nodeend_id" on table "cyclab_segments"
    # voir import.py

    # a remettre qd fini retirer intesection_path
    cur.execute("ALTER TABLE cyclab_intersection_paths DROP CONSTRAINT IF EXISTS FK_cyclab_intersection_paths_node_id")

    # a remettre qd fini segments
    cur.execute("ALTER TABLE cyclab_segments DROP CONSTRAINT IF EXISTS FK_cyclab_segments_way_id")
    cur.execute("ALTER TABLE cyclab_segments DROP CONSTRAINT IF EXISTS FK_cyclab_segments_nodeStart_id")
    cur.execute("ALTER TABLE cyclab_segments DROP CONSTRAINT IF EXISTS FK_cyclab_segments_nodeEnd_id")

    # TODO reajouter les contraintes

    # a remettre à la fin
    # créer une table de deleted_cyclab_way_vote
    # cur.execute("ALTER TABLE cyclab_way_vote DROP CONSTRAINT FK_cyclab_way_vote_way_id_fkey")


    # TO RESTORE
    db.disconnect()

    exe_osmosis_update(log)

    cur = db.connect()


    """ Recalculer le nombre de chemin qui passe par un noeud"""

    log.start('TMP NODES TABLE CREATION')
    # doit se faire sur tous les noeuds

    # TO DELETE
    # nodes_to_process = nodes['modify'][:]  # copy de nodes modify
    # nodes_to_process.extend(nodes['create'])  # mis dans un meme tableau car meme procedure
    #
    # print(len(nodes_to_process))

    cur.execute("CREATE TEMPORARY TABLE tmp_nodes (\
       id BIGINT NOT NULL, \
       ways_nbr BIGINT NOT NULL, \
       PRIMARY KEY(id)); \
       \
       INSERT INTO tmp_nodes (id, ways_nbr) \
       SELECT n.id AS id, COUNT(w.id) AS ways_nbr \
          FROM nodes AS n \
          JOIN ways as w ON n.id = ANY(w.nodes) \
          GROUP BY n.id \
          ")
    #     HAVING  COUNT(w.id) > 1;"
    #             )
    log.end_current_task()


    log.start('UPDATING NODES TABLE')
    cur.execute("UPDATE nodes AS n \
       SET ways_nbr = tmp.ways_nbr \
       FROM tmp_nodes as tmp WHERE n.id = tmp.id")
       # AND tmp.ways_nbr > 1 "sinon si ajout d'un way sur un point, pas de détection"
       # AND n.id  = any(ARRAY" + str(nodes_to_process) + ")")
    log.end_current_task()


    log.start('BUFFERING NODES')
    cur.execute("SELECT id, geom, ways_nbr FROM nodes")
    buff_nodes = {}

    for row in cur:
        buff_nodes[row[0]] = (row[1], row[2])
    log.end_current_task()


    """ Debug """

    log.start('UPDATING SEGMENTS - CREATE')
    """POUR LES WAYS CREES -> AJOUT DES NOUVEAUX SEGMENTS"""
    """LES INTERSECTION ONT ETES RAJOUTES"""
    """AVOIR LE MEME CODE QU'IMPORT?"""
    """ EN FAIT IMPORT = UPDATE MAIS AVEC RIEN AU DEBUT"""
    cur.execute("SELECT id, nodes, tags FROM ways WHERE id = any(ARRAY[" + str(ways['create']) + "])")
    cur2 = db.new_cursor() # comment to debug

    for row in cur:
        way_id = row[0]
        way_nodes = row[1]
        way_tags = row[2]
        current_segment = [buff_nodes[way_nodes[0]][0]]
        nodestart_id = way_nodes[0]
        for n in way_nodes[1:]:
            current_segment.append(buff_nodes[n][0])
            nodeend_id = n
            if buff_nodes[n][1] > 1:
                db_fct.db_add_segment( # comment to debug
                    # print('ADD NEW SEGMENT w_id: {}, nodestart_id: {}, nodeend_id: {} current semgnet: {}'.format(
                    cur2, # comment to debug
                    way_id, nodestart_id, nodeend_id, current_segment)
                    #)
                current_segment = [buff_nodes[n][0]]
                nodestart_id = n
        if len(current_segment) > 1:
            db_fct.db_add_segment(  # comment to debug
                # print('ADD NEW SEGMENT w_id: {}, nodestart_id: {}, nodeend_id: {} current semgnet: {}'.format(
                cur2,  # comment to debug
                way_id, nodestart_id, nodeend_id, current_segment)
                #)
    cur2.close()  # comment to debug
    log.end_current_task()

    # """ debug
    print "MAJ SEGEMNTS NODES TABLE"
    print "TODO MODIFY"
    # fait en fct du way : si supprimer -> on supprime le segment
    # si ajouter on ajouter le segment
    # si modifier on repasse sur les segments


    log.start("UPDATING SEGMENTS - MODIFY")
    cur.execute(
        "SELECT id, nodes, tags FROM ways WHERE id = any(ARRAY[" +
        str(ways['modify']) + "])")
    cur2 = db.new_cursor()

    for row in cur:
        way_id = row[0]
        way_nodes = row[1]
        way_tags = row[2]

        db_current_segments = []
        cur2.execute(
            "SELECT id, nodestart_id, nodeend_id FROM cyclab_segments WHERE way_id = " + str(way_id))
        for row in cur2:
            db_current_segments.append(row)

        current_segment = [buff_nodes[way_nodes[0]][0]]
        nodestart_id = way_nodes[0]
        for n in way_nodes[1:]:
            current_segment.append(buff_nodes[n][0])
            nodeend_id = n
            if buff_nodes[n][1] > 1:  # debut d'un segment
                update_segment(
                    cur2, way_id, nodestart_id, nodeend_id, current_segment,
                    db_current_segments)
                current_segment = [buff_nodes[n][0]]
                nodestart_id = n
        if len(current_segment) > 1:
            update_segment(
                cur2, way_id, nodestart_id, nodeend_id, current_segment,
                db_current_segments)

        segment_id_to_del = [int(s[0]) for s in db_current_segments]
        if segment_id_to_del:
            save_and_delete_segments_and_their_votes(cur2, log, segment_id_to_del)
    log.end_current_task()

    cur2.close()
    # """

    db.disconnect()

    # subprocess.call(["rm", tmp_osc_filename])

    log.end()
