DROP TABLE IF EXISTS nodes, ways, segments, intersection_paths, migration_versions, way_node;
DROP SEQUENCE IF EXISTS nodes_id_seq, ways_id_seq, segments_id_seq, intersection_paths_id_seq;