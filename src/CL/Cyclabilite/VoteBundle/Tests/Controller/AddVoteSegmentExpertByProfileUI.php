<?php

namespace CL\Cyclabilite\VoteBundle\Tests\Controller;

use CL\Cyclabilite\UserBundle\Tests\Helper\LoginHelper;
use CL\Cyclabilite\VoteBundle\Entity\VoteSegment;

/**
 * Test the ability to add a new vote for a segment 
 * with the environment 'test_expert_average_one_profile_one_vote'
 *
 * @todo choose an entity with no vote then test it (+ add vote)
 * @author Champs-Libres <marc.ducobu@champs-libres.coop>
 */
class AddVoteSegmentExpertByProfileUI extends AddVoteSegmentExpertByProfileCore
{
    protected $container;
    protected $em;
    protected $faker;
    protected $segment;
    protected $client;
    protected $user;
    protected $user1;
    protected $user2;
    protected $form;
    
    protected static $segmentId;
    
    public function testSend()
    {
        $this->initConnexion();
        $this->client = static::createClient(array('environment' => 'test_expert_average_one_profile_one_vote'));
        $this->initSegmentUsers(self::MODE_U1_EXPERT_U2_NORMAL);

        $this->user = LoginHelper::authenticateClient($this->client, $this->container);

        $now = new \DateTime('now');
        $profiles = $this->user->getProfiles();
        $profileSelected = $profiles->get(array_rand($profiles->getKeys()));
                
        $crawler = $this->client->request('GET', '/votes/form/segment/'.static::$segmentId);

        $form = $crawler->filter('form')->form();
        $form['vote_segment[value]'] = rand(1, 5);
        $form['vote_segment[profile]'] = $profileSelected->getId();

        $valuesDirection = [
            VoteSegment::DIRECTION_BOTH, VoteSegment::DIRECTION_FORWARD, VoteSegment::DIRECTION_BACKWARD];
        $form['vote_segment[direction]'] = $valuesDirection[array_rand($valuesDirection)];
              
        $this->client->submit($form);
        $response = $this->client->getResponse();
        
        $this->assertTrue($response->isSuccessful());
        
        $responseData = json_decode($response->getcontent(), true);
        $avgAndCount = $this->sqlComputeAvgVoteAndCountVoteForSegment();
        
        $this->assertEquals($responseData['feature']['properties']['vote_number'], $avgAndCount['count']);
        $this->assertEquals($responseData['feature']['properties']['vote_average'], $avgAndCount['avg']);
    }
}
