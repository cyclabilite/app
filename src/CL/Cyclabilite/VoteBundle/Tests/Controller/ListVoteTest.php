<?php

namespace CL\Cyclabilite\VoteBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Description of ListVoteTest
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class ListVoteTest extends WebTestCase
{

   public function setUp()
   {
      $kernel = static::createKernel();
      $kernel->boot();

      $this->container = $kernel->getContainer();
   }

   public function testListVotesSegment()
   {
      $em = $this->container->get('doctrine')
            ->getManager();

      try {
         $segment = $em->createQuery('SELECT s '
               . 'FROM CLCyclabiliteVoteBundle:Segment s '
               . 'WHERE SIZE(s.votes) >= 1')
               ->setMaxResults(1)
               ->getSingleResult();
      } catch ( Doctrine\ORM\NonUniqueResultException $e ) {
         throw new \Exception('There is no segment where number of votes > 1. '
                 . 'HINT: execute the test suite AddVoteSegmentTest a couple of '
                 . 'timeto add some votes',
                 0, $e);
      }


      $client = static::createClient();
      $client->request('GET', '/votes/list/segment/'.$segment->getId().'.json');

      $this->assertTrue($client->getResponse()->isSuccessful());
      $this->assertTrue($client->getResponse()
            ->headers->contains('Content-Type', 'application/json'));

      $response = $client->getResponse()->getContent();

      $responseDecoded = json_decode($response);

      $this->assertGreaterThan(0, count($responseDecoded));
   }

   /**
    * Test if the query ($_GET) parameters (limit & offset) work well
    */
   public function testListVotesSegmentLength()
   {
      $segment = $this->container->get('doctrine')->getManager()
            ->createQuery('SELECT s '
                  . 'FROM CLCyclabiliteVoteBundle:Segment s '
                  . 'WHERE SIZE(s.votes) > 2')
           ->setMaxResults(1)
           ->getSingleResult();

      $client = static::createClient();
      $client->request('GET', '/votes/list/segment/'.$segment->getId().'.json',
            array('limit' => 2));

      $responseDecoded = json_decode($client->getResponse()->getContent(), true);
      $this->assertEquals(2, count($responseDecoded),
            'assert that the limit of response === the parameter "limit"');

      $secondId = $responseDecoded[1]['id'];

      $client->request('GET', '/votes/list/segment/'.$segment->getId().'.json',
            array('limit' => 1, 'offset' => 1));

      $responseDecoded = json_decode($client->getResponse()->getContent(), true);
      $firstId = $responseDecoded[0]['id'];

      $this->assertEquals($secondId, $firstId,
            'assert that the id of the second vote is moved to first position '
            . 'when using offset parameter');
   }

   public function testListVotesSegmentOutOfLimit()
   {
      $segment = $this->container->get('doctrine')->getManager()
            ->createQuery('SELECT s '
                  . 'FROM CLCyclabiliteVoteBundle:Segment s '
                  . 'WHERE SIZE(s.votes) > 2')
           ->setMaxResults(1)
           ->getSingleResult();

      $client = static::createClient();
      $client->request('GET', '/votes/list/segment/'.$segment->getId().'.json',
            array('limit' => 101));

      $this->assertFalse($client->getResponse()->isSuccessful());
      $this->assertEquals(400, $client->getResponse()->getStatusCode());
   }
}
