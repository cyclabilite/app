<?php

namespace CL\Cyclabilite\VoteBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\Query\ResultSetMapping;

/**
 * Test the ability to send a post request to
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class AddVoteIntersectionPathTest extends AbstractControllerTest
{
    private static $container;
    private static $user;
    //private static $client;
    private static $node;
    private static $intersectionPath;
    private static $voteNumberNodeBeforeTest;
    private $form;
    private $way;

    public static function setUpBeforeClass()
    {
        //$client = static::createClient();

        $kernel = static::createKernel();
        $kernel->boot();

        static::$container = $kernel->getContainer();

        //static::$user = LoginHelper::authenticateClient($client, static::$container);

        //static::$client = $client;

        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('nid', 'nid');

        $nodeId = static::$container->get('doctrine')->getManager()
            ->createNativeQuery('SELECT nid, total FROM
               (SELECT count(cyclab_segments.nodestart_id)
               + COUNT(cyclab_segments.nodeend_id) AS total, nodestart_id AS nid
               FROM cyclab_segments GROUP BY nodestart_id) as sb1
               WHERE sb1.total > 2 LIMIT 1 OFFSET :firstResult', $rsm)
            ->setParameter('firstResult', rand(0, 15))
            ->getSingleResult();

        static::$node = static::$container->get('doctrine')->getManager()
            ->getRepository('CLCyclabiliteVoteBundle:Node')
            ->find($nodeId['nid']);

        static::$voteNumberNodeBeforeTest = static::$node->getVoteNumber();
    }

    public function setUp()
    {
        $now = new \DateTime('now');
        $profiles = static::$user->getProfiles();
        $profileSelected = $profiles->get(array_rand($profiles->getKeys()));

        $crawler = $this->client->request(
            'GET',
            '/votes/form/intersection/'.static::$node->getId()
        );

        $form = $crawler->filter('form')->form();

        $form['vote_intersection[value]'] = rand(1, 5);
        $form['vote_intersection[profile]'] = $profileSelected->getId();

        //pick two different segments for start and end
        $segmentsStartValues = $form->get('vote_intersection[segmentStart]')
            ->availableOptionValues();
        $valueStart = $segmentsStartValues[array_rand($segmentsStartValues)];

        $segmentEndValues = $form->get('vote_intersection[segmentEnd]')
            ->availableOptionValues();
        $segmentEndValuesFiltered =
            array_filter(
                $segmentEndValues,
                function ($var) use ($valueStart) {
                    return !($var === $valueStart);
                }
            );
        $valueEnd = $segmentEndValuesFiltered[array_rand($segmentEndValuesFiltered)];

        $form['vote_intersection[segmentStart]'] = $valueStart;
        $form['vote_intersection[segmentEnd]'] = $valueEnd;

        $this->form = $form;
    }

    public function testValidVoteIntersectionPath()
    {
        $this->markTestSkipped('Must be revisited : to check the normal voting procedure!');

        $this->client->submit($this->form);

        $this->assertTrue($this->client->getResponse()->isSuccessful());
    }

    /**
     * @depends testValidVoteIntersectionPath
     */
    public function testAverageAndCount()
    {
        $this->markTestSkipped('Must be revisited : to check the normal voting procedure!');

        $em = static::$container->get('doctrine')->getManager();

        $node = static::$node;
        $em->refresh($node);

        if ($node === null) {
            throw new Exception("way should not be null");
        }

        $intersectionPaths = $em
            ->getRepository('CLCyclabiliteVoteBundle:IntersectionPath')
            ->findBy(array('node' => $node));

        $infos = $em
            ->createQuery('SELECT count(v), avg(v.value) '
                . 'FROM CLCyclabiliteVoteBundle:VoteIntersectionPath v '
                . 'JOIN v.intersectionPath i '
                . 'WHERE v.intersectionPath IN (:paths) '
                . 'GROUP BY i.node')
            ->setParameter('paths', $intersectionPaths)
            ->getSingleResult();

        $this->assertEquals($infos[1], $node->getVoteNumber());
        $this->assertEquals($infos[2], $node->getVoteAverage());
        $this->assertEquals(static::$voteNumberNodeBeforeTest + 1, $node->getVoteNumber());
        $this->assertEquals(static::$voteNumberNodeBeforeTest + 1, $infos[1]);

        return $intersectionPaths;
    }

   /**
    * @depends testAverageAndCount
    * @param \CL\Cyclabilite\VoteBundle\Entity\IntersectionPath[] $intersections
    */
    public function testAverageAndCountOnIntersections($intersections)
    {
        $this->markTestSkipped('Must be revisited : to check the normal voting procedure!');

        $em = static::$container->get('doctrine')->getManager();

        foreach ($intersections as $intersection) {
            $infos = $em
                ->createQuery('SELECT count(v), avg(v.value) '
                    . 'FROM CLCyclabiliteVoteBundle:VoteIntersectionPath v '
                    . 'WHERE v.intersectionPath = :intersection '
                    . 'GROUP BY v.intersectionPath ')
                ->setParameter('intersection', $intersection)
                ->getSingleResult();

            $this->assertEquals($infos[1], $intersection->getVoteNumber());
            $this->assertEquals($infos[2], $intersection->getVoteAverage());
        }
    }
}
