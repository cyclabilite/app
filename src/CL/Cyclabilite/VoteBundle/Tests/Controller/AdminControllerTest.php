<?php

namespace CL\Cyclabilite\VoteBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AdminControllerTest extends AbstractControllerTest
{
    /**
     * Test if the different user (unlogged user, normal logged user and admin user)
     * can access / can not access to the admin part.
     */
    public function testAccess()
    {
        $this->adminLogIn();

        $r = $this->client->request('GET', '/admin/');

        $rep = $this->client->getResponse();
        $statusCode = $rep->getStatusCode();
        $this->assertEquals($statusCode, 200);

        $r = $this->client->request('GET', '/admin/recompute-average');
        $statusCode = $this->client->getResponse()->getStatusCode();
        $this->assertEquals($statusCode, 200);

        $this->adminLogOut();


        $this->userLogin();

        $this->client->request('GET', '/admin/');

        $statusCode = $this->client->getResponse()->getStatusCode();
        $this->assertEquals($statusCode, 401);

        $this->client->request('GET', '/admin/recompute-average');
        $statusCode = $this->client->getResponse()->getStatusCode();
        $this->assertEquals($statusCode, 401);

        $this->userLogOut();

        $this->client->request('GET', '/admin');
        $statusCode = $this->client->getResponse()->getStatusCode();
        $this->assertEquals($statusCode, 401);

        $this->client->request('GET', '/admin/recompute-average');
        $statusCode = $this->client->getResponse()->getStatusCode();
        $this->assertEquals($statusCode, 401);
    }
}
