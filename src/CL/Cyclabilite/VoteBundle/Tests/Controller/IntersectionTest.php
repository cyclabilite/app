<?php

namespace CL\Cyclabilite\VoteBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Test the segments are available
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class IntersectionTest extends WebTestCase
{

   public function testListAllIsSuccessfull()
   {

      $client = static::createClient();

      $client->request('GET', '/intersection/list/all.json');

      $response = $client->getResponse();

      $this->assertTrue($client->getResponse()->isSuccessful());

      return $response;
   }

   /**
    * @depends testListAllIsSuccessfull
    * @param \Symfony\Component\HttpFoundation\Response $response the response from testListAllIsSuccessfull
    */
   public function testListAllIsJsonType($response)
   {
      $this->assertTrue($response
            ->headers->contains('Content-Type', 'application/json'));
   }

   /**
    * @depends testListAllIsSuccessfull
    * @param \Symfony\Component\HttpFoundation\Response $response
    */
   public function testListAllIsNotEmpty($response)
   {
      $segmentList = json_decode($response->getContent(), true);

      $this->assertLessThanOrEqual(100, count($segmentList));

      return $segmentList;
   }

   /**
    * @depends testListAllIsNotEmpty
    * @param array $segmentList thearray of segment returned by testListAllIsNotEmpty
    */
   public function testListBBOXisSuccessfull($segmentList)
   {
      $bbox = $this->createBBOX($segmentList[0]);

      $client = static::createClient();

      $client->request('GET', '/intersection/list/bbox.json', array('bbox' => $bbox));

      $response = $client->getResponse();

      $this->assertTrue($response->isSuccessful());
   }

   private function createBBOX($node)
   {
      $east = (double) $node['geometry']['coordinates'][0] + 0.005; 
      $west = (double) $node['geometry']['coordinates'][0] - 0.005;
      $north = (double) $node['geometry']['coordinates'][1] + 0.005;
      $south = (double) $node['geometry']['coordinates'][1] - 0.005;

      return "$west,$south,$east,$north";
   }

}
