<?php

namespace CL\Cyclabilite\VoteBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Faker\Factory as FakerFactory;
use CL\Cyclabilite\VoteBundle\ComputeAverage\ComputeExpertAverage;

/**
 * Core for testing the ability to add a new vote on Intersection & IntersectionPath
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 * @author Marc Ducobu <marc.ducobu@champs-libres.coop>
 */
class AddVoteIntersectionPathExpertByProfileCore extends WebTestCase
{
    const MODE_USER_ALL_EXPERT = 1;
    const MODE_USER_ALL_NORMAL = 2;
    const MODE_U1_EXPERT_U2_NORMAL = 3;
    
    /**
     * Will choose an interesting entity : an entity on which more than two users
     * has voted.
     *
     * @see PhpUnitDoc ( method called before the first test of the test case class is run )
     */
    public static function setUpBeforeClass()
    {
        $kernel = static::createKernel(array('environment' => 'test_expert_average_one_profile_one_vote'));
        $kernel->boot();
        
        $container = $kernel->getContainer();
        $em = $container->get('doctrine')->getManager();
        
        $numberIntersectionWithTwoUserQ = $em
            ->getConnection()
            ->query('SELECT COUNT(*)
                FROM 
                (SELECT sq.node_id 
                    FROM 
                        (SELECT 
                        DISTINCT v.user_id, p.node_id
                        FROM cyclab_intersection_path_vote v 
                        INNER JOIN cyclab_intersection_paths p ON v.intersection_path_id = p.id) AS sq
                    GROUP BY sq.node_id
                    HAVING COUNT(*) > 1) AS ssq;')
            ->fetch();
            
        $numberIntersectionWithTwoUser = $numberIntersectionWithTwoUserQ['count'];
        
        $intersectionIdWithTwoUsers = $em
            ->getConnection()
            ->query('SELECT sq.node_id 
                FROM 
                (SELECT 
                    DISTINCT v.user_id, p.node_id
                    FROM cyclab_intersection_path_vote v 
                    INNER JOIN cyclab_intersection_paths p ON v.intersection_path_id = p.id) AS sq
                GROUP BY sq.node_id
                HAVING COUNT(*) > 1
                OFFSET '.rand(0, $numberIntersectionWithTwoUser - 1).'
                LIMIT 1')
            ->fetch();
            
        static::$intersectionId = $intersectionIdWithTwoUsers['node_id'];
        print "\n	Selected Intersection : ".static::$intersectionId."\n";
    }
    
    /**
     * Initialization to do before the tests (boot a kernel, ...).
     * As we compare object with the results of sql requests it is important
     * that the locals objects (and their changes) match with the DB, for that
     * we close the connection and re-connect between each test (we boot a kernel)
     */
    protected function initConnexion() //more initDBConnexion
    {
        $kernel = static::createKernel(array('environment' => 'test_expert_average_one_profile_one_vote'));
        $kernel->boot();
        
        $this->container = $kernel->getContainer();
        $this->em = $this->container->get('doctrine')->getManager();
        $this->faker = FakerFactory::create();
        
        $this->intersection = $this->em
            ->getRepository('CLCyclabiliteVoteBundle:Node')
            ->find(static::$intersectionId);
    }
    
    /**
     * Initialize the users that vote for the selected connection.
     * @param Int $mode if equals to
     *  - self::MODE_USER_ALL_EXPERT > all the users became experts
     *  - self::MODE_USER_ALL_NORMAL > all the users became normal-users
     *  - self::MODE_U1_EXPERT_U2_NORMAL > user 1 became expert and user 2 a normal user
     */
    protected function initIntersectionUsers($mode)
    {
        $votes = $this->intersection->getVotes();
        
        $this->user1 = $votes->first()->getUser();
        
        while ($votes->next()) {
            $this->user2 = $votes->current()->getUser();
            if ($this->user1 != $this->user2) {
                break;
            }
        }
        
        if ($mode == self::MODE_U1_EXPERT_U2_NORMAL) {
            $this->user1->setExpert(true);
            $this->user2->setExpert(false);
        } else {
            $expert = ($mode == self::MODE_USER_ALL_EXPERT);
            $votes = $this->intersection->getVotes();
            
            foreach ($votes as $vote) {
                $vote->getUser()->setExpert($expert);
            }
        }
        
        $this->container->get('cl_cyclabilite_vote.compute_average')->recomputeAverage($this->intersection);
        
        foreach ($this->intersection->getIntersectionPaths() as $intersectionPath) {
            $this->container->get('cl_cyclabilite_vote.compute_average')->recomputeAverage($intersectionPath);
        }
        
        $this->em->flush();
    }
    
    /**
     * Compute the average and the number of vote from votes emitted by the experts &
     * votes emitted by the normal users.
     * @param Array $expertVoteData Data about the votes emitted by the experts (count -> number of votes, avg, )
     * @param Array $normalVoteData Data about the votes emitted by the normal users (count -> number of votes, avg, )
     * @return an Array with two key : avg (for the average) and count (for the number of votes)
     */
    protected function computeAvgAndCount($expertVoteData, $normalVoteData)
    {
        $avg = 0;
        $count = 0;
        if ($expertVoteData['count'] > 0) {
            if ($normalVoteData['count'] > 0) {
                $avg = ($expertVoteData['avg'] + $normalVoteData['avg']) / 2;
                $count =  $expertVoteData['count'] + $normalVoteData['count'];
            } else {
                $avg = $expertVoteData['avg'];
                $count = $expertVoteData['count'];
            }
        } else {
            if ($normalVoteData['count'] > 0) {
                $avg =  $normalVoteData['avg'];
                $count = $normalVoteData['count'];
            }
        }
        return array('avg' => $avg, 'count' => $count);
    }
    
    /**
     * Compute via a sql request the average of the vote and count the number of the votes
     * @return an associative array with two keys : avg for the average and count for the
     * number of votes
     */
    public function sqlComputeAvgVoteAndCountVoteForIntersection()
    {
        $normalVoteData = $this->em
            ->getConnection()
            ->query('SELECT avg(v.vote_value), count(v.id)
                        FROM cyclab_intersection_path_vote v
                        WHERE (v.user_id, v.profile_id, v.intersection_path_id, v.datetime_vote, v.datetime_creation) IN
                        (
                            SELECT v2.user_id, v2.profile_id, v2.intersection_path_id, v2.datetime_vote, max(v2.datetime_creation)
                            FROM cyclab_intersection_path_vote v2
                            WHERE (v2.user_id, v2.profile_id, v2.intersection_path_id, v2.datetime_vote) IN
                            (
                                SELECT v1.user_id, v1.profile_id, v1.intersection_path_id, max(v1.datetime_vote)
                                FROM cyclab_intersection_path_vote v1
                                INNER JOIN cyclab_intersection_paths p1 ON v1.intersection_path_id = p1.id
                                INNER JOIN cyclab_users u1 ON v1.user_id = u1.id
                                WHERE u1.isexpert = FALSE
                                AND p1.node_id = '.$this->intersection->getId().'	
                                GROUP BY v1.user_id, v1.profile_id, v1.intersection_path_id
                            )
                            GROUP BY v2.user_id, v2.profile_id, v2.intersection_path_id, v2.datetime_vote
                        )')
            ->fetch();
        
        $expertVoteData = $this->em
            ->getConnection()
            ->query('SELECT avg(v.vote_value), count(v.id)
                        FROM cyclab_intersection_path_vote v
                        WHERE (v.user_id, v.profile_id, v.intersection_path_id, v.datetime_vote, v.datetime_creation) IN
                        (
                            SELECT v2.user_id, v2.profile_id, v2.intersection_path_id, v2.datetime_vote, max(v2.datetime_creation)
                            FROM cyclab_intersection_path_vote v2
                            WHERE (v2.user_id, v2.profile_id, v2.intersection_path_id, v2.datetime_vote) IN
                            (
                                SELECT v1.user_id, v1.profile_id, v1.intersection_path_id, max(v1.datetime_vote)
                                FROM cyclab_intersection_path_vote v1
                                INNER JOIN cyclab_intersection_paths p1 ON v1.intersection_path_id = p1.id
                                INNER JOIN cyclab_users u1 ON v1.user_id = u1.id
                                WHERE u1.isexpert = TRUE
                                AND p1.node_id = '.$this->intersection->getId().'	
                                GROUP BY v1.user_id, v1.profile_id, v1.intersection_path_id
                            )
                            GROUP BY v2.user_id, v2.profile_id, v2.intersection_path_id, v2.datetime_vote
                        )')
            ->fetch();
        
        $avgAndCount = $this->computeAvgAndCount($expertVoteData, $normalVoteData);
        
        return $avgAndCount;
    }
    
    /**
     * Check for the current Intersection (and its IntersectionPaths) if it computation of average corresponds to
     * with the DB
     */
    public function checkIntersectionHasAGoodAverage()
    {
            $avgAndCount = $this->sqlComputeAvgVoteAndCountVoteForIntersection();
        
        if ($avgAndCount['count'] == 0) {
            $this->assertTrue(false);
        }
        
        $avgAndCount = $this->sqlComputeAvgVoteAndCountVoteForIntersection();
        
        //* debug code
        print "(IN:".$this->intersection->getId().")IgetVA: ".
            $this->intersection->getVoteAverage()." -   query_avg: ".$avgAndCount['avg']." \n";
        print "(IN:".$this->intersection->getId().")IgetVN: ".
            $this->intersection->getVoteNumber()." - query_count: ".$avgAndCount['count']." \n";
        //*/
        
        $this->assertEquals($this->intersection->getVoteNumber(), $avgAndCount['count']);
        $this->assertEquals($this->intersection->getVoteAverage(), $avgAndCount['avg']);
      
        foreach ($this->intersection->getIntersectionPaths() as $intersectionPath) {
            $expertVoteData = $this->em
                ->getConnection()
                ->query('SELECT avg(v.vote_value), count(v.id)
                        FROM cyclab_intersection_path_vote v
                        WHERE (v.user_id, v.profile_id, v.intersection_path_id, v.datetime_vote, v.datetime_creation) IN
                        (
                            SELECT v2.user_id, v2.profile_id, v2.intersection_path_id, v2.datetime_vote, max(v2.datetime_creation)
                            FROM cyclab_intersection_path_vote v2
                            WHERE (v2.user_id, v2.profile_id, v2.intersection_path_id, v2.datetime_vote) IN
                            (
                                SELECT v1.user_id, v1.profile_id, v1.intersection_path_id, max(v1.datetime_vote)
                                FROM cyclab_intersection_path_vote v1
                                INNER JOIN cyclab_users u1 ON v1.user_id = u1.id
                                WHERE u1.isexpert = TRUE
                                AND v1.intersection_path_id = '.$intersectionPath->getId().'	
                                GROUP BY v1.user_id, v1.profile_id, v1.intersection_path_id
                            )
                            GROUP BY v2.user_id, v2.profile_id, v2.intersection_path_id, v2.datetime_vote
                        )')
                ->fetch();
                
            $normalVoteData = $this->em
                ->getConnection()
                ->query('SELECT avg(v.vote_value), count(v.id)
                        FROM cyclab_intersection_path_vote v
                        WHERE (v.user_id, v.profile_id, v.intersection_path_id, v.datetime_vote, v.datetime_creation) IN
                        (
                            SELECT v2.user_id, v2.profile_id, v2.intersection_path_id, v2.datetime_vote, max(v2.datetime_creation)
                            FROM cyclab_intersection_path_vote v2
                            WHERE (v2.user_id, v2.profile_id, v2.intersection_path_id, v2.datetime_vote) IN
                            (
                                SELECT v1.user_id, v1.profile_id, v1.intersection_path_id, max(v1.datetime_vote)
                                FROM cyclab_intersection_path_vote v1
                                INNER JOIN cyclab_users u1 ON v1.user_id = u1.id
                                WHERE u1.isexpert = FALSE
                                AND v1.intersection_path_id = '.$intersectionPath->getId().'	
                                GROUP BY v1.user_id, v1.profile_id, v1.intersection_path_id
                            )
                            GROUP BY v2.user_id, v2.profile_id, v2.intersection_path_id, v2.datetime_vote
                        )')
                ->fetch();
            
            /* debug code
            print "(IP:".$intersectionPath->getId().") - IgetVA: ".
                $intersectionPath->getVoteAverage()." -   query_avg: ".$avgAndCount['avg']." \n";
            print "(IP:".$intersectionPath->getId().") - IgetVN: ".
                $intersectionPath->getVoteNumber()." - query_count: ".$avgAndCount['count']." \n";
            //*/
                
            $avgAndCount = $this->computeAvgAndCount($expertVoteData, $normalVoteData);
            $this->assertEquals($intersectionPath->getVoteNumber(), $avgAndCount['count']);
            $this->assertEquals($intersectionPath->getVoteAverage(), $avgAndCount['avg']);
        }
    }
}
