<?php

namespace CL\Cyclabilite\VoteBundle\Tests\Controller;

use CL\Cyclabilite\VoteBundle\Entity\IntersectionPath;
use CL\Cyclabilite\VoteBundle\Entity\VoteIntersectionPath;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\Common\Collections\Criteria;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Test the validation of intersection path
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class IntersectionPathValidationTest extends WebTestCase
{
   private static $container;

   public static function setUpBeforeClass()
   {
      $kernel = static::createKernel();
      $kernel->boot();

      static::$container = $kernel->getContainer();
   }

   /**
    * Test that an intersection path composed of non-contiguous
    * members is invalid.
    *
    * Members are contiguous if a nodestart|nodeEnd of an intersectionPath's
    * segment start is equal to the node, which is egal to the nodeStart|nodeEnd
    * of the segment_end.
    *
    * The validation here must have 2 errors: one because segment are not
    * contiguous, one because the node does not belong to both segments.
    */
   public function testMembersAreContiguous()
   {
      //create a not contiguous intersection path
      $segmentA = static::$container->get('doctrine')->getManager()
            ->createQuery('SELECT s FROM CLCyclabiliteVoteBundle:Segment s')
            ->setMaxResults(1)
            ->getSingleResult();

      $segmentB = static::$container->get('doctrine')->getManager()
            ->createQuery('SELECT s FROM CLCyclabiliteVoteBundle:Segment s'
                  . ' WHERE s.nodeStart NOT IN (:nodes) '
                  . ' AND s.nodeEnd NOT IN (:nodes)')
            ->setParameter('nodes', array($segmentA->getNodeStart(),
               $segmentA->getNodeEnd()))
            ->setMaxResults(1)
            ->getSingleResult();

      $node = $segmentA->getNodeStart();

      $intersectionPath = new IntersectionPath();
      $intersectionPath->setNode($node)
            ->setSegmentEnd($segmentA)
            ->setSegmentStart($segmentB);

      $errors = static::$container->get('validator')
            ->validate($intersectionPath);

      $this->assertEquals(1, count($errors));

      return $intersectionPath;
   }

   /**
    * test if the validation of a vote with an incorrect intersection path
    * return errors.
    *
    * Rationale:  the intersection path are created "on the fly" if they
    * do not exists in database. We must, then, check the validation on the vote
    * will perform a validation the the IntersectionPath.
    *
    * @depends testMembersAreContiguous
    */
   public function testValidationFromVote(IntersectionPath $intersectionPath)
   {
      $vote = new VoteIntersectionPath();

      $vote->setIntersectionPath($intersectionPath);

      $errors = static::$container->get('validator')
           ->validate($vote);

      $this->assertGreaterThan(2, count($errors));

      foreach ($errors as $error) {
         $propertyPaths[] = $error->getPropertyPath();
      }

      $this->assertTrue(in_array('intersectionPath.node', $propertyPaths));
   }

   public function testValidVote()
   {
      //create a valid intersectionPath

      //get a node which is connected to segments
      $rsm = new ResultSetMapping();
      $rsm->addScalarResult('nid', 'nid');

      $nodeId = static::$container->get('doctrine')->getManager()
            ->createNativeQuery('SELECT nid, total FROM (SELECT count(cyclab_segments.nodestart_id)
+ COUNT(cyclab_segments.nodeend_id) AS total, nodestart_id AS nid
FROM cyclab_segments GROUP BY nodestart_id) as sb1 WHERE sb1.total > 2 LIMIT 1', $rsm)
            ->getSingleResult();

      $node = static::$container->get('doctrine')->getManager()
            ->getRepository('CLCyclabiliteVoteBundle:Node')
            ->find($nodeId['nid']);

      $segments = static::$container->get('doctrine')->getManager()
            ->getRepository('CLCyclabiliteVoteBundle:Segment')
            ->matching(Criteria::create()
                  ->where(
                        Criteria::expr()
                        ->eq('nodeStart', $node)
                        )
                  ->orWhere(
                        Criteria::expr()
                        ->eq('nodeEnd', $node)
                        )
                  );

      if (count($segments) < 2) {
         throw new \Exception('we could not retrieve segments connected to the node'
               . ' with id '.$node->getId().'. Impossible to build an IntersectionPath');
      }

      $intersectionPath = new IntersectionPath();
      $intersectionPath->setNode($node)
            ->setSegmentEnd($segments[0])
            ->setSegmentStart($segments[1]);

      $errors = static::$container->get('validator')
            ->validate($intersectionPath);

      $this->assertEquals(0, count($errors));

   }
}
