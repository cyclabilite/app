<?php

namespace CL\Cyclabilite\VoteBundle\Tests\Controller;

use CL\Cyclabilite\UserBundle\Tests\Helper\LoginHelper;

/**
 * Test the ability to add a new vote for an intersection & intersectionpath 
 * with the environment 'test_expert_average_one_profile_one_vote'
 *
 * @todo choose an entity with no vote then test it (+ add vote)
 * @author Champs-Libres <marc.ducobu@champs-libres.coop>
 */
class AddVoteIntersectionPathExpertByProfileUI extends AddVoteIntersectionPathExpertByProfileCore
{
    protected $container;
    protected $em;
    protected $faker;
    protected $intersection;
    protected $client;
    protected $user;
    protected $user1;
    protected $user2;
    protected $form;
    
    protected static $intersectionId;
    
    public function testSend()
    {
        $this->initConnexion();
        $this->client = static::createClient(array('environment' => 'test_expert_average_one_profile_one_vote'));
        $this->initIntersectionUsers(self::MODE_U1_EXPERT_U2_NORMAL);

        $this->user = LoginHelper::authenticateClient($this->client, $this->container);

        $now = new \DateTime('now');
        $profiles = $this->user->getProfiles();
        $profileSelected = $profiles->get(array_rand($profiles->getKeys()));
            
        $crawler = $this->client->request(
            'GET',
            '/votes/form/intersection/'.static::$intersectionId
        );
              
        $form = $crawler->filter('form')->form();
        $form['vote_intersection[value]'] = rand(1, 5);
        $form['vote_intersection[profile]'] = $profileSelected->getId();
      
        //pick two different segments for start and end
        $segmentsStartValues = $form->get('vote_intersection[segmentStart]')
            ->availableOptionValues();
        $valueStart = $segmentsStartValues[array_rand($segmentsStartValues)];
      
        $segmentEndValues = $form->get('vote_intersection[segmentEnd]')
            ->availableOptionValues();
        $segmentEndValuesFiltered =
            array_filter(
                $segmentEndValues,
                function ($var) use ($valueStart) {
                    return !($var === $valueStart);
                }
            );
        $valueEnd = $segmentEndValuesFiltered[array_rand($segmentEndValuesFiltered)];
      
        $form['vote_intersection[segmentStart]'] = $valueStart;
        $form['vote_intersection[segmentEnd]'] = $valueEnd;
        
        $this->client->submit($form);
        $response = $this->client->getResponse();
        
        $this->assertTrue($response->isSuccessful());
        
        $responseData = json_decode($response->getcontent(), true);
        $avgAndCount = $this->sqlComputeAvgVoteAndCountVoteForIntersection();
        
        $this->assertEquals($responseData['feature']['properties']['vote_number'], $avgAndCount['count']);
        $this->assertEquals($responseData['feature']['properties']['vote_average'], $avgAndCount['avg']);
    }
}
