<?php

namespace CL\Cyclabilite\VoteBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Faker\Factory as FakerFactory;

/**
 * Core for testing the ability to add a new vote on a segment
 *
 * @author Champs-Libres <info@champs-libres.coop>
 */
class AddVoteSegmentExpertByProfileCore extends WebTestCase
{
    const MODE_USER_ALL_EXPERT = 1;
    const MODE_USER_ALL_NORMAL = 2;
    const MODE_U1_EXPERT_U2_NORMAL = 3;
    
    /**
     * Will choose an interesting segment : a segment on which more than two users
     * has voted.
     *
     * @see PhpUnitDoc ( method called before the first test of the test case class is run )
     */
    public static function setUpBeforeClass()
    {
        $kernel = static::createKernel(array('environment' => 'test_expert_average_one_profile_one_vote'));
        $kernel->boot();
        
        $container = $kernel->getContainer();
        $em = $container->get('doctrine')->getManager();
        
        $numberSegmentWithTwoUserQ = $em
            ->getConnection()
            ->query('SELECT COUNT(*)
                FROM 
                (SELECT sq.segment_id 
                    FROM 
                        (SELECT 
                        DISTINCT v.user_id, v.segment_id
                        FROM cyclab_segment_vote v) AS sq
                    GROUP BY sq.segment_id
                    HAVING COUNT(*) > 1) AS ssq;')
            ->fetch();
            
        $numberSegmentWithTwoUser = $numberSegmentWithTwoUserQ['count'];
        
        $segementIdWithTwoUsers = $em
            ->getConnection()
            ->query('SELECT sq.segment_id 
                FROM 
                (SELECT 
                    DISTINCT v.user_id, v.segment_id
                    FROM cyclab_segment_vote v) as sq
                GROUP BY sq.segment_id
                HAVING COUNT(*) > 1
                OFFSET '.rand(0, $numberSegmentWithTwoUser - 1).'
                LIMIT 1')
            ->fetch();
            
        static::$segmentId = $segementIdWithTwoUsers['segment_id'];
        print "\n	Selected Segment : ".static::$segmentId."\n";
    }
    
    /**
     * Initialization to do before the tests (boot a kernel, ...).
     * As we compare object with the results of sql requests it is important
     * that the locals objects (and their changes) match with the DB, for that
     * we close the connection and re-connect between each test (we boot a kernel)
     */
    protected function initConnexion() //more initDBConnexion
    {
        $kernel = static::createKernel(array('environment' => 'test_expert_average_one_profile_one_vote'));
        $kernel->boot();
        
        $this->container = $kernel->getContainer();
        $this->em = $this->container->get('doctrine')->getManager();
        $this->faker = FakerFactory::create();
        
        $this->segment = $this->em
            ->getRepository('CLCyclabiliteVoteBundle:Segment')
            ->find(static::$segmentId);
    }
    
    /**
     * Initialize the users that vote for the selected connection.
     * @param Int $mode if equals to
     *  - self::MODE_USER_ALL_EXPERT > all the users became experts
     *  - self::MODE_USER_ALL_NORMAL > all the users became normal-users
     *  - self::MODE_U1_EXPERT_U2_NORMAL > user 1 became expert and user 2 a normal user
     */
    protected function initSegmentUsers($mode)
    {
        $votes = $this->segment->getVotes();
        $this->user1 = $votes->first()->getUser();
        
        while ($votes->next()) {
            $this->user2 = $votes->current()->getUser();
            if ($this->user1 != $this->user2) {
                break;
            }
        }
        
        if ($mode == self::MODE_U1_EXPERT_U2_NORMAL) {
            $this->user1->setExpert(true);
            $this->user2->setExpert(false);
        } else {
            $expert = ($mode == self::MODE_USER_ALL_EXPERT);
            $votes = $this->segment->getVotes();
            
            foreach ($votes as $vote) {
                $vote->getUser()->setExpert($expert);
            }
        }
        
        $this->container->get('cl_cyclabilite_vote.compute_average')->recomputeAverage($this->segment);
        $this->em->flush();
    }
    
    /**
     * Compute the average and the number of vote from votes emitted by the experts &
     * votes emitted by the normal users.
     * @param Array $expertVoteData Data about the votes emitted by the experts (count -> number of votes, avg, )
     * @param Array $normalVoteData Data about the votes emitted by the normal users (count -> number of votes, avg, )
     * @return an Array with two key : avg (for the average) and count (for the number of votes)
     * @todo Partager entre Intersection - Segment
     */
    protected function computeAvgAndCount($expertVoteData, $normalVoteData)
    {
        $avg = 0;
        $count = 0;
        if ($expertVoteData['count'] > 0) {
            if ($normalVoteData['count'] > 0) {
                $avg = ($expertVoteData['avg'] + $normalVoteData['avg']) / 2;
                $count =  $expertVoteData['count'] + $normalVoteData['count'];
            } else {
                $avg = $expertVoteData['avg'];
                $count = $expertVoteData['count'];
            }
        } else {
            if ($normalVoteData['count'] > 0) {
                $avg =  $normalVoteData['avg'];
                $count = $normalVoteData['count'];
            }
        }
        return array('avg' => $avg, 'count' => $count);
    }
    
    /**
     * Compute via a sql request the average of the vote and count the number of the votes
     * @return an associative array with two keys : avg for the average and count for the
     * number of votes
     */
    public function sqlComputeAvgVoteAndCountVoteForSegment()
    {
        $normalVoteData = $this->em
            ->getConnection()
            ->query('SELECT avg(v.vote_value), count(v.id)
                        FROM cyclab_segment_vote v
                        WHERE (v.user_id, v.profile_id, v.direction, v.datetime_vote, v.datetime_creation) IN
                        (
                            SELECT v2.user_id, v2.profile_id, v2.direction, v2.datetime_vote, max(v2.datetime_creation)
                            FROM cyclab_segment_vote v2
                            WHERE (v2.user_id, v2.profile_id, v2.direction, v2.datetime_vote) IN
                            (
                                SELECT v1.user_id, v1.profile_id, v1.direction, max(v1.datetime_vote)
                                FROM cyclab_segment_vote v1
                                INNER JOIN cyclab_users u1 ON v1.user_id = u1.id
                                WHERE u1.isexpert = FALSE
                                AND v1.segment_id = '.$this->segment->getId().'	
                                GROUP BY v1.user_id, v1.profile_id, v1.direction
                            )
                            AND v2.segment_id = '.$this->segment->getId().'	
                            GROUP BY v2.user_id, v2.profile_id, v2.direction, v2.datetime_vote
                        )
                        AND v.segment_id = '.$this->segment->getId())
            ->fetch();
        
        $expertVoteData = $this->em
            ->getConnection()
            ->query('SELECT avg(v.vote_value), count(v.id)
                        FROM cyclab_segment_vote v
                        WHERE (v.user_id, v.profile_id, v.direction, v.datetime_vote, v.datetime_creation) IN
                        (
                            SELECT v2.user_id, v2.profile_id, v2.direction, v2.datetime_vote, max(v2.datetime_creation)
                            FROM cyclab_segment_vote v2
                            WHERE (v2.user_id, v2.profile_id, v2.direction, v2.datetime_vote) IN
                            (
                                SELECT v1.user_id, v1.profile_id, v1.direction, max(v1.datetime_vote)
                                FROM cyclab_segment_vote v1
                                INNER JOIN cyclab_users u1 ON v1.user_id = u1.id
                                WHERE u1.isexpert = TRUE
                                AND v1.segment_id = '.$this->segment->getId().'	
                                GROUP BY v1.user_id, v1.profile_id, v1.direction
                            )
                            AND v2.segment_id = '.$this->segment->getId().'	
                            GROUP BY v2.user_id, v2.profile_id, v2.direction, v2.datetime_vote
                        )
                        AND v.segment_id = '.$this->segment->getId())
            ->fetch();
        
        $avgAndCount = $this->computeAvgAndCount($expertVoteData, $normalVoteData);
        
        return $avgAndCount;
    }
    
    /**
     * Check for the current Segment,  if its computation of average corresponds to
     * with the DB
     */
    public function checkSegmentHasAGoodAverage()
    {
        $avgAndCount = $this->sqlComputeAvgVoteAndCountVoteForSegment();
        
        if ($avgAndCount['count'] == 0) {
            $this->assertTrue(false);
        }
        
        $avgAndCount = $this->sqlComputeAvgVoteAndCountVoteForSegment();
        
        //* debug code
        print "(SG:".$this->segment->getId().")IgetVA: ".
            $this->segment->getVoteAverage()." -   query_avg: ".$avgAndCount['avg']." \n";
        print "(SG:".$this->segment->getId().")IgetVN: ".
            $this->segment->getVoteNumber()." - query_count: ".$avgAndCount['count']." \n";
        //*/
        
        $this->assertEquals($this->segment->getVoteNumber(), $avgAndCount['count']);
        $this->assertEquals($this->segment->getVoteAverage(), $avgAndCount['avg']);
    }
}
