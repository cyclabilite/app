<?php

namespace CL\Cyclabilite\VoteBundle\Tests\Controller;

use CL\Cyclabilite\VoteBundle\Entity\VoteSegment;
use CL\Cyclabilite\VoteBundle\ComputeAverage\ComputeExpertAverage;

/**
 * Test the ability to add a new vote with the environment
 * test_expert_average_one_profile_one_vote on an intersection & intersection paths
 *
 * @todo choose an entity with no vote then test it (+ add vote)
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 * @author Marc Ducobu <marc.ducobu@champs-libres.coop>
 */
class AddVoteSegmentExpertByProfile extends AddVoteSegmentExpertByProfileCore
{
    protected $container;
    protected $em;
    protected $faker;
    protected $user1;
    protected $user2;
    protected $segment;
    
    protected static $segmentId;
    
    /**
     * Add a random new vote on the entity
     *
     * @param Segment $segment the segment which will be evaluated
     * @param User $user The user evaluating the entity
     */
    protected function addNewVote($segment, $user)
    {
        $newVote = new VoteSegment();
        $newVote->setSegment($segment);
        $newVote->setUser($user);
        $newVote->setValue(rand(1, 5));
        $newVote->setDatetimeVote($this->faker->dateTimeThisDecade());
        $profiles = $user->getProfiles();
        $profileSelected = $profiles->get(array_rand($profiles->getKeys()));
        $newVote->setProfile($profileSelected);        
        $newVote->setDirection(
            [VoteSegment::DIRECTION_BOTH, VoteSegment::DIRECTION_FORWARD, VoteSegment::DIRECTION_BACKWARD][rand(0,2)]
        );
        $this->em->persist($newVote);
        $this->em->flush();
    }
    
    /**
     * Test if the class used for the average computation is ComputeExpertAverage
     * (and not ComputeNormalAverage, ...)
     */
    public function testGoodServiceLoaded()
    {
        $this->initConnexion();
        $computerClass = $this->container->get('cl_cyclabilite_vote.compute_average');
        $this->assertTrue($computerClass instanceof ComputeExpertAverage);
    }
    
    /**
     * Test the recompute average for a segment
     * considering all users (that emit a vote on it) as experts.
     */
    public function testRecomputeAvgUserAllExperts()
    {
        $this->initConnexion();
        $this->initSegmentUsers(self::MODE_USER_ALL_EXPERT);
        $this->checkSegmentHasAGoodAverage();
    }
    
    /**
     * Test the recompute average for a segment
     * considering all users (that emit a vote on it) as normal users.
     */
    public function testRecomputeAvgUserAllNormal()
    {
        $this->initConnexion();
        $this->initSegmentUsers(self::MODE_USER_ALL_NORMAL);
        $this->checkSegmentHasAGoodAverage();
    }
    
    
    /**
     * Test the recomputation of the  average for a segment
     * considering that user1 is expert and user2 is a normal user
     */
    public function testRecomputeAvgUserUser1ExpertUser2Normal()
    {
        $this->initConnexion();
        $this->initSegmentUsers(self::MODE_U1_EXPERT_U2_NORMAL);
        $this->checkSegmentHasAGoodAverage();
    }
    
    /*
     * Test the creation of a new vote, considering that user1 is an expert and
     * user2 a normal user.
     *
     *  @depends testRecomputeAvgUserUser1ExpertUser2Normal
     */
    public function testAddNewVoteUser1ExpertUser2Normal()
    {
        $this->initConnexion();
        $this->initSegmentUsers(self::MODE_U1_EXPERT_U2_NORMAL);
        $this->addNewVote($this->segment, $this->user1);
        $this->checkSegmentHasAGoodAverage();
    }
    
    /*
     * Test the creation of a new vote, considering that all the users (
     * having emitting a vote on this entity) are normal users
     *
     *  @depends testRecomputeAvgUserAllNormal
     */
    public function testAddNewVoteAllNormal()
    {
        $this->initConnexion();
        $this->initSegmentUsers(self::MODE_USER_ALL_NORMAL);
        $this->addNewVote($this->segment, $this->user1);
        $this->checkSegmentHasAGoodAverage();
    }
    
    
    /*
    * Test the creation of a new vote, considering that all the users (
    * having emitting a vote on this entity) as experts
    *
    * @depends testRecomputeAvgUserAllExperts
    */
    public function testAddNewVoteAllExperts()
    {
        $this->initConnexion();
        $this->initSegmentUsers(self::MODE_USER_ALL_EXPERT);
        $this->addNewVote($this->segment, $this->user1);
        $this->checkSegmentHasAGoodAverage();
    }
}
