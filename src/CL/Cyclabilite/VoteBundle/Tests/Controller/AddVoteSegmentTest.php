<?php

namespace CL\Cyclabilite\VoteBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * Test the ability to send a post request to
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class AddVoteSegmentTest2 extends AbstractControllerTest
{
    private static $container;
    private static $segment;
    private static $voteNumberBeforeTest;

    private $form;

    public static function setUpBeforeClass()
    {
        $kernel = static::createKernel();
        $kernel->boot();

        static::$container = $kernel->getContainer();
        static::$segment = static::$container->get('doctrine')->getManager()
            ->createQuery('SELECT s FROM CLCyclabiliteVoteBundle:Segment s ')
            ->setFirstResult(rand(1, 2))
            ->setMaxResults(1)
            ->getSingleResult();

        static::$voteNumberBeforeTest = static::$segment->getVoteNumber();
    }

    public function setUp()
    {
        $now = new \DateTime('now');
        $user = $this->userLogin();

        $profiles = $user->getProfiles();
        $profileSelected = $profiles->get(array_rand($profiles->getKeys()));

        $crawler = $this->client->request('GET', '/votes/form/segment/'.static::$segment->getId());

        $form = $crawler->filter('form')->form();
        $form['vote_segment[value]'] = rand(1, 5);
        $form['vote_segment[profile]'] = $profileSelected->getId();

        $valuesDirection = array('backward', 'forward', 'both');
        $form['vote_segment[direction]'] = $valuesDirection[array_rand($valuesDirection)];

        $this->form = $form;
    }

    public function testValidVoteSegment()
    {
        $this->markTestSkipped('Must be revisited : to check the normal voting procedure!');

        $this->client->submit($this->form);

        $this->assertTrue($this->client->getResponse()->isSuccessful());
    }

    /**
     * @depends testValidVoteSegment
     */
    public function testAverageAndCount()
    {
        $this->markTestSkipped('Must be revisited : to check the normal voting procedure!');

        $em = static::$container->get('doctrine')->getManager();

        $segment = static::$segment;
        $em->refresh($segment);

        if ($segment === null) {
            throw new Exception("way should not be null");
        }

        $infos = $em
            ->createQuery('SELECT count(v), avg(v.value) '
                . 'FROM CLCyclabiliteVoteBundle:VoteSegment v '
                . 'WHERE v.segment = :entityId '
                . 'GROUP BY v.segment')
            ->setParameter('entityId', $segment)
            ->getSingleResult();

        $this->assertEquals($infos[1], $segment->getVoteNumber());
        $this->assertEquals($infos[2], $segment->getVoteAverage());
        $this->assertEquals(static::$voteNumberBeforeTest + 1, $segment->getVoteNumber());
        $this->assertEquals(static::$voteNumberBeforeTest + 1, $infos[1]);
    }
}
