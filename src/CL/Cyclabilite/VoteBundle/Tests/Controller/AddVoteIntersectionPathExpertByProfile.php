<?php

namespace CL\Cyclabilite\VoteBundle\Tests\Controller;

use CL\Cyclabilite\VoteBundle\Entity\VoteIntersectionPath;
use CL\Cyclabilite\VoteBundle\ComputeAverage\ComputeExpertAverage;

/**
 * Test the ability to add a new vote with the environment
 * test_expert_average_one_profile_one_vote on an intersection & intersection paths
 *
 * @todo choose an entity with no vote then test it (+ add vote)
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 * @author Marc Ducobu <marc.ducobu@champs-libres.coop>
 */
class AddVoteIntersectionPathExpertByProfile extends AddVoteIntersectionPathExpertByProfileCore
{
    protected $container;
    protected $em;
    protected $faker;
    protected $user1;
    protected $user2;
    protected $intersection;
    
    protected static $intersectionId;
    
    /**
     * Add a random new vote on the entity
     *
     * @param Intersection $entity the Intersection which will be evaluated
     * @param User $user The user evaluating the entity
     */
    protected function addNewVote($entity, $user)
    {
        $segments = $entity->getSegments();
        
        $segmentStart = null;
        if (rand(0, 1)) {
            $segmentStartPosition = rand(0, sizeof($segments));
            $segmentStart = $segments->get($segmentStartPosition);
        }
        
        $segmentEnd = null;
        if (rand(0, 1)) {
            $segmentEndPosition = rand(0, sizeof($segments) - 1);
            if ($segmentStart != null && $segmentEndPosition >= $segmentStartPosition) {
                $segmentEndPosition++;
            }
            $segmentEnd = $segments->get($segmentEndPosition);
        }
                
        $intersectionPaths = $this->em
            ->getRepository('CLCyclabiliteVoteBundle:IntersectionPath')
            ->findByOrCreate(array(
                'node' => $entity,
                'segmentStart' => $segmentStart,
                'segmentEnd' => $segmentEnd
            ));
        
        if (count($intersectionPaths) !== 1) {
            throw new \Exception("We could not create / retrieve intersection paths");
        }
        $intersectionPath = $intersectionPaths[0];

        $newVote = new VoteIntersectionPath();
        $newVote->setIntersectionPath($intersectionPath);
        $newVote->setUser($user);
        $newVoteValue = rand(1, 5);
        $newVote->setValue($newVoteValue);
        $newVote->setDatetimeVote($this->faker->dateTimeThisDecade());
        $profiles = $user->getProfiles();
        $profileSelected = $profiles->get(array_rand($profiles->getKeys()));
        $newVote->setProfile($profileSelected);
        
        $this->em->persist($newVote);
        $this->em->flush();
    }
    
    /**
     * Test if the class used for the average computation is ComputeExpertAverage
     * (and not ComputeNormalAverage, ...)
     */
    public function testGoodServiceLoaded()
    {
        $this->initConnexion();
        $computerClass = $this->container->get('cl_cyclabilite_vote.compute_average');
        $this->assertTrue($computerClass instanceof ComputeExpertAverage);
    }
    
    /**
     * Test the recompute average for an intersection & intersectionPath
     * considering all users (that emit a vote on it) as experts.
     */
    public function testRecomputeAvgUserAllExperts()
    {
        $this->initConnexion();
        $this->initIntersectionUsers(self::MODE_USER_ALL_EXPERT);
        $this->checkIntersectionHasAGoodAverage();
    }
    
    /**
     * Test the recompute average for an intersection & intersectionPath
     * considering all users (that emit a vote on it) as normal users.
     */
    public function testRecomputeAvgUserAllNormal()
    {
        $this->initConnexion();
        $this->initIntersectionUsers(self::MODE_USER_ALL_NORMAL);
        $this->checkIntersectionHasAGoodAverage();
    }
    
    
    /**
     * Test the recomputation of the  average for an intersection & intersectionPath
     * considering that user1 is expert and user2 is a normal user
     */
    public function testRecomputeAvgUserUser1ExpertUser2Normal()
    {
        $this->initConnexion();
        $this->initIntersectionUsers(self::MODE_U1_EXPERT_U2_NORMAL);
        $this->checkIntersectionHasAGoodAverage();
    }
    
    /*
     * est the creation of a new vote, considering that user1 is an expert and
     * user2 a normal user.
     *
     *  @depends testRecomputeAvgUserUser1ExpertUser2Normal
     */
    public function testAddNewVoteUser1ExpertUser2Normal()
    {
        $this->initConnexion();
        $this->initIntersectionUsers(self::MODE_U1_EXPERT_U2_NORMAL);
        $this->addNewVote($this->intersection, $this->user1);
        $this->checkIntersectionHasAGoodAverage();
    }
    
    /*
     * Test the creation of a new vote, considering that all the users (
     * having emitting a vote on this entity) are normal users
     *
     *  @depends testRecomputeAvgUserAllNormal
     */
    public function testAddNewVoteAllNormal()
    {
        $this->initConnexion();
        $this->initIntersectionUsers(self::MODE_USER_ALL_NORMAL);
        $this->addNewVote($this->intersection, $this->user1);
        $this->checkIntersectionHasAGoodAverage();
    }
    
    
    /*
    * Test the creation of a new vote, considering that all the users (
    * having emitting a vote on this entity) as experts
    *
    * @depends testRecomputeAvgUserAllExperts
    */
    public function testAddNewVoteAllExperts()
    {
        $this->initConnexion();
        $this->initIntersectionUsers(self::MODE_USER_ALL_EXPERT);
        $this->addNewVote($this->intersection, $this->user1);
        $this->checkIntersectionHasAGoodAverage();
    }
}
