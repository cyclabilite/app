<?php

namespace CL\Cyclabilite\VoteBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Test the ways are available
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class WayTest extends WebTestCase
{

   public function testListAllIsSuccessfull()
   {

      $client = static::createClient();

      $client->request('GET', '/way/list/all.json');

      $response = $client->getResponse();

      $this->assertTrue($client->getResponse()->isSuccessful());

      return $response;
   }

   /**
    * @depends testListAllIsSuccessfull
    * @param \Symfony\Component\HttpFoundation\Response $response the response from testListAllIsSuccessfull
    */
   public function testListAllIsJsonType($response)
   {
      $this->assertTrue($response
            ->headers->contains('Content-Type', 'application/json'));
   }

   /**
    * @depends testListAllIsSuccessfull
    * @param \Symfony\Component\HttpFoundation\Response $response
    */
   public function testListAllIsNotEmpty($response)
   {
      $segmentList = json_decode($response->getContent(), true);

      $this->assertLessThanOrEqual(100, count($segmentList));

      return $segmentList;
   }

   /**
    * @depends testListAllIsNotEmpty
    * @param array $segmentList thearray of segment returned by testListAllIsNotEmpty
    */
   public function testListBBOXisSuccessfull($segmentList)
   {
      $bbox = $this->createBBOX($segmentList[0]);

      $client = static::createClient();

      $client->request('GET', '/way/list/bbox.json', array('bbox' => $bbox));

      $response = $client->getResponse();

      $this->assertTrue($response->isSuccessful());
   }

   private function createBBOX($segment)
   {
      $east = $segment['geometry']['coordinates'][0][0];
      $west = $segment['geometry']['coordinates'][0][0];
      $north = $segment['geometry']['coordinates'][0][1];
      $south = $segment['geometry']['coordinates'][0][1];

      foreach ($segment['geometry']['coordinates'] as $point) {
         if ($point[1] > $north) {
            $north = $point[1];
         }

         if ($point[1] < $south) {
            $south = $point[1];
         }

         if ($point[0] > $east) {
            $east = $point[0];
         }

         if ($point[0] < $west) {
            $west = $point[0];
         }
      }
      return "$west,$south,$east,$north";
   }
}
