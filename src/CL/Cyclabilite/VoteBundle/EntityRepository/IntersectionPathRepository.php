<?php

namespace CL\Cyclabilite\VoteBundle\EntityRepository;

use Doctrine\ORM\EntityRepository;

use CL\Cyclabilite\VoteBundle\Entity\IntersectionPath;


/**
 * Repository of IntersectionPathRepository
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class IntersectionPathRepository extends EntityRepository
{
   
   public function findByOrCreate(array $criteria, array $orderBy = null, $limit = null, $offset = null)
   {
      //catch the request by node/segmentStart/segmentEnd
      if (isset($criteria['node'])
         && array_key_exists('segmentStart', $criteria)
         && array_key_exists('segmentEnd', $criteria)) {
         
         $existingIntersectionPaths = parent::findBy($criteria, $orderBy, 
               $limit, $offset);
         
         //if no existingIntersection, create a new one and return it
         if (count($existingIntersectionPaths) === 0) {
            $newIntersectionPath = new IntersectionPath();
            $newIntersectionPath->setNode($criteria['node'])
                  ->setSegmentEnd($criteria['segmentEnd'])
                  ->setSegmentStart($criteria['segmentStart']);
            
            $this->getEntityManager()->persist($newIntersectionPath);
            $this->getEntityManager()->flush($newIntersectionPath);
            
            return array($newIntersectionPath);
         }
      } else {
         throw new \Exception('The criteria array must contain the keys node, segmentStart and segmentEnd.');
      }
      
      //else, 
      return parent::findBy($criteria, $orderBy, $limit, $offset);
   }
}
