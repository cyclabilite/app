<?php
namespace CL\Cyclabilite\VoteBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class RecomputeAverageCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('cyclab:vote:recompute-avg')
            ->setDescription('Recompute average (to perform when the experts list has been changed).')
            ->setHelp('This command will recompute the pre-computed note (average) of each segment and each intersection  (to perform when the experts list has been changed).');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'Recompudation of all the pre-computed average',
            '=============================================',
            '(use php -d memory_limit=512M to change this parameter)',
            '(use php -d max_execution_time=600 to change this parameter)',
            '',
        ]);

        $recomputeAverage = $this->getContainer()->
            get('cl_cyclabilite_vote.recompute_all_average');
        $statusMessage = $recomputeAverage->recomputeAll();
        $output->writeln($statusMessage);
    }
}
