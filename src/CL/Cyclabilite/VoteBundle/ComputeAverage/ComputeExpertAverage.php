<?php

namespace CL\Cyclabilite\VoteBundle\ComputeAverage;

use CL\Cyclabilite\VoteBundle\Entity\VoteAbstract;
use CL\Cyclabilite\VoteBundle\Entity\Node;
use CL\Cyclabilite\VoteBundle\Entity\Segment;

/**
 * Compute the average with an expert way : the average is as follows :
 * 0.5 (AVERAGE OF THE EXPERT VOTE) + 0.5 (AVERAGE OF THE NORMAL VOTE).
 *
 * @author Champs-Libres <info@champs-libres.coop>
 */
class ComputeExpertAverage implements ComputeAverageInterface
{
    const KEY_AVERAGE_NON_EXPERT = 'voteAverageNonExpert';
    const KEY_AVERAGE_EXPERT = 'voteAverageExpert';
    const KEY_NUMBER_NON_EXPERT = 'voteNumberNonExpert';
    const KEY_NUMBER_EXPERT = 'voteNumberExpert';
    const KEY_VOTES_ID_IN_EXPERT_AVERAGE = 'voteIdInExpertAverage';
    const KEY_VOTES_ID_IN_NON_EXPERT_AVERAGE = 'voteIdInNONExpertAverage';
    
    const NAME = 'Expert Average';
    
    const EXPERT_VOTE_PERCENTAGE= 0.5; // The final vote will computed as
    //      EXPERT_VOTE_PERCENTAGE * (AVERAGE OF EXPERT VOTES)
    //      + (1 - EXPERT_VOTE_PERCENTAGE) * (AVERAGE OF NON-EXPERT VOTES)
    
    
    /*
    If True for the computation of the average of an entity we only consider 
    one vote for each user profile (the most recent one) otherwise regarding to
    $this->$onlyConsiderOneVoteByProfile either all the vote are used
    for the average computation, either only one vote for each user are used.
    */
    private $onlyConsiderOneVoteByProfile = true;
    
    /*
    If True for the computation of the average of an entity we only consider 
    one vote for each user (the most recent one) otherwise all the vote are used
    for the average computation. This only works if $onlyConsiderOneVoteByProfile
    is false.
    */
    private $onlyConsiderOneVoteForUser = true;
    
    public function __construct($onlyConsiderOneVoteByProfile = true, $onlyConsiderOneVoteForUser = true)
    {
        $this->onlyConsiderOneVoteByProfile = $onlyConsiderOneVoteByProfile;
        $this->onlyConsiderOneVoteForUser = $onlyConsiderOneVoteForUser;
    }
    
    /**
     * see ComputeAverageInterface
     */
    public function getName()
    {
        return self::NAME;
    }
    
    /**
     * Init the averageData array (as there is no vote)
     * @param array $averageData
     * @return array The initialized $averageData
     */
    private function initAverageData($averageData)
    {
        $averageData[self::KEY_AVERAGE_EXPERT] = 0;
        $averageData[self::KEY_NUMBER_EXPERT] = 0;
        $averageData[self::KEY_AVERAGE_NON_EXPERT] = 0;
        $averageData[self::KEY_NUMBER_NON_EXPERT] = 0;
        $averageData[self::KEY_VOTES_ID_IN_EXPERT_AVERAGE] = [];
        $averageData[self::KEY_VOTES_ID_IN_NON_EXPERT_AVERAGE] = [];
        return $averageData;
    }
    
    /**
     * Update the average & voteNumber attributes of an $entity
     * regarding to the data contained in its $averageData attribute.
     *
     * @param Entity $entity The entity
     */
    private function updateAverageAndVoteNumberFromAverageData($entity)
    {
        $averageData = $entity->getAverageData();
        
        if ($averageData[self::KEY_NUMBER_NON_EXPERT] > 0) {
            if ($averageData[self::KEY_NUMBER_EXPERT] > 0) {
                $entity->setVoteAverage(
                    ($averageData[self::KEY_AVERAGE_NON_EXPERT] * (1 - self::EXPERT_VOTE_PERCENTAGE)) +
                    ($averageData[self::KEY_AVERAGE_EXPERT] * self::EXPERT_VOTE_PERCENTAGE)
                );
            } else {
                $entity->setVoteAverage($averageData[self::KEY_AVERAGE_NON_EXPERT]);
            }
        } else {
            if ($averageData[self::KEY_NUMBER_EXPERT] > 0) {
                $entity->setVoteAverage($averageData[self::KEY_AVERAGE_EXPERT]);
            }
        }
        
        $entity->setVoteNumber($averageData[self::KEY_NUMBER_EXPERT] + $averageData[self::KEY_NUMBER_NON_EXPERT]);
    }
    
    /**
     * see ComputeAverageInterface
     */
    public function updateAverageForNewVote($entity, VoteAbstract $newVote)
    {
        $keyVotesIdInAverage = self::KEY_VOTES_ID_IN_NON_EXPERT_AVERAGE;
        if ($newVote->isExpertVote()) {
            $keyVotesIdInAverage = self::KEY_VOTES_ID_IN_EXPERT_AVERAGE;
        }
        $averageData = $entity->getAverageData();
        $addNewVoteToAverage = true;
        
        if (! array_key_exists($keyVotesIdInAverage, $averageData)) {
            $averageData = $this->initAverageData($averageData);
            $entity->setAverageData($averageData);
        }
        
        if ($this->onlyConsiderOneVoteByProfile) {
            $votes = $entity->getVotes();
            foreach ($votes as $v) {
                if (in_array($v->getId(), $averageData[$keyVotesIdInAverage]) //car plsr votes pour intersection path
                && $v->getUser() == $newVote->getUser()
                && $v->getProfile() == $newVote->getProfile()
                // si instance de node alors si meme intersection path ajoute
                && ((! $entity instanceof Node) || $v->getIntersectionPath() == $newVote->getIntersectionPath())
                // si instance de segment alors regarde si meme intersection
                && ((! $entity instanceof Segment) || $v->getDirection() == $newVote->getDirection())) {
                    if ($v->getDatetimeVote() < $newVote->getDatetimeVote()
                        || ($v->getDatetimeVote() == $newVote->getDatetimeVote() && $v->getDatetimeCreation() < $newVote->getDatetimeCreation())) {
                        $this->removeVoteFromAverage($entity, $v);
                    } else {
                        // a vote already exist and is more recent
                        $addNewVoteToAverage = false;
                    }
                    break;
                }
            }
        } elseif ($this->onlyConsiderOneVoteForUser) {
            // check if the user has already vote, if it is the case, then
            // remove the vote
            // beware we can for an intersection we want to count one vote
            // for user and intersection path
            throw new \Exception("One Vote For User has not been created yet", 1);
            $votes = $entity->getVotes();
            foreach ($votes as $v) {
                if (in_array($v->getId(), $averageData[$keyVotesIdInAverage])
                        && $v->getUser() == $newVote->getUser()) {
                            
                    // checker la date et verifier si est le plus recent
                    $this->removeVoteFromAverage($entity, $v); // ATTENTION VOTES DEJA RETIRE
                    break;
                }
            }
        }
        
        if ($addNewVoteToAverage) {
            $this->addVoteToAverage($entity, $newVote);
        }
    }
    
    /**
    * Add a vote to the average
    *
    * @param $entity The given entity (segment, intersection or intersection_path)
    * (the entity must use the VotesInformationTrait)
    * @param $vote The vote to add to the average
    */
    private function addVoteToAverage($entity, VoteAbstract $vote)
    {
        $this->updateAverage($entity, $vote);
    }
    
    /**
    * Remove a vote from the average
    *
    * @param $entity The given entity (segment, intersection or intersection_path)
    * (the entity must use the VotesInformationTrait)
    * @param $vote The vote to remove from the average
    */
    private function removeVoteFromAverage($entity, VoteAbstract $vote)
    {
        $this->updateAverage($entity, $vote, true);
    }
    
    /**
    * Add or remove a vote to / from the average
    *
    * @param $entity The given entity (segment, intersection or intersection_path)
    * (the entity must use the VotesInformationTrait)
    * @param $vote The vote to add or remove to the average
    * @param $rmVote If true the function will remove the vote, otherwise the
    * function will add the vote. By default this parameter is false.
    */
    private function updateAverage($entity, VoteAbstract $vote, $rmVote = false)
    {
        $averageData = $entity->getAverageData();
        
        $keyVoteAverage = self::KEY_AVERAGE_NON_EXPERT;
        $keyVoteNumber = self::KEY_NUMBER_NON_EXPERT;
        $keyVotesIdInAverage = self::KEY_VOTES_ID_IN_NON_EXPERT_AVERAGE;
        if ($vote->isExpertVote()) {
            $keyVoteAverage = self::KEY_AVERAGE_EXPERT;
            $keyVoteNumber = self::KEY_NUMBER_EXPERT;
            $keyVotesIdInAverage= self::KEY_VOTES_ID_IN_EXPERT_AVERAGE;
        }
        
        if ($rmVote) {
            if (! $entity->getVotes()->contains($vote)) {
                throw new \Exception("It is only possible to remove an vote of the entity to the average", 1);
            }
        } elseif (! array_key_exists(self::KEY_NUMBER_EXPERT, $averageData)) {
            throw new \Exception("AverageData must be initialized", 1);
        }
        
        if ($rmVote) {
            if ($averageData[$keyVoteNumber] <= 1) {
                $averageData[$keyVoteAverage] = 0;
                $averageData[$keyVoteNumber] = 0;
            } else {
                $averageData[$keyVoteAverage] = ($averageData[$keyVoteAverage] * $averageData[$keyVoteNumber]
                    - $vote->getValue()) / ($averageData[$keyVoteNumber] - 1);
                $averageData[$keyVoteNumber] = $averageData[$keyVoteNumber] - 1;
            }
            
            array_splice(
                $averageData[$keyVotesIdInAverage],
                array_search($vote->getId(), $averageData[$keyVotesIdInAverage]),
                1
            );
        } else {
            $averageData[$keyVoteAverage] = ($averageData[$keyVoteAverage] * $averageData[$keyVoteNumber]
                + $vote->getValue()) / ($averageData[$keyVoteNumber] + 1);
            $averageData[$keyVoteNumber] = $averageData[$keyVoteNumber] + 1;
            
            array_push($averageData[$keyVotesIdInAverage], $vote->getId());
        }
        
        $entity->setAverageData($averageData);
        $this->updateAverageAndVoteNumberFromAverageData($entity);
    }

    /**
     * see ComputeAverageInterface
     */
    public function recomputeAverage($entity)
    {
        $averageData = $this->initAverageData(array());
        
        $votesToConsider = [];
        if ($this->onlyConsiderOneVoteByProfile) {
            if ($entity instanceof Segment) {
                foreach ($entity->getVotesByDirection() as $direction => $votesByDirection) {
                    foreach ($entity->sortVotesByUserIdAndProfileId($votesByDirection) as $userId => $votesByProfileForUserId) {
                        foreach ($votesByProfileForUserId as $profileId => $votes) {
                            array_push($votesToConsider, $votes[0]);
                        }
                    }
                }
            } else {
                if ($entity instanceof Node) {
                    $entitiesToConsider = $entity->getIntersectionPaths();
                } else {
                    $entitiesToConsider = [$entity];
                }
                
                foreach ($entitiesToConsider as $e) {
                    foreach ($e->getVotesByUserIdAndProfileId() as $userId => $votesByProfileForUserId) {
                        foreach ($votesByProfileForUserId as $profileId => $votes) {
                            array_push($votesToConsider, $votes[0]);
                        }
                    }
                }
            }
        } elseif ($this->onlyConsiderOneVoteForUser) {
            throw new Exception("GERER  NODE ET getIntersectionPaths", 1);
            
            foreach ($entity->getVotesByUserId() as $userId => $votes) {
                array_push($votesToConsider, $votes[0]);
            }
        } else {
            throw new Exception("GERER  NODE ET getIntersectionPaths", 1);
            
            $votesToConsider = $entity->getVotes();
        }

        foreach ($votesToConsider as $vote) {
            $keyVoteAverage = self::KEY_AVERAGE_NON_EXPERT;
            $keyVoteNumber = self::KEY_NUMBER_NON_EXPERT;
            $keyVotesIdInAverage = self::KEY_VOTES_ID_IN_NON_EXPERT_AVERAGE;
            if ($vote->isExpertVote()) {
                $keyVoteAverage = self::KEY_AVERAGE_EXPERT;
                $keyVoteNumber = self::KEY_NUMBER_EXPERT;
                $keyVotesIdInAverage = self::KEY_VOTES_ID_IN_EXPERT_AVERAGE;
            }
            
            $averageData[$keyVoteAverage] = ($averageData[$keyVoteAverage] * $averageData[$keyVoteNumber]
                + $vote->getValue()) / ($averageData[$keyVoteNumber] + 1);
            $averageData[$keyVoteNumber] = $averageData[$keyVoteNumber] + 1;
            array_push($averageData[$keyVotesIdInAverage], $vote->getId());
        }
        
        $entity->setAverageData($averageData);
        $this->updateAverageAndVoteNumberFromAverageData($entity);
    }
}
