<?php

namespace CL\Cyclabilite\VoteBundle\ComputeAverage;

use Doctrine\ORM\Event\LifecycleEventArgs;
use CL\Cyclabilite\VoteBundle\Entity\VoteIntersectionPath;
use CL\Cyclabilite\VoteBundle\Entity\VoteSegment;
use CL\Cyclabilite\VoteBundle\Entity\VoteAbstract;

use CL\Cyclabilite\VoteBundle\ComputeAverage\ComputeAverageInterface;

/**
 * Compute the average in a normal way : the average is as follows :
 * SUM OF THE VOTES / NUMBER OF THE VOTES
 *
 * @author Champs-Libres <info@champs-libres.coop>
 */
class ComputeNormalAverage implements ComputeAverageInterface
{
    const NAME = 'Normal average';
    
    /**
     * see ComputeAverageInterface
     */
    public function getName()
    {
        return self::NAME;
    }
    
    /**
     * see ComputeAverageInterface
     */
    public function updateAverageForNewVote($entity, VoteAbstract $newVote)
    {        
        if ($entity->getVoteAverage() === null) {
            $entity->setVoteAverage(0);
            $entity->setVoteNumber(0);
        }

        $entity->setVoteAverage((
            $entity->getVoteAverage() * $entity->getVoteNumber() + $newVote->getValue()
                ) / ($entity->getVoteNumber() + 1));
        $entity->setVoteNumber($entity->getVoteNumber() + 1);
    }
    
    /**
     * see ComputeAverageInterface
     */
    public function recomputeAverage($entity)
    {
        $sumOfVotes = 0;
        $voteNumber = 0;
        
        foreach ($entity->getVotes() as $vote) {
            $sumOfVotes += $vote->getValue();
            $voteNumber = $voteNumber + 1;
        }

        $entity->setVoteNumber($voteNumber);
        
        if($voteNumber > 0) {
            $entity->setVoteAverage($sumOfVotes / $voteNumber);
        } else {
            $entity->setVoteAverage(0);
        }
        
    }
}
