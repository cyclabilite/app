<?php

namespace CL\Cyclabilite\VoteBundle\ComputeAverage;
use CL\Cyclabilite\VoteBundle\Entity\VoteAbstract;

// TODO à mettre dans src/Service/ComputeAverage

/**
 * @author Champs-Libres <info@champs-libres.coop>
 */
interface ComputeAverageInterface 
{
    /**
     * For a given $entity (segment or intersection or intersection path),
     * compute the vote-average for a new vote. It is expected that this
     * function only change the parameters voteAverage, voteNumber and
     * averageData.
     *
     * @param $entity The given entity (segment, intersection or intersection_path)
     * (the entity must use the VotesInformationTrait)
     * @param $newVote The new vote for computing the average
     * @todo create an interface for the entites  segment, intersection or intersection_path
     * that use the VotesInformationTrait
     */
    public function updateAverageForNewVote($entity, VoteAbstract $newVote);

    /**
     * For a given $entity (segment or intersection or intersection path),
     * recompute the average. It is expected that this
     * function only change the parameters voteAverage, voteNumber and
     * averageData.
     *
     * @param $entity The given entity (segment, intersection or intersection_path)
     * (the entity must use the VotesInformationTrait)
     */
    public function recomputeAverage($entity);


    /**
     * Returns a unique name for identifying the method used.
     */
    public function getName();
}
