<?php

namespace CL\Cyclabilite\VoteBundle\ComputeAverage;

use Doctrine\ORM\Event\LifecycleEventArgs;
use CL\Cyclabilite\VoteBundle\Entity\VoteIntersectionPath;
use CL\Cyclabilite\VoteBundle\Entity\VoteSegment;

/**
 * Listener that will listen for the prePersist event on
 * VoteIntersectionPath and VoteSegment such that
 * it will compute the new average of the associatedEntity
 *
 * @author Champs-Libres <info@champs-libres.coop>
 */
class PrePersistListener
{
    /** The class used for the averageComputer */
    private $averageComputer;
    
    /**
     * Constructor
     */
    public function __construct(ComputeAverageInterface $averageComputer)
    {
        $this->averageComputer = $averageComputer;
    }
    
    /**
     * Listen to the prePersist event of the doctrine.event_listener
     * dispatcher. For the VoteIntersectionPath & VoteSegment will
     * throw the good procedure to update the average.
     */
    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();
        
        if ($entity instanceof VoteIntersectionPath || $entity instanceof VoteSegment) {
            $this->averageComputer->updateAverageForNewVote(
                $entity->getAssociatedEntity(),
                $entity
            );
            
            if ($entity instanceof VoteIntersectionPath) {
                $this->averageComputer->updateAverageForNewVote(
                    $entity->getAssociatedEntity()->getNode(),
                    $entity
                );
            }
        }
        $em->flush();
    }
}
