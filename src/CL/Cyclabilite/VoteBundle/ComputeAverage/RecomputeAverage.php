<?php

namespace CL\Cyclabilite\VoteBundle\ComputeAverage;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\EntityManager;
use CL\Cyclabilite\VoteBundle\Entity\VoteIntersectionPath;
use CL\Cyclabilite\VoteBundle\Entity\VoteSegment;

/**
 * Listener that will listen for the prePersist event for the entities
 * VoteIntersectionPath and VoteSegment such that it will compute the
 * new average of the associated entity
 *
 * @author Champs-Libres <info@champs-libres.coop>
 */
class RecomputeAverage
{
    /** The entity manager */
    private $em;
    
    private $averageComputer;
    
    /**
     * Constructor
     */
    public function __construct(EntityManager $em, ComputeAverageInterface $averageComputer)
    {
        $this->em = $em;
        $this->averageComputer = $averageComputer;
    }
    
    /**
     * Do the recomputation of the average for a given entity
     *
     * @param $entity The entity (Node or IntersectionPath or Segment)
     */
    public function recompute($entity)
    {
        $this->averageComputer->recomputeAverage($e);
    }

    /**
     * Do the recomputation of all the votes over the entities
     *
     * @return String A return message.
     */
    public function recomputeAll()
    {
        $entityClass = ['Node', 'IntersectionPath', 'Segment'];
        $returnMessage = $this->averageComputer->getName()." used.";
        
        foreach ($entityClass as $eC) {
            $qbCount = $this->em->createQueryBuilder();
            $qbCount->select('count(e.id)');
            $qbCount->from('CLCyclabiliteVoteBundle:'.$eC, 'e');
            $entityNumber = $qbCount->getQuery()->getSingleScalarResult();
            
            $returnMessage = $returnMessage.' CLCyclabiliteVoteBundle:'.$eC.' processing : ';
            
            $repository = $this->em->getRepository('CLCyclabiliteVoteBundle:'.$eC);

            $query = $repository->createQueryBuilder('e')
                ->where('e.voteNumber > 0')
                ->getQuery();
        
            $entities = $query->getResult();

            $cpt = 0;
                
            foreach ($entities as $e) {
                $this->averageComputer->recomputeAverage($e);
                $cpt += 1;

                if($cpt > 1000) {
                    $this->em->flush();
                    $cpt = 0;

                    $returnMessage = $returnMessage.".";
                }

            }
            $this->em->flush();
            
            $returnMessage = $returnMessage." Done!";
        }
        return $returnMessage;
    }
}
