<?php

namespace CL\Cyclabilite\VoteBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;


/**
 * Direction of a vote
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class DirectionType extends AbstractType
{
   public function configureOptions(OptionsResolver $resolver)
   {
      $resolver->setDefaults(array(
          'choices' => array(
              'forward' => 'forward',
              'backward' => 'backward',
              'both' => 'both'
          ),
      ));
   }

   public function getName()
   {
      return 'direction';
   }

   public function getParent()
   {
      return ChoiceType::class;
   }

}
