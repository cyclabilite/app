<?php

namespace CL\Cyclabilite\VoteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use CL\Cyclabilite\UserBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

/**
 * Description of VoteAbstractType
 *
 * @author Julien Fastré <julien arobase fastre point info>
 */
abstract class AbstractVoteType extends AbstractType
{   /**
    *
    * @var TokenStorage Token to get User
    */
   private $token;

   public function __construct(TokenStorage $token)
   {
      $this->token = $token;
   }

   public function buildForm(FormBuilderInterface $builder, array $options)
   {
      $user = $this->token->getToken()->getUser();

      $builder
         ->add('value', ChoiceType::class, array(
             'choices' => array(
                 '1' => '1',
                 '2' => '2',
                 '3' => '3',
                 '4' => '4',
                 '5' => '5'
             ),
             'expanded' => true,
             'multiple' => false
         ))
         ->add('datetime_vote', DateTimeType::class)
         ->add('profile', EntityType::class, array(
             'class' => 'CL\Cyclabilite\UserBundle\Entity\Profile',
             'query_builder' => function (EntityRepository $er) use ($user) {
                return $er->CreateQueryBuilder('p')
                        ->where('p.user = ?1')
                        ->setParameter(1, $user);
             }
         ))
      ;
   }

}
