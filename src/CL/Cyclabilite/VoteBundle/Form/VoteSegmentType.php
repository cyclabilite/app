<?php

namespace CL\Cyclabilite\VoteBundle\Form;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use CL\Cyclabilite\UserBundle\Entity\User;
use CL\Cyclabilite\VoteBundle\Form\Type\DirectionType;

class VoteSegmentType extends AbstractVoteType
{
   /**
    * @param FormBuilderInterface $builder
    * @param array $options
    */
   public function buildForm(FormBuilderInterface $builder, array $options)
   {
      parent::buildForm($builder, $options);

      $builder
         ->add('direction', DirectionType::class);
   }

   /**
    * @param OptionsResolver $resolver
    */
   public function configureOptions(OptionsResolver $resolver)
   {
      $resolver->setDefaults(array(
          'data_class' => 'CL\Cyclabilite\VoteBundle\Entity\VoteSegment'
      ));
   }
}
