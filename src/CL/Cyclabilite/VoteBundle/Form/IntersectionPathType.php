<?php

namespace CL\Cyclabilite\VoteBundle\Form;

use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;


/**
 * Form to chose an intersection Path
 *
 * @author Champs-Libres COOP <info@champs-libres.coop>
 */
class IntersectionPathType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('segmentStart', TextType::class)
            ->add('segmentEnd', TextType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CL\Cyclabilite\VoteBundle\Entity\IntersectionPath'
        ));
    }
}