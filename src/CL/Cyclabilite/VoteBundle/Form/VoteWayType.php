<?php

namespace CL\Cyclabilite\VoteBundle\Form;

use Symfony\Component\OptionsResolver\OptionsResolver;
use CL\Cyclabilite\VoteBundle\Form\AbstractVoteType;
use Symfony\Component\Form\FormBuilderInterface;
use CL\Cyclabilite\VoteBundle\Form\Type\DirectionType;

/**
 * Vote on a way
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class VoteWayType extends AbstractVoteType
{
   /**
    * @param OptionsResolver $resolver
    */
   public function configureOptions(OptionsResolver $resolver)
   {
      $resolver->setDefaults(array(
          'data_class' => 'CL\Cyclabilite\VoteBundle\Entity\VoteWay'
      ));
   }

   /**
    * @param FormBuilderInterface $builder
    * @param array $options
    */
   public function buildForm(FormBuilderInterface $builder, array $options)
   {
      parent::buildForm($builder, $options);

      $builder->add('direction', DirectionType::class);
   }

}
