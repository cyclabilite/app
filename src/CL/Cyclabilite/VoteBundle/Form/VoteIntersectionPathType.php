<?php

namespace CL\Cyclabilite\VoteBundle\Form;

use Symfony\Component\OptionsResolver\OptionsResolver;
use CL\Cyclabilite\VoteBundle\Form\AbstractVoteType;
use Symfony\Component\Form\FormBuilderInterface;
use CL\Cyclabilite\VoteBundle\Entity\Node;


/**
 * Vote on IntersectionPath
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class VoteIntersectionPathType extends AbstractVoteType
{
   /**
    *
    * @var Node
    */
   private $node;

   const NAME = 'VoteIntersectionPathType';

   public function buildForm(FormBuilderInterface $builder, array $options)
   {
      parent::buildForm($builder, $options);

      $builder
         ->add('intersectionPath', IntersectionPathType::class)
         ->add('value');
   }

   public function configureOptions(OptionsResolver $resolver)
   {
      $resolver->setDefaults(array(
         'data_class' => 'CL\Cyclabilite\VoteBundle\Entity\VoteIntersectionPath'
      ));
   }
}
