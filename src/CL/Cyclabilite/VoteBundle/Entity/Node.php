<?php

namespace CL\Cyclabilite\VoteBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Node
 * @ORM\Entity
 * @ORM\Table(name="nodes")
 */
class Node extends NodeAbstract
{
    /**
     * @var IntersectionPath The intersection path that are associated to this
     * Intersection
     *
     * TODO verifier si creer a la volée (normalement oui) -> decire
     * @ORM\OneToMany(targetEntity="IntersectionPath", mappedBy="node")
     */
    protected $intersectionPaths;


    /**
     * @var Segment  The segment that starts on this intersection.
     * @ORM\OneToMany(targetEntity="Segment", mappedBy="nodeStart")
     */
    protected $startingSegments;

    /**
     * @var Segment The segments that ends on this intersection.
     * @ORM\OneToMany(targetEntity="Segment", mappedBy="nodeEnd")
     */
    protected $endingSegments;
}
