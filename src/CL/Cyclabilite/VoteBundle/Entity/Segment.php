<?php

namespace CL\Cyclabilite\VoteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Segment
 * @ORM\Entity(repositoryClass="CL\Cyclabilite\VoteBundle\Repository\SegmentRepository")
 * @ORM\Table(name="cyclab_segments")
 */
class Segment extends SegmentAbstract
{
    /**
     * @var Way
     * @ORM\ManyToOne(targetEntity="Way", inversedBy="segments")
     * @ORM\JoinColumn(name="way_id", referencedColumnName="id")
     */
    protected $way;

    /**
     * @var Node
     * @ORM\ManyToOne(targetEntity="Node")
     */
    protected $nodeStart;

    /**
     * @var Node
     * @ORM\ManyToOne(targetEntity="Node")
     */
    protected $nodeEnd;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     * @ORM\OneToMany(targetEntity="CL\Cyclabilite\VoteBundle\Entity\VoteSegment", mappedBy="segment")
     * @ORM\OrderBy({"datetime_vote" = "DESC"})
     */
    protected $votes;
}
