<?php

namespace CL\Cyclabilite\VoteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Vote on SegmentDeleted
 *
 * @ORM\Entity
 * @ORM\Table(name="deleted_cyclab_segment_vote")
 * @ORM\HasLifecycleCallbacks
 */
class VoteSegmentDeleted extends VoteSegmentAbstract
{
    /**
     * @var SegmentDeleted
     * @ORM\ManyToOne(targetEntity="SegmentDeleted", inversedBy="votes")
     * @ORM\JoinColumn(name="segment_id", referencedColumnName="id")
     */
    protected $segment;

    /**
     * Create copy of the vote associated to an existing segment (not deleted)
     * @param Segment $newSegment The existing segment
     * @return VoteSegment THe copy of the deleted vote
     */
    public function transferTo(Segment $newSegment) {
        $newVoteSegment = new VoteSegment();
        $this->copyDataTo($newVoteSegment);
        $newVoteSegment->setSegment($newSegment);
        return $newVoteSegment;
    }
}
