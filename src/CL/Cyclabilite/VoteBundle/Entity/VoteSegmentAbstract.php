<?php

namespace CL\Cyclabilite\VoteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use CL\Cyclabilite\VoteBundle\Entity\VoteAbstract;
use CL\Cyclabilite\VoteBundle\Entity\VoteSegmentAbstract;

/**
 * Vote on a segment
 * @ORM\MappedSuperclass
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 * @author Marc Ducobu <marc.ducobu@champs-libres.coop>
 * @version 1.0.2 directionForward is now replaced by direction
 */
abstract class VoteSegmentAbstract extends VoteAbstract
{
    const DIRECTION_FORWARD = 'forward';
    const DIRECTION_BACKWARD = 'backward';
    const DIRECTION_BOTH = 'both';

    /**
     * @var SegmentAbstract
     */
    protected $segment;

    /**
     * true if the vote is in the forward direction (from
     * node_start to node_end), false if backward direction
     *
     * @var boolean
     * @ORM\Column(type="string")
     */
    protected $direction = self::DIRECTION_BOTH;

    /**
     * Copy some data from the object ($this) to another VoteAbstre ($copy)
     *
     * The copied data are : direction (+ heritage)
     */
    public function copyDataTo($copy)
    {
        parent::copyDataTo($copy);
        $copy->setDirection($this->getDirection());
    }

    /**
     * Set segment
     *
     * @param \CL\Cyclabilite\VoteBundle\Entity\Segment $segment
     * @return VoteSegment
     */
    public function setSegment(\CL\Cyclabilite\VoteBundle\Entity\Segment $segment = null)
    {
        $this->segment = $segment;
        return $this;
    }

    /**
     * Get segment
     *
     * @return \CL\Cyclabilite\VoteBundle\Entity\Segment
     */
    public function getSegment()
    {
        return $this->segment;
    }

    /**
     * Set the direction. The direction is a string (see the constants
     * DIRECTION_FORWARD, DIRECTION_BACKWARD and DIRECTION_BOTH).
     *
     * The forward direction means that the cyclist goes form node_start to
     * node_end
     *
     * @param $direction The direction (must be DIRECTION_FORWARD or
     * or DIRECTION_BACKWARD or DIRECTION_BOTH)
     */
    public function setDirection($direction)
    {
        $this->direction = $direction;
        return $this;
    }

    /**
     * Get the direction. The direction is a string (see the constants
     * DIRECTION_FORWARD, DIRECTION_BACKWARD and DIRECTION_BOTH).
     *
     * The forward direction means that the cyclist goes form node_start to
     * node_end
     *
     * @return The direction : this is one of these class constants :
     * DIRECTION_FORWARD, DIRECTION_BACKWARD and DIRECTION_BOTH
     */
    public function getDirection()
    {
        return $this->direction;
    }

    public function getType()
    {
        return 'segment';
    }

    public function getAssociatedEntity()
    {
        return $this->getSegment();
    }
}
