<?php

namespace CL\Cyclabilite\VoteBundle\Entity;

use Symfony\Component\Validator\ExecutionContextInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * IntersectionPath
 * @ORM\Entity(repositoryClass="CL\Cyclabilite\VoteBundle\EntityRepository\IntersectionPathRepository")
 * @ORM\Table(name="cyclab_intersection_paths")
 */
class IntersectionPath extends IntersectionPathAbstract
{
    /**
     * @var \CL\Cyclabilite\VoteBundle\Entity\Node
     * @ORM\ManyToOne(targetEntity="CL\Cyclabilite\VoteBundle\Entity\Node")
     */
    protected $node;

    /**
     * @var \CL\Cyclabilite\VoteBundle\Entity\Segment
     * @ORM\ManyToOne(targetEntity="CL\Cyclabilite\VoteBundle\Entity\Segment")
     */
    protected $segmentStart;

    /**
     * @var \CL\Cyclabilite\VoteBundle\Entity\Segment
     * @ORM\ManyToOne(targetEntity="CL\Cyclabilite\VoteBundle\Entity\Segment")
     */
    protected $segmentEnd;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @ORM\OneToMany(targetEntity="CL\Cyclabilite\VoteBundle\Entity\VoteIntersectionPath", mappedBy="intersectionPath")
     * @ORM\OrderBy({"datetime_vote"="DESC", "datetime_creation"="DESC"})
     */
    protected $votes;
}
