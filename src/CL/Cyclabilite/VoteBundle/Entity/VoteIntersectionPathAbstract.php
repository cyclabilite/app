<?php

namespace CL\Cyclabilite\VoteBundle\Entity;

use CL\Cyclabilite\VoteBundle\Entity\VoteAbstract;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\MappedSuperclass
 */
class VoteIntersectionPathAbstract extends VoteAbstract
{
    protected $intersectionPath;

    /**
     * Set intersection
     *
     * @param \CL\Cyclabilite\VoteBundle\Entity\IntersectionPath $intersection
     * @return VoteIntersectionPath
     */
    public function setIntersectionPath(\CL\Cyclabilite\VoteBundle\Entity\IntersectionPath $intersectionPath = null)
    {
        $this->intersectionPath = $intersectionPath;

        return $this;
    }

    /**
     * Get intersection
     *
     * @return \CL\Cyclabilite\VoteBundle\Entity\IntersectionPath
     */
    public function getIntersectionPath()
    {
        return $this->intersectionPath;
    }

    public function getType()
    {
        return 'intersection';
    }

    public function getAssociatedEntity()
    {
        return $this->getIntersectionPath();
    }

    protected function takeVoteIntoAccountForEntity()
    {
        parent::takeVoteIntoAccountForEntity();
        $this->getAssociatedEntity()->getNode()->takeVoteIntoAccount($this);
    }
}
