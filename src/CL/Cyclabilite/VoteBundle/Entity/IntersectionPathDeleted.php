<?php

namespace CL\Cyclabilite\VoteBundle\Entity;

use Symfony\Component\Validator\ExecutionContextInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * IntersectionPath
 * @ORM\Entity
 * @ORM\Table(name="deleted_cyclab_intersection_paths")
 */
class IntersectionPathDeleted extends IntersectionPathAbstract
{
    /**
     * @var \CL\Cyclabilite\VoteBundle\Entity\NodeAll
     * @ORM\ManyToOne(targetEntity="CL\Cyclabilite\VoteBundle\Entity\NodeAll")
     */
    protected $node;

    /**
     * @var \CL\Cyclabilite\VoteBundle\Entity\SegmentDeleted
     * @ORM\ManyToOne(targetEntity="CL\Cyclabilite\VoteBundle\Entity\SegmentDeleted")
     */
    protected $segmentStart;

    /**
     * @var \CL\Cyclabilite\VoteBundle\Entity\SegmentDeleted
     * @ORM\ManyToOne(targetEntity="CL\Cyclabilite\VoteBundle\Entity\SegmentDeleted")
     */
    protected $segmentEnd;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @ORM\OneToMany(targetEntity="CL\Cyclabilite\VoteBundle\Entity\VoteIntersectionPathDeleted", mappedBy="intersectionPath")
     * @ORM\OrderBy({"datetime_vote"="DESC", "datetime_creation"="DESC"})
     */
    protected $votes;
}
