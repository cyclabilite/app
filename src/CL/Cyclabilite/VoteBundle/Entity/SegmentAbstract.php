<?php

namespace CL\Cyclabilite\VoteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use CL\GeoBundle\Entity\Linestring;

/** @ORM\MappedSuperclass */
abstract class SegmentAbstract extends VotesInformationAbstract
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned":true})
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     */
    protected $id;

    /**
     * @var WayAbstract
     */
    protected $way;

    /**
     * @var NodeAbstract
     *
     */
    protected $nodeStart;

    /**
     * @var NodeAbstract
     */
    protected $nodeEnd;

    /**
     * @var CL\GeoBundle\Entity\Linestring
     *
     * @ORM\Column(type="linestring", name="geom")
     */
    protected $line;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    protected $votes;

    public function __construct()
    {
        $this->votes = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Get nodeStart
     *
     * @return \CL\Entity\Node
     */
    public function getNodeStart()
    {
        return $this->nodeStart;
    }

    /**
     * Get nodeEnd
     *
     * @return \CL\Entity\Node
     */
    public function getNodeEnd()
    {
        return $this->nodeEnd;
    }

    /**
     * Set way
     *
     * @param \CL\Cyclabilite\VoteBundle\Entity\Way $way
     * @return Segment
     */
    public function setWay(\CL\Cyclabilite\VoteBundle\Entity\Way $way = null)
    {
        $this->way = $way;

        return $this;
    }

    /**
     * Get way
     *
     * @return \CL\Cyclabilite\VoteBundle\Entity\Way
     */
    public function getWay()
    {
        return $this->way;
    }

    /**
     * Set nodeStart
     *
     * @param \CL\Cyclabilite\VoteBundle\Entity\Node $nodeStart
     * @return Segment
     */
    public function setNodeStart(\CL\Cyclabilite\VoteBundle\Entity\Node $nodeStart = null)
    {
        $this->nodeStart = $nodeStart;

        return $this;
    }

    /**
     * Set nodeEnd
     *
     * @param \CL\Cyclabilite\VoteBundle\Entity\Node $nodeEnd
     * @return Segment
     */
    public function setNodeEnd(\CL\Cyclabilite\VoteBundle\Entity\Node $nodeEnd = null)
    {
        $this->nodeEnd = $nodeEnd;

        return $this;
    }

    public function getLine()
    {
        return $this->line;
    }

    public function getVotes()
    {
        return $this->votes;
    }

    /**
     * Returns an associated array with directions as keys and as values
     * array of all the votes with the direction (given in by the key)
     */
    public function getVotesByDirection()
    {
        $votesByDirection = array();

        foreach ($this->getVotes() as $v) {
            if (!array_key_exists($v->getDirection(), $votesByDirection)) {
                $votesByDirection[$v->getDirection()] = array($v);
            } else {
                array_push($votesByDirection[$v->getDirection()], $v);
            }
        }
        return $votesByDirection;
    }

    /**
     * This function is for debugging only: it allows to see a string
     * into form.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getWay()->getName().' between '.$this->getNodeStart()
                ->getId().' and '.$this->getNodeEnd()->getId();
    }
}
