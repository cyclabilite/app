<?php

namespace CL\Cyclabilite\VoteBundle\Entity;

use CL\Cyclabilite\VoteBundle\Entity\VoteAbstract;
use Doctrine\ORM\Mapping as ORM;

/**
 * VoteIntersectionPath
 * @ORM\Entity
 * @ORM\Table(name="deleted_cyclab_intersection_path_vote")
 */
class VoteIntersectionPathDeleted extends VoteIntersectionPathAbstract
{
    /**
     * @var \CL\Cyclabilite\VoteBundle\Entity\IntersectionPathDeleted
     */
    protected $intersectionPath;
}
