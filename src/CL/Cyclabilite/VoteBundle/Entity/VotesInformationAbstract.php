<?php

namespace CL\Cyclabilite\VoteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Gather properties and method about vote Information
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 * @author Marc Ducobu <marc.ducobu@champs-libres.coop>
 */

abstract class VotesInformationAbstract
{
    /**
     * @var int
     * @ORM\Column(type="integer", name="vote_default")
     */
    protected $voteDefault;

    /**
     * @var int
     * @ORM\Column(type="integer", name="vote_number")
     */
    protected $voteNumber = 0;

    /**
     * @var float
     * @ORM\Column(type="float", name="vote_average")
     */
    protected $voteAverage = null;


    /**
     * @var json array Array to store specific information used for the
     * computation of the average.
     * @ORM\Column(type="json_array")
     */
    protected $averageData = [];

    public function getVoteDefault()
    {
        return $this->voteDefault;
    }

    public function getVoteNumber()
    {
        return $this->voteNumber;
    }

    public function setVoteNumber($voteNumber)
    {
        $this->voteNumber = $voteNumber;
        return $this;
    }

    public function getVoteAverage()
    {
        return $this->voteAverage;
    }

    public function setVoteAverage($voteAverage)
    {
        $this->voteAverage = $voteAverage;
        return $this;
    }

    /**
     * Set averageData, an array to store specific data use for the
     * computation of the average.
     *
     * @param array $averageData
     * @return Entity
     */
    public function setAverageData($averageData)
    {
        $this->averageData = $averageData;
        return $this;
    }

    /**
     * Get averageData, an array to store specific data use for the
     * computation of the average.
     *
     * @return array
     */
    public function getAverageData()
    {
        return $this->averageData;
    }

    /**
     * Get an associative array such that :
     * - the keys are an user Id
     * - the values are the array of the votes for the user Id given by the key
     *
     * This function respect the order of the getvotes function so if the
     * getvotes function return votes chronologically each sub-arrays will
     * have the votes in the chronological order.
     *
     * @return The associative array with the users Id as key, and for each user
     * id, the array of the vote of this user.
     */
    public function getVotesByUserId()
    {
        $votesByUserId = array();

        foreach ($this->getVotes() as $v) {
            if (array_key_exists($v->getUser()->getId(), $votesByUserId)) {
                array_push($votesByUserId[$v->getUser()->getId()], $v);
            } else {
                $votesByUserId[$v->getUser()->getId()] = array($v);
            }
        }

        return $votesByUserId;
    }

    /**
     * Give a list of votes, compute an associative array such that :
     * - the keys are an user Id
     * - the values are  an associative array such that
     *       - the keys are an profile id
     *       - the values are an of the votes for the user id given by the key
     *         and the profile id given by the key
     *
     * This function respect the order of the given list of votes.
     *
     * @param Array $votesToSort The list of the votes to sort
     * @return The associative array with the user id as key, and for each user
     * id, the associate array with the profile id as key, and for each profile
     * id the array of the vote of this user and this profile.
     */
    public function sortVotesByUserIdAndProfileId($votesToSort)
    {
        $votesByUserIdAndProfileId = array();

        foreach ($votesToSort as $v) {
            if (! array_key_exists($v->getUser()->getId(), $votesByUserIdAndProfileId)) {
                $votesByUserIdAndProfileId[$v->getUser()->getId()] = array($v->getProfile()->getId() => array($v));
            } else {
                if (! array_key_exists($v->getProfile()->getId(), $votesByUserIdAndProfileId[$v->getUser()->getId()])) {
                    $votesByUserIdAndProfileId[$v->getUser()->getId()][$v->getProfile()->getId()] = array($v);
                } else {
                    array_push($votesByUserIdAndProfileId[$v->getUser()->getId()][$v->getProfile()->getId()], $v);
                }
            }
        }

        return $votesByUserIdAndProfileId;
    }


    /**
     * Get an associative array such that :
     * - the keys are an user Id
     * - the values are  an associative array such that
     *       - the keys are an profile id
     *       - the values are an of the votes for the user id given by the key
     *         and the profile id given by the key
     *
     * This function respect the order of the getvotes function so if the
     * getvotes function return votes chronologically each sub-arrays will
     * have the votes in the chronological order.
     *
     * @return The associative array with the user id as key, and for each user
     * id, the associate array with the profile id as key, and for each profile
     * id the array of the vote of this user and this profile.
     */
    public function getVotesByUserIdAndProfileId()
    {
        return $this->sortVotesByUserIdAndProfileId($this->getVotes());
    }
}
