<?php

namespace CL\Cyclabilite\VoteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use CL\GeoBundle\Entity\Linestring;

/**
 * SegmentDeleted
 *
 * @ORM\Entity(repositoryClass="CL\Cyclabilite\VoteBundle\Repository\SegmentDeletedRepository")
 * @ORM\Table(name="deleted_cyclab_segments")
 * @ORM\HasLifecycleCallbacks
 */
class SegmentDeleted extends SegmentAbstract
{
    /**
     * Question / TODO est-ce le bon endroit pour faire de l'héritage ?
     * @ORM\ManyToOne(targetEntity="WayAll")
     */
    protected $way;


    /**
     * @var NodeAll
     * @ORM\ManyToOne(targetEntity="NodeAll")
     *
     */
    protected $nodeStart;

    /**
     * @var NodeAll
     * @ORM\ManyToOne(targetEntity="NodeAll")
     */
    protected $nodeEnd;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     * @ORM\OneToMany(targetEntity="VoteSegmentDeleted", mappedBy="segment")
     */
    protected $votes;


    /**
     * @var bool inFrigo
     * @ORM\Column(type="boolean", options={"default" : FALSE})
     *
     * When the user do not want to report the vote now (and put the segment
     * in 'refrigerator' to be treated after
     */
    protected $inFrigo = False;

    /**
     * Get in Frigo (refrigerator)
     *
     * @return boolean
     */
    public function getInFrigo()
    {
        return $this->inFrigo;
    }

    /*
     * Set in Frigo (refrigerator)
    */
    public function setInFrigo($inFrigo)
    {
       $this->inFrigo = $inFrigo;
       return $this;
    }
}
