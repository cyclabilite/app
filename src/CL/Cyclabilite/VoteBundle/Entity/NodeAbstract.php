<?php

namespace CL\Cyclabilite\VoteBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/** @ORM\MappedSuperclass */
abstract class NodeAbstract extends VotesInformationAbstract
{
    /**
    * @var integer
    * @ORM\Id
    * @ORM\Column(type="bigint")
     */
    protected $id;

    /**
     * @var point
     * @ORM\Column(type="point", name="geom")
     */
    protected $point;

    /**
     * @var IntersectionPathAbstract The intersection path that are associated to this
     * Intersection
     */
    protected $intersectionPaths;

    /**
     * @var SegmentAbstract The segment that starts on this intersection.
     */
    protected $startingSegments;

    /**
     * @var SegmentAbstract The segments that ends on this intersection.
     */
    protected $endingSegments;

    public function __construct()
    {
        $this->intersectionPaths = new ArrayCollection();
        $this->startingSegments = new ArrayCollection();
        $this->endingSegments = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set point
     *
     * @param point $point
     * @return Node
     */
    public function setPoint($point)
    {
        $this->point = $point;
        return $this;
    }

    /**
     * Get point
     *
     * @return point
     */
    public function getPoint()
    {
        return $this->point;
    }


    /**
     * Get the segments starting / stopping a this points
     */
    public function getSegments()
    {
        $segments = new ArrayCollection();

        foreach ($this->startingSegments as $s) {
            $segments->add($s);
        }

        foreach ($this->endingSegments as $s) {
            $segments->add($s);
        }

        return $segments;
    }

    /**
     * Get the votes that has been associated to this intersection
     */
    public function getVotes()
    {
        $votesToReturn = new ArrayCollection();

        foreach ($this->intersectionPaths as $ip) {
            foreach ($ip->getVotes() as $v) {
                $votesToReturn->add($v);
            }
        }

        return $votesToReturn;
    }


    /**
     * Get the array collection of the intersection paths encode of the node
     * (an intersection path exists only if a vote exists on it)
     */
    public function getIntersectionPaths()
    {
        return $this->intersectionPaths;
    }
}
