<?php

namespace CL\Cyclabilite\VoteBundle\Entity;

use Symfony\Component\Validator\ExecutionContextInterface;
use Doctrine\ORM\Mapping as ORM;

/** @ORM\MappedSuperclass */
abstract class IntersectionPathAbstract extends VotesInformationAbstract
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned":true})
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     */
    protected $id;

    /**
     * @var NodeAbstract
     */
    protected $node;

    /**
     * @var SegmentAbstract
     */
    protected $segmentStart;

    /**
     * @var SegmentAbstract
     */
    protected $segmentEnd;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    protected $votes;

    public function __construct()
    {
       $this->votes = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set node
     *
     * @param \CL\Cyclabilite\VoteBundle\Entity\Node $node
     * @return IntersectionPath
     */
    public function setNode(\CL\Cyclabilite\VoteBundle\Entity\Node $node = null)
    {
        $this->node = $node;

        return $this;
    }

    /**
     * Get node
     *
     * @return \CL\Cyclabilite\VoteBundle\Entity\Node
     */
    public function getNode()
    {
        return $this->node;
    }

    /**
     * Set segmentStart
     *
     * @param \CL\Cyclabilite\VoteBundle\Entity\Segment $segmentStart
     * @return IntersectionPath
     */
    public function setSegmentStart(\CL\Cyclabilite\VoteBundle\Entity\Segment $segmentStart = null)
    {
        $this->segmentStart = $segmentStart;

        return $this;
    }

    /**
     * Get segmentStart
     *
     * @return \CL\Cyclabilite\VoteBundle\Entity\Segment
     */
    public function getSegmentStart()
    {
        return $this->segmentStart;
    }

    /**
     * Set segmentEnd
     *
     * @param \CL\Cyclabilite\VoteBundle\Entity\Segment $segmentEnd
     * @return IntersectionPath
     */
    public function setSegmentEnd(\CL\Cyclabilite\VoteBundle\Entity\Segment $segmentEnd = null)
    {
        $this->segmentEnd = $segmentEnd;

        return $this;
    }

    /**
     * Get segmentEnd
     *
     * @return \CL\Cyclabilite\VoteBundle\Entity\Segment
     */
    public function getSegmentEnd()
    {
        return $this->segmentEnd;
    }

    /**
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVotes()
    {
       return $this->votes;
    }

    /**
     * Function used for validation. Check wether the node is common to
     * both segments (if the segments are not null (has been chosen))
     */
    public function isSegmentAndNodesContigous(ExecutionContextInterface $context)
    {
        if ($this->getSegmentEnd() && $this->getSegmentStart()) {
            if($this->getSegmentEnd()->getId() === $this->getSegmentStart()->getId()) {
                $context->addViolationAt('segmentStart', 'intersection_path.'
                    . 'segments.segments_are_identical', array(
                       '%start%' => $this->getSegmentStart()->getId(),
                       '%end%' => $this->getSegmentEnd()->getId()
                    ));
                $context->addViolationAt('segmentEnd', 'intersection_path.'
                    . 'segments.segments_are_identical', array(
                       '%start%' => $this->getSegmentStart()->getId(),
                       '%end%' => $this->getSegmentEnd()->getId()
                    ));
                return;
            }
        }

        $nodeId = $this->getNode()->getId();

        if($this->getSegmentStart()
            && $nodeId !=  $this->getSegmentStart()->getNodeStart()->getId()
            && $nodeId !=  $this->getSegmentStart()->getNodeEnd()->getId()) {
                $context->addViolationAt('node',
                    'The node %node% is not in the segment %segment%.',
                    array(
                        '%segment%' => $this->getSegmentStart()->getId(),
                        '%node%' => $nodeId
                    )
                );
        }

        if($this->getSegmentEnd()
            && $nodeId !=  $this->getSegmentEnd()->getNodeStart()->getId()
            && $nodeId !=  $this->getSegmentEnd()->getNodeEnd()->getId()) {
                $context->addViolationAt('node',
                    'The node %node% is not in the segment %segment%.',
                    array(
                        '%segment%' => $this->getSegmentEnd()->getId(),
                        '%node%' => $nodeId
                    )
                );
        }
    }
}
