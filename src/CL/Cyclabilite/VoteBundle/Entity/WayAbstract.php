<?php

namespace CL\Cyclabilite\VoteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Way
 * @ORM\MappedSuperclass
 */
abstract class WayAbstract
{
   /**
    * @var integer
    * @ORM\Id
    * @ORM\Column(type="bigint", options={"unsigned":true})
    */
   private $id;

   /**
    * @var boolean
    */
   private $visible;

   /**
    * @var string
    * @ORM\Column(type="string", nullable=true)
    */
   private $name;

   /**
    * @var string
    * @ORM\Column(type="string")
    */
   private $highway;

   /**
    * @var linestring
    * @ORM\Column(type="linestring", name="linestring")
    */
   private $line;

   /**
    * @var \Doctrine\Common\Collections\Collection of SegmentAbstract
    */
   // private $segments;

   /**
    * @var \Doctrine\Common\Collections\Collection
    */
   // private $points;

   public function __construct()
   {
      $this->points = new \Doctrine\Common\Collections\ArrayCollection();
      $this->segments =  new \Doctrine\Common\Collections\ArrayCollection();
   }

   /**
    * Get id
    *
    * @return integer
    */
   public function getId()
   {
      return $this->id;
   }

   /**
    * Set idOsm
    *
    * @param integer $idOsm
    * @return Way
    */
   public function setIdOsm($idOsm)
   {
      $this->idOsm = $idOsm;

      return $this;
   }

   /**
    * Get idOsm
    *
    * @return integer
    */
   public function getIdOsm()
   {
      return $this->idOsm;
   }

   /**
    * Set visible
    *
    * @param boolean $visible
    * @return Way
    */
   public function setVisible($visible)
   {
      $this->visible = $visible;

      return $this;
   }

   /**
    * Get visible
    *
    * @return boolean
    */
   public function getVisible()
   {
      return $this->visible;
   }

   /**
    * Set name
    *
    * @param string $name
    * @return Way
    */
   public function setName($name)
   {
      $this->name = $name;

      return $this;
   }

   /**
    * Get name
    *
    * @return string
    */
   public function getName()
   {
      return $this->name;
   }

   /**
    * Set highway
    *
    * @param string $highway
    * @return Way
    */
   public function setHighway($highway)
   {
      $this->highway = $highway;

      return $this;
   }

   /**
    * Get highway
    *
    * @return string
    */
   public function getHighway()
   {
      return $this->highway;
   }

   /**
    * Set line
    *
    * @param  $line
    * @return Way
    */
   public function setLine($line)
   {
      $this->line = $line;

      return $this;
   }

   /**
    * Get line
    *
    * @return \CL\GeoBundle\Entity\Linestring
    */
   public function getLine()
   {
      return $this->line;
   }

   public function getSegments()
   {
      return $this->segments;
   }


   public function addSegment(Segment $segment): self
   {
      if (!$this->segments->contains($segment)) {
           $this->segments[] = $segment;
           $segment->setWay($this);
      }
      return $this;
   }

   public function removeSegment(Segment $segment): self
   {
      if ($this->segments->contains($segment)) {
           $this->segments->removeElement($segment);
           // set the owning side to null (unless already changed)
           if ($segment->getWay() === $this) {
               $segment->setWay(null);
           }
      }

      return $this;
   }



   /**
    * Add points
    *
    * @param \CL\Cyclabilite\VoteBundle\Entity\Node $points
    * @return Way
    */
   public function addPoint(\CL\Cyclabilite\VoteBundle\Entity\Node $points)
   {
      $this->points[] = $points;

      return $this;
   }

   /**
    * Remove points
    *
    * @param \CL\Cyclabilite\VoteBundle\Entity\Node $points
    */
   public function removePoint(\CL\Cyclabilite\VoteBundle\Entity\Node $points)
   {
      $this->points->removeElement($points);
   }

   /**
    * Get points
    *
    * @return \Doctrine\Common\Collections\Collection
    */
   public function getPoints()
   {
      return $this->points;
   }
}
