<?php

namespace CL\Cyclabilite\VoteBundle\Entity;

use CL\Cyclabilite\VoteBundle\Entity\VoteAbstract;
use Doctrine\ORM\Mapping as ORM;

/**
 * VoteIntersectionPath
 * @ORM\Entity
 * @ORM\Table(name="cyclab_intersection_path_vote")
 */
class VoteIntersectionPath extends VoteIntersectionPathAbstract
{
    /**
     * @var \CL\Cyclabilite\VoteBundle\Entity\IntersectionPath
     * @ORM\ManyToOne(targetEntity="IntersectionPath", inversedBy="votes")
     * @ORM\JoinColumn(name="intersection_path_id", referencedColumnName="id")
     */
    protected $intersectionPath;
}
