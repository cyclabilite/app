<?php

namespace CL\Cyclabilite\VoteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * This class contains methods and parameters common to all kinds of votes
 * @ORM\MappedSuperclass
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 * @author Marc Ducobu <marc.ducobu@champs-libres.coop>
 */
abstract class VoteAbstract
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="cyclab_votes_id_seq", allocationSize=1)
     */
    protected $id;

    /**
     * @var integer
     * @ORM\Column(type="integer", name="vote_value")
     */
    protected $value;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", name="datetime_vote")
     */
    protected $datetime_vote;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", name="datetime_creation")
     */
    protected $datetime_creation;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $client;

    /**
     * TODO : DELETE
     * @var \CL\Cyclabilite\UserBundle\Entity\User
     * @ORM\ManyToOne(targetEntity="CL\Cyclabilite\UserBundle\Entity\User")
     */
    protected $user;

    /**
     * @var \CL\Cyclabilite\UserBundle\Entity\Profile
     * @ORM\ManyToOne(targetEntity="CL\Cyclabilite\UserBundle\Entity\Profile")
     */
    protected $profile;

    public function __construct()
    {
        $now = new \DateTime();
        $this->datetime_creation = $now;
        $this->datetime_vote = $now;
    }

    /**
     * Copy some data from the object ($this) to another VoteAbstre ($copy)
     *
     * The copied data are : value, datetimeVote, datetimeCreation,
     * client, user, profile.
     */
    public function copyDataTo($copy)
    {
        $copy->setValue($this->getValue());
        $copy->setDatetimeVote($this->getDatetimeVote());
        $copy->setDatetimeCreation($this->getDatetimeCreation());
        $copy->setClient($this->getClient());
        $copy->setUser($this->getUser());
        $copy->setProfile($this->getProfile());
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value to the vote.
     *
     * This method will update the pre-computed values for the associatedEntity
     *
     * Set a value to th
     * This method update some pre-computed values
     *
     * @param integer $value
     * @return VoteSegment
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * Get value
     *
     * @return integer
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set datetime_vote
     *
     * @param \timestamp $datetimeVote
     * @return VoteSegment
     */
    public function setDatetimeVote(\DateTime $datetimeVote)
    {
        $this->datetime_vote = $datetimeVote;

        return $this;
    }

    /**
     * Get datetime_vote
     *
     * @return \timestamp
     */
    public function getDatetimeVote()
    {
        return $this->datetime_vote;
    }

    /**
     * Set datetime_creation
     *
     * @param \timestamp $datetimeCreation
     * @return VoteSegment
     */
    public function setDatetimeCreation(\DateTime $datetimeCreation)
    {
        $this->datetime_creation = $datetimeCreation;

        return $this;
    }

    /**
     * Get datetime_creation
     *
     * @return \timestamp
     */
    public function getDatetimeCreation()
    {
        return $this->datetime_creation;
    }

    /**
     * Set client
     *
     * @param string $client
     * @return VoteSegment
     */
    public function setClient($client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return string
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set user
     *
     * @param \CL\Cyclabilite\UserBundle\Entity\User $user
     * @return VoteSegment
     */
    public function setUser(\CL\Cyclabilite\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \CL\Cyclabilite\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set voteProfile
     *
     * @param \CL\Cyclabilite\UserBundle\Entity\Profile
     * @return VoteSegment
     */
    public function setProfile(\CL\Cyclabilite\UserBundle\Entity\Profile $profile = null)
    {
        $this->profile = $profile;

        return $this;
    }

    /**
     * Get voteProfile
     *
     * @return \CL\Cyclabilite\UserBundle\Entity\Profile
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * Get the attribute $is_expert_vote
     *
     * @return Boolean
     */
    public function isExpertVote()
    {
        return $this->getUser()->isExpert();
    }

    /**
     * @return string the type of the vote (segment|...)
     */
    // abstract public function getType();

    //abstract public function getAssociatedEntity();
}
