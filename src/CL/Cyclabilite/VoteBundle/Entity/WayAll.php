<?php

namespace CL\Cyclabilite\VoteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Way
 * @ORM\Entity
 * @ORM\Table(name="ways_all")
 */
class WayAll extends WayAbstract
{
    /**
     * @var \Doctrine\Common\Collections\Collection of SegmentAbstract
     * @ORM\OneToMany(targetEntity="CL\Cyclabilite\VoteBundle\Entity\SegmentDeleted", mappedBy="way")
     */
    protected $segments;

    /**
     * // @var \Doctrine\Common\Collections\Collection
     * // @ORM\ManyToMany(targetEntity="CL\Cyclabilite\VoteBundle\Entity\NodeAll")
     * // @ORM\JoinTable(name="way_nodes",
     *      joinColumns={@ORM\JoinColumn(name="way_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="node_id", referencedColumnName="id")}
     * )
     */
    //private $points;
}
