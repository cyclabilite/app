<?php

namespace CL\Cyclabilite\VoteBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Deleted or not nodes.
 *
 * Such of theses nodes may be present in a SegementDeleted : for a deleted
 * segement, the node may be deleted or reused in a new segment.
 *
 * @ORM\Entity
 * @ORM\Table(name="nodes_all")
 */
class NodeAll extends NodeAbstract
{
    /**
     * @var IntersectionPathDeleted The intersection path that are associated to this
     * Intersection
     *
     * TODO verifier si creer a la volée (normalement oui) -> decire
     * @ORM\OneToMany(targetEntity="IntersectionPathDeleted", mappedBy="node")
     */
    protected $intersectionPaths;


    /**
     * @var SegmentDeleted  The segment that starts on this intersection.
     * @ORM\OneToMany(targetEntity="SegmentDeleted", mappedBy="nodeStart")
     */
    protected $startingSegments;

    /**
     * @var SegmentDeleted The segments that ends on this intersection.
     * @ORM\OneToMany(targetEntity="SegmentDeleted", mappedBy="nodeEnd")
     */
    protected $endingSegments;
}
