<?php

namespace CL\Cyclabilite\VoteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Vote on a segment
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 * @version 1.0.2 directionForward is now replaced by direction
 *
 *
 * @ORM\Entity
 * @ORM\Table(name="cyclab_segment_vote")
 * @UniqueEntity({"user", "profile", "datetime_vote", "segment"})
 *
 */
class VoteSegment extends VoteSegmentAbstract
{
    /**
     * @var \CL\Cyclabilite\VoteBundle\Entity\Segment
     * @ORM\ManyToOne(targetEntity="CL\Cyclabilite\VoteBundle\Entity\Segment", inversedBy="votes")
     */
    protected $segment;
}
