<?php

namespace CL\Cyclabilite\VoteBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Intersection
 * @ORM\Entity
 * @ORM\Table(name="cyclab_intersections")
 */
class Intersection extends NodeAbstract
{
    /**
     * @var IntersectionPath The intersection path that are associated to this
     * Intersection
     *
     * TODO verifier si creer a la volée (normalement oui) -> decire
     * @ORM\OneToMany(targetEntity="IntersectionPath", mappedBy="node")
     */
    protected $intersectionPaths;


    /**
     * @var Segment  The segment that starts on this intersection.
     * @ORM\OneToMany(targetEntity="Segment", mappedBy="nodeStart")
     */
    protected $startingSegments;

    /**
     * @var Segment The segments that ends on this intersection.
     * @ORM\OneToMany(targetEntity="Segment", mappedBy="nodeEnd")
     */
    protected $endingSegments;
}
