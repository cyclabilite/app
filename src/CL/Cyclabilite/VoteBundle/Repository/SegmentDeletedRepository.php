<?php

namespace CL\Cyclabilite\VoteBundle\Repository;

use Doctrine\ORM\EntityRepository;

use CL\Cyclabilite\VoteBundle\Entity\SegmentDeleted;


/**
 * Repository for segmentDeleted
 *
 */
class SegmentDeletedRepository extends EntityRepository
{
    public function findWithVotes()
    {
        $selectIdsQB = $this
            ->createQueryBuilder('s')
            ->select("s.id")
            ->innerJoin('s.votes', 'v')
            ->where('s.inFrigo=False')
            ->groupBy('s.id')
            ->having('COUNT(s.id) > 0')
            ->setMaxResults(100);

        $selectIdsResult = $selectIdsQB->getQuery()->getResult();

        $getId = function($row) {
            return $row['id'];
        };

        $selectedIds = array_map($getId, $selectIdsResult);

        $qb = $this->createQueryBuilder('s');
        $qb->where($qb->expr()->in('s.id', $selectedIds));
        return $qb->getQuery()->getResult();
    }
}
