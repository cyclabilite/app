<?php

namespace CL\Cyclabilite\VoteBundle\Repository;

use Doctrine\ORM\EntityRepository;


/**
 * Repository of SegmentDeleted
 */
class SegmentRepository extends EntityRepository
{
    /**
     * Exec a list of queries. Stop if the return of the function execute is
     * equivalent to False. Return the last return of the function execute.
     */
    public function execQueries($queries)
    {
        $em = $this->getEntityManager();
        $conn = $em->getConnection();

        foreach ($queries as $query) {
            $statement = $conn->prepare($query);
            $ok = $statement->execute();
            if(! $ok) {
                return $ok; // return False and stop
            }
        }

        return $ok;
    }

    /**
     * Check if a list of table exists in the DB
     * 
     * $tableNames The list of the tables name
     */
    public function checkIfTableExists($tableNames)
    {
        $sql = "SELECT COUNT(*)
            FROM information_schema.tables 
            WHERE table_name = any(ARRAY['".implode("','", $tableNames)."'])";

        $em = $this->getEntityManager();
        $conn = $em->getConnection();
        $statement = $conn->prepare($sql);
        $statement->execute();
        $result = $statement->fetchAll();

        return intval($result[0]["count"]) == sizeof($tableNames);
    }

    /**
     * Drop trable from the DB
     * 
     * $tableNames the list of the tables name
     */
    public function dropTables($tableNames)
    {
        $queries = [];

        foreach ($tableNames as $tableName) {
            $queries[] = 'DROP TABLE IF EXISTS '.$tableName.';';
        }

        $ok = $this->execQueries($queries);
        return $ok; // True if all gets well
    }

    /**
     * Generate the table cyclab_segments_l93 used for Trivial Segment Vote Transfer (without index).
     * 
     * bufferSize4current is given in meters
     */
    public function generateCyclabSegmentsL93Table($bufferSize4current=3)
    {
        if($bufferSize4current) {
            $buffRequest4current = ', ST_Buffer(ST_Transform(s.geom, 2154), '.$bufferSize4current.') AS buff_l93 ';
        } else {
            $buffRequest4current = '';
        }

        $queries = [
            'DROP TABLE IF EXISTS cyclab_segments_l93;',
            'CREATE TABLE cyclab_segments_l93 AS (
                SELECT
                    s.*, ST_Transform(s.geom, 2154) as geom_l93
                    '.$buffRequest4current.'
                    FROM  cyclab_segments as s);',
        ];

        $ok = $this->execQueries($queries);
        return $ok; // True if all gets well
    }


    /**
     * Generate the index for table cyclab_segments_l93 used for Trivial Segment Vote Transfer.
     * 
     * bufferSize4current is given in meters
     */
    public function generateCyclabSegmentsL93Index($bufferSize4current=3)
    {
        $queries = [
            'CREATE INDEX ON "cyclab_segments_l93" USING GIST("geom_l93");',
            'CREATE INDEX ON "cyclab_segments_l93" ("id");',
        ];

        if($bufferSize4current) {
            $queries[] = 'CREATE INDEX ON "cyclab_segments_l93" USING GIST("buff_l93");';
        }

        $ok = $this->execQueries($queries);
        return $ok; // True if all gets well
    }


    /**
     * Generate the table _l93 used for Trivial Segment Vote Transfer
     * 
     * bufferSize4deleted is given in meter
     * 
     * To recompute before getTrivialTransfer
     *
     */
    private function generateDeletedCyclabSegmentsL93Table($bufferSize4deleted=3)
    {
        if($bufferSize4deleted) {
            $buffRequest4deleted = ', ST_Buffer(ST_Transform(s.geom, 2154), '.$bufferSize4deleted.') AS buff_l93 ';
        } else {
            $buffRequest4deleted = '';
        }

        $queries = [
            'DROP TABLE IF EXISTS deleted_cyclab_segments_l93;',
            'CREATE TABLE deleted_cyclab_segments_l93 AS (
                SELECT
                    s.id AS id,
                    ST_Transform(s.geom, 2154) as geom_l93
                    '.$buffRequest4deleted.'
                    , count(*) AS vote_nbr
                FROM deleted_cyclab_segments as s
                JOIN deleted_cyclab_segment_vote as v ON v.segment_id = s.id
                GROUP BY s.id, s.geom
                HAVING COUNT(*) > 0);',
            'CREATE INDEX ON "deleted_cyclab_segments_l93" USING  GIST("geom_l93");',
            'CREATE INDEX ON "deleted_cyclab_segments_l93" ("id");',
        ];
            
        if($bufferSize4deleted) {
            $queries[] = 'CREATE INDEX ON "deleted_cyclab_segments_l93" USING GIST("buff_l93");';
        }

        $ok = $this->execQueries($queries);
        return $ok;
    }

    /** 
     * Generate the table trivial_deleted_seg_for_transfer
     * 
     * To execute before getTrivialTransfer
     */
    private function generateTrivialDeletedSegmentTable($bufferSize4deleted=3, $bufferSize4current=3)
    {
        $queries = [
            'DROP TABLE IF EXISTS trivial_deleted_seg_for_transfer;',
        ];

        if($bufferSize4current && $bufferSize4deleted){ #buffer pour del et current
            $queries[] =
                'CREATE TABLE trivial_deleted_seg_for_transfer AS (
                    SELECT del_s.id del_seg_id, COUNT(*)
                    FROM cyclab_segments_l93 as s
                    JOIN deleted_cyclab_segments_l93 as del_s ON ST_Covers(del_s.buff_l93, s.geom_l93)
                    GROUP BY del_s.id
                    HAVING COUNT(*) = 1);';
        }

        $ok = $this->execQueries($queries);
        return $ok;
    }
 

    /**
     * @param float $bufferSize4deleted The size of the buffer for deleted (in meters - or null)
     * @param float $bufferSize4current The size of the buffer for new seg (in meters - o rnull)
     * @param int $limit The numbeer of trivial transfer to get (optional)
     * @return Array[int, int] : The array for the transfer : each row contains
     * the id of the deleted segment and the id of the new segment that is
     * associated to it.
     */
    public function getTrivialTransfer($bufferSize4deleted=3, $bufferSize4current=3,  $limit=null)
    {
        $ok = $this->generateDeletedCyclabSegmentsL93Table($bufferSize4deleted);
        if(!$ok) {
            return $ok;
        }
    
        $ok = $this->generateTrivialDeletedSegmentTable($bufferSize4deleted, $bufferSize4current);
        if(!$ok) {
            return $ok;
        }

        $mustHaveTables = [ // tables that we need for the transfer
            'cyclab_segments_l93',
            'deleted_cyclab_segments_l93']; 

        if($bufferSize4current && $bufferSize4deleted){
            $mustHaveTables[] = 'trivial_deleted_seg_for_transfer';
        }

        if($this->checkIfTableExists($mustHaveTables)) {
            if(! $limit) {
                $limit = 'null';
            }
        
            if($bufferSize4current && $bufferSize4deleted) { # if buffer pour del et current
                $query = 'SELECT trsf.del_seg_id as del_seg_id, new_seg.id as new_seg_id
                            FROM trivial_deleted_seg_for_transfer as trsf
                            JOIN deleted_cyclab_segments_l93 as del_seg
                                ON trsf.del_seg_id = del_seg.id
                            JOIN cyclab_segments_l93 as new_seg
                                ON ST_Covers(del_seg.buff_l93, new_seg.geom_l93)
                                AND ST_Covers(new_seg.buff_l93, del_seg.geom_l93)
                            LIMIT '.$limit.';';
            } else {
                if($bufferSize4current) { # if only buffer pour current
                    $query = 'SELECT del_seg.id as del_seg_id, new_seg.id as new_seg_id
                                FROM deleted_cyclab_segments_l93 as del_seg
                                JOIN cyclab_segments_l93 as new_seg ON ST_Covers(new_seg.buff_l93, del_seg.geom_l93)
                                LIMIT '.$limit.';';
    
                } else { # else only buffer pour del
                    $query = 'SELECT del_seg.id as del_seg_id, new_seg.id as new_seg_id
                                FROM deleted_cyclab_segments_l93 as del_seg
                                JOIN cyclab_segments_l93 as new_seg ON ST_Covers(del_seg.buff_l93, new_seg.geom_l93)
                                LIMIT '.$limit.';';
                }
            }
    
            $em = $this->getEntityManager();
            $conn = $em->getConnection();
            $statement = $conn->prepare($query);
            $statement->execute();
            
            $result = $statement->fetchAll();
    
            return $result;
        } else {
            return False;
            // TODO ajouter un log (pour debug)
        }
    }
}