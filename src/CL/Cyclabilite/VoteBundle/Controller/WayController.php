<?php

namespace CL\Cyclabilite\VoteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use CL\GeoBundle\Entity\BBox;

/**
 * Way controller.
 *
 */
class WayController extends Controller
{
    /**
     * Lists all Ways being in the db (limit 100).
     *
     */
    public function listAllAction($_format)
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('CLCyclabiliteVoteBundle:Way')->findBy(array(), null, 100);

        switch ($_format) {
           case 'html' : 
              return $this->render('CLCyclabiliteVoteBundle:Way:listAll.html.twig', array(
               'entities' => $entities,
               ));
              break;
           case 'json':
              return $this->returnJson($entities);
        }
        
    }

    /**
     * List ALL the ways being in a bbox
     * 
     * @param type $_format (json or html)
     * @param type $bbox (the bbox)
     */
    public function listBBOXAction($_format, Request $request)
    {
        try {
            $bbox = BBox::getBBoxFromRequest($request);
        } catch (Exception $ex) {
            $r = new Response($ex->getMessage());
            $r->setStatusCode(Response::HTTP_BAD_REQUEST);
            return $r;
        }

        $repository = $this->getDoctrine()
            ->getRepository('CLCyclabiliteVoteBundle:Way');

        $query = $repository->createQueryBuilder('w')
            ->where('intersects(:bbox, w.line) = TRUE')
            ->setParameter('bbox', $bbox)
            ->getQuery();
    

        $ways = $query->getResult();

        switch($_format) {
            case 'html' :
                return $this->render('CLCyclabiliteVoteBundle:Way:listAll.html.twig', array(
                    'entities' => $ways,
                ));
                break;
            case 'json' :
                return $this->returnJson($ways);
        }       
    }    

    private function returnJson($results)
    {
        return new Response(
                $this->get('serializer')->serialize($results, 'json')
            );
    }
}

?>