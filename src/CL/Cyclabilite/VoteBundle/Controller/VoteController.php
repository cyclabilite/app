<?php

namespace CL\Cyclabilite\VoteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use CL\Cyclabilite\VoteBundle\Entity\VoteSegment;
use CL\Cyclabilite\VoteBundle\Entity\VoteIntersectionPath;
use CL\Cyclabilite\VoteBundle\Form\VoteSegmentType;
use CL\Cyclabilite\VoteBundle\Form\VoteIntersectionPathType;
use CL\Cyclabilite\VoteBundle\Entity\IntersectionPath;

class VoteController extends Controller
{
    public function listVotesAction($type, $id, $_format, Request $request)
    {
        $limit = $request->query->getInt('limit', 10);
        $offset = $request->query->getInt('offset', 0);

        if ($limit > 100) {
            $response = new Response(
                $this->get('translator')->trans('c.vote.list_votes'
                    . '.max_result', array('%max%' => '100'))
            );
            $response->setStatusCode(Response::HTTP_BAD_REQUEST);
            return $response;
        }

        $em = $this->getDoctrine()->getManager();

        if ($type == 'segment') {
            $segment = $em->getRepository('CLCyclabiliteVoteBundle:Segment')
                ->find($id);

            if ($segment === NULL) {
                $response = new Response(
                    $this->get('translator')->trans('c.vote.list_votes.'
                        . 'segment_not_found', array('%id%' => $id))
                );
                $response->setStatusCode(Response::HTTP_NOT_FOUND);
                return $response;
            }

            $votes = $em->createQuery('SELECT v '
                    . 'FROM CLCyclabiliteVoteBundle:VoteSegment v '
                    . 'WHERE v.segment = :segment')
                ->setParameter('segment', $segment)
                ->setFirstResult($offset)
                ->setMaxResults($limit)
                ->getResult();
        } elseif ($type == 'instersection_path') {
            $intersectionPath = $em
                ->getRepository('CLCyclabiliteVoteBundle:IntersectionPath')
                ->find($id);

            if ($intersectionPath === NULL) {
                $response = new Response(
                    $this->get('translator')->trans('c.vote.list_votes.'
                        . 'intersection_path_not_found', array('%id%' => $id))
                );
                $response->setStatusCode(Response::HTTP_NOT_FOUND);
                return $response;
            }

            $votes = $em->createQuery('SELECT v '
                    . 'FROM CLCyclabiliteVoteBundle:VoteIntersectionPath v '
                    . 'WHERE v.intersectionPath = :intersectionPath')
                ->setParameter('intersectionPath', $intersectionPath)
                ->setFirstResult($offset)
                ->setMaxResults($limit)
                ->getResult();
        } elseif ($type == 'node') {
            $node = $em->getRepository('CLCyclabiliteVoteBundle:Node')
                ->find($id);

            if ($node === NULL) {
                $response = new Response(
                    $this->get('translator')->trans('c.vote.list_votes.'
                        . 'node_not_found', array('%id%' => $id))
                );
                $response->setStatusCode(Response::HTTP_NOT_FOUND);
                return $response;
            }

            $intersectionPaths = $em
                ->getRepository('CLCyclabiliteVoteBundle:IntersectionPath')
                ->findBy(array('node' => $node));

            $votes = $em
                ->getRepository('CLCyclabiliteVoteBundle:VoteIntersectionPath')
                ->findBy(array('intersectionPath' => $intersectionPaths));
        } else {
            $response = new Response(
                $this->get('translator')->trans('c.vote.list_votes.'
                    . 'type_not_exist', array('%type%' => $type))
            );
            $response->setStatusCode(Response::HTTP_BAD_REQUEST);
            return $response;
        }

        //for json answer (currently only implementation)
        $serializer = $this->get('serializer');

        $responseJson = $serializer->serialize($votes, $_format, array());
        $response = new Response($responseJson);
        return $response;
    }

    public function showFormAction($type, $id)
    {
        //check for connection
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $response = new Response($this->get('translator')
                    ->trans('global.please_log_in')
            );
            $response->setStatusCode(Response::HTTP_FORBIDDEN);
            return $response;
        }

        $user = $this->get('security.token_storage')->getToken()->getUser();

        if ($type == 'segment') {
            $vote = new VoteSegment();
            $formType = VoteSegmentType::class;
        } elseif ($type == 'intersection') { // unifier avec voteAction
            $intersection = $this->getDoctrine()->getManager()
                ->getRepository('CLCyclabiliteVoteBundle:Node')
                ->find($id);

            if ($intersection === NULL) {
                throw $this->createNotFoundException('node not found in database');
            }

            $vote = new VoteIntersectionPath();
            $form = $this->createForm(VoteIntersectionPathType::class, $vote);
            $formType = VoteIntersectionPathType::class;
        } else {
            throw $this->createNotFoundException("The type $type does not "
                . "exists for a vote");
        }

        //the way we create formType is different from node to segment
        if ($type == 'segment') {
            $form = $this->createForm($formType, $vote, array(
                'action' => $this->generateUrl(
                    'cl_cyclabilite_vote.vote.add',
                    array('type' => $type, '_format' => 'json', 'id' => $id)),
                'csrf_protection' => false)
            );
        } elseif ($type == 'intersection') {
            $form = $this->createForm($formType, $vote, array(
                'action' => $this->generateUrl(
                    'cl_cyclabilite_vote.vote.add',
                    array('type' => $type, '_format' => 'json', 'id' => $id)),
                'csrf_protection' => false
            ));
        }

        return $this->render(
            'CLCyclabiliteVoteBundle:Vote:form.html.twig',
            array('form' => $form->createView())
        );
    }

    public function addAction($type, $_format, $id, Request $request)
    {
        // RMQ_GROS_BORDEL
        // Attention au nom du formulaire pour intersection :
        // le formulaire porte sur un intersectionPath du coup le nom du
        // formulaire html généré par Symfony est vote_intersection_path
        // et non vote_intersection
        $entity = null;

        //check for connection
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            $response = new Response($this->get('translator')
                    ->trans('c.vote.add.please_register')
            );
            $response->setStatusCode(Response::HTTP_FORBIDDEN);
            return $response;
        }

        // used in the VoteXXXType constructor
        $user = $this->get('security.token_storage')->getToken()->getUser();

        //check  the type is correct
        if ($type == 'segment') {
            $vote = new VoteSegment();

            $entity = $this->getDoctrine()->getManager()
                ->getRepository('CLCyclabiliteVoteBundle:Segment')
                ->find($id);

            if (!$entity) {
                throw $this->createNotFoundException("Segment with id "
                    . "$id does not exists");
            }

            $vote->setSegment($entity);
            $formType = VoteSegmentType::class;
        } elseif ($type == 'intersection') { // We are in an intersection path
            # récupération de l'intersection
            $entity = $this->getDoctrine()
                ->getRepository('CLCyclabiliteVoteBundle:Node')
                ->find($id);

            # récupération du segment start Id
            # voir RMQ_GROS_BORDEL pour vote_intersection_path et non vote_intersection
            $segmentStartId = $request->request->get('vote_intersection_path')['segmentStart'];
            if($segmentStartId) {
                $segmentStart = $this->getDoctrine()
                    ->getRepository('CLCyclabiliteVoteBundle:Segment')
                    ->find($segmentStartId);
            } else {
                $segmentStart = Null;
            }

            # récupération du segment end Id
            $segmentStartEnd = $request->request->get('vote_intersection_path')['segmentEnd'];
            $segmentEndId = $request->request->get('segmentEnd');
            if($segmentEndId) {
                $segmentEnd = $this->getDoctrine()
                    ->getRepository('CLCyclabiliteVoteBundle:Segment')
                    ->find($segmentEndId);
            } else {
                $segmentStart = Null;
            }

            # récupération ou création de l'intersection path !
            $intersectionPaths = $this->getDoctrine()
                ->getRepository('CLCyclabiliteVoteBundle:IntersectionPath')
                ->findByOrCreate(array(
                'node' => $entity,
                'segmentStart' => $segmentStart,
                'segmentEnd' => $segmentEnd
            ));

            if (count($intersectionPaths) !== 1) {
                throw new \Exception("We could not create / retrieve intersection paths");
            }
            $intersectionPath = $intersectionPaths[0];

            $vote = new VoteIntersectionPath();
            $formType = VoteIntersectionPathType::class;
            $vote->setIntersectionPath($intersectionPath); // ici on gère l'intersection path

        } else {
            throw $this->createNotFoundException("The type $type does not "
                . "exists for a vote");
        }

        //check that something has been sent...
        if ($request->getMethod() !== 'POST') {
            $response = new Response($this->get('translator')
                    ->trans('c.vote.add.request_empty'));
            $response->setStatusCode(Response::HTTP_METHOD_NOT_ALLOWED);
            return $response;
        }

        //if ok, we go on...
        //add current user
        $vote->setUser($this->get('security.token_storage')->getToken()->getUser());

        //add client
        //until now, this is only possible with web. Later, we will introduce
        //the possibility to get the client used with the API key.
        $vote->setClient('web');

        if ($type == 'segment') {
            $form = $this->createForm($formType, $vote);
            $form->handleRequest($request);
        } elseif ($type == 'intersection') {
            $form = $this->createForm($formType, $vote, array(
                'csrf_protection' => false
            ));
            $form->handleRequest($request);
        } else {
            dump("AIe aie aie");
        }

        $errors = $this->get('validator')->validate($vote);

        if ($errors->count() === 0) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($vote);
            $em->flush();

            //return the vote
            if ($_format == 'html') {
                //TODO: show the individual vote ?
                return new Response("ok");
            } elseif ($_format == 'json') {
                $content =
                    $this->get('serializer')->serialize(
                        array(
                            'vote' => $vote,
                            'feature' =>$entity
                        ), 'json');
                $response = new Response($content);
                return $response;
            }
        }

        //if errors :
        if ($_format == 'html') {
            $this->forward(
                'CLCyclabiliteVoteBundle:Vote:showForm', array('type' => $type));
        } else { // $type == 'josn'
            foreach ($errors as $key => $error) {
                $messages[$key] = $error->getMessage();
            }
            $content = $this->get('serializer')->serialize($messages, 'json');
            $response = new Response($content);
            $response->setStatusCode(
                Response::HTTP_NOT_ACCEPTABLE,
                $this
                    ->get('translator')
                    ->trans('controller.vote.add.errors_in_form'));
            return $response;
        }
    }

}
