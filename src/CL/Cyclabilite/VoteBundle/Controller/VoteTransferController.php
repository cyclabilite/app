<?php

namespace CL\Cyclabilite\VoteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

use CL\Cyclabilite\VoteBundle\Entity\SegmentDeleted;
use CL\Cyclabilite\VoteBundle\Entity\VoteSegment;

class VoteTransferController extends Controller
{
    const PHP_TIME_LIMIT = 180;

    /**
     * List of the deleted segments having at least one vote (that we want
     * to transfer to an exiting segment)
     *
     * @Route("/admin/transfer/segment/list/", name="deleted_segment_list_for_transfer")
     */
    public function deletedSegmentVoteList(Request $request)
    {
        $view_parameters = array();

        $em = $this->getDoctrine()->getManager();
        $deletedSegmentVotesNbr = $em->getRepository('CLCyclabiliteVoteBundle:VoteSegmentDeleted')->count([]);
        $view_parameters['del_segment_votes_nbr'] = $deletedSegmentVotesNbr;

        $deletedSegementNbr = $em->getRepository('CLCyclabiliteVoteBundle:SegmentDeleted')->count([]);
        $view_parameters['del_segment_nbr'] = $deletedSegementNbr;

        // $deletedSegements = $em->getRepository('CLCyclabiliteVoteBundle:SegmentDeleted')->findBy(array(), Null, 100);
        $deletedSegements = $em->getRepository('CLCyclabiliteVoteBundle:SegmentDeleted')->findWithVotes();

        $view_parameters['deleted_segments'] = $deletedSegements;

        return $this->render('CLCyclabiliteVoteBundle:VoteTransfer:segment-list.html.twig', $view_parameters);
    }

    /**
     * Select a deleted segment having at least one vote. And display the
     * form to report the vote(s) to another segmet.
     *
     * @Route("/admin/transfer/segment/next/", name="deleted_segment_for_transfer_next")
     */
    public function deletedSegmentVoteNext(Request $request)
    {
        $view_parameters = array();

        $em = $this->getDoctrine()->getManager();
        $deletedSegements = $em->getRepository('CLCyclabiliteVoteBundle:SegmentDeleted')->findWithVotes();

        if(count($deletedSegements) > 1) {
            return $this->redirectToRoute(
                'deleted_segment_transfer',
                ['id' => $deletedSegements[0]->getId()]);
        }

        return $this->redirectToRoute('deleted_segment_list_for_transfer');
    }

    /**
     * set a deleted vote in 'Frigo'
     *
     * @Route("/admin/transfer/put/segment/{id}/in/frigo/", name="set_segment_in_frigo")
     */
    public function setDeletedSegmentInFrigo(Request $request, SegmentDeleted $segment)
    {
        $em = $this->getDoctrine()->getManager();

        $segment->setInFrigo(True);

        $em->flush();

        $this->addFlash(
            'notice',
            'Le segment  #'.$segment->getId().' a été mis au "Frigo"'
        );

        return $this->redirectToRoute('deleted_segment_list_for_transfer');
    }


    /**
     * Get the number of trivial trasfers for a given buffer size
     *
     * @Route("/admin/trivial/transfer/stats/{delBufferSize}/{newBufferSize}", name="trivtrans_stats")
     *
     */
    public function stats4TrivialTransfer(Request $request, float $delBufferSize, float $newBufferSize)
    {
        set_time_limit(600);

        $em = $this->getDoctrine()->getManager();
        $deletedSegmentVotesNbr = $em->getRepository('CLCyclabiliteVoteBundle:VoteSegmentDeleted')->count([]);
        $deletedSegementNbr = $em->getRepository('CLCyclabiliteVoteBundle:SegmentDeleted')->count([]);

        $allTrivialTransfers = $em
            ->getRepository('CLCyclabiliteVoteBundle:Segment')
            ->getTrivialTransfer($delBufferSize, $newBufferSize);


        $trivialTransfersNbr = sizeof($allTrivialTransfers);

        return $this->render(
            'CLCyclabiliteVoteBundle:Admin:trivial-transfer-stats.html.twig',
            [
                'trivial_transfers_nbr' => $trivialTransfersNbr,
                'deleted_seg_nbr' => $deletedSegementNbr,
                'deleted_seg_votes_nbr' => $deletedSegmentVotesNbr,
            ]);
    }

    /**
     * Clean (remove) used for trivial transfer + configure variables needs for the generation of the tables (buffer size)
     *
     * Drops the tables :
     * - cyclab_segments_l93
     * - deleted_cyclab_segments_l93
     * - trivial_deleted_seg_for_transfer
     *
     * (more explanations about the trivial transfer can be found at doc/trivial_transfer_protocol.md
     *
     * @Route("/admin/trivial/transfer/reinit/{newBufferSize}/{delBufferSize}/", name="trivtrans_reinit")
     */
    public function cleanTrivialTransferTables(Request $request, SessionInterface $session, $newBufferSize, $delBufferSize)
    {
        $em = $this->getDoctrine()->getManager();
        $ok = $em
            ->getRepository('CLCyclabiliteVoteBundle:Segment')
            ->dropTables(['cyclab_segments_l93', 'deleted_cyclab_segments_l93', 'trivial_deleted_seg_for_transfer']);

        $response = new JsonResponse(['ok' => $ok]);

        if($ok) {
            $session->set('reinit', TRUE);
            $session->set('del_buffer_size', $delBufferSize);
            $session->set('new_buffer_size', $newBufferSize);
        } else {
            $response->setStatusCode(Response::HTTP_BAD_REQUEST);
        }

        return $response;
    }

    /**
     * Prepare table CyclabSegmentsL93 for trivial transfer
     *
     * Creates the table cyclab_segments_l93
     *
     * (more explanations about the trivial transfer can be found at doc/trivial_transfer_protocol.md
     *
     * @Route("/admin/trivial/transfer/prepare/table/csl93", name="trivtrans_prepare_table_csl93")
     */
    public function generateCyclabSegmentsL93Table(Request $request, SessionInterface $session)
    {
        set_time_limit(self::PHP_TIME_LIMIT);
        $start = time();
        $newBufferSize = $session->get('new_buffer_size', -1);

        if($session->get('reinit') && $newBufferSize != -1) {
            $em = $this->getDoctrine()->getManager();
            $ok = $em
                ->getRepository('CLCyclabiliteVoteBundle:Segment')
                ->generateCyclabSegmentsL93Table($newBufferSize);
        } else {
            $ok = FALSE;
        }

        $response = new JsonResponse(['ok' => $ok, 'time' => (time() - $start)]);
        if(! $ok) {
            $response->setStatusCode(Response::HTTP_BAD_REQUEST);
        }

        return $response;
    }


    /**
     * Prepare index for the table CyclabSegmentsL93 for trivial transfer
     *
     * Creates an indexes on the table cyclab_segments_l93
     *
     * (more explanations about the trivial transfer can be found at doc/trivial_transfer_protocol.md
     *
     * @Route("/admin/trivial/transfer/prepare/index/csl93", name="trivtrans_prepare_index_csl93")
     */
    public function generateCyclabSegmentsL93Index(Request $request, SessionInterface $session)
    {
        set_time_limit(self::PHP_TIME_LIMIT);
        $start = time();
        $newBufferSize = $session->get('new_buffer_size', -1);

        if($session->get('reinit') && $newBufferSize != -1) {
            $em = $this->getDoctrine()->getManager();
            $ok = $em
                ->getRepository('CLCyclabiliteVoteBundle:Segment')
                ->generateCyclabSegmentsL93Index($newBufferSize);
        } else {
            $ok = FALSE;
        }

        $response = new JsonResponse(['ok' => $ok, 'time' => (time() - $start)]);
        if(! $ok) {
            $response->setStatusCode(Response::HTTP_BAD_REQUEST);
        }

        return $response;
    }



    /**
     * Get the number of trivial transfers for a given buffer size
     *
     * - Prepares the db for the trivial transfers procedure
     * - and when it is finish gives access to this procedure
     *
     * For the preparation of the trivial transfers procedure, a JS script will
     * visit the following routes :
     *  - trivtrans_reinit
     *  - trivtrans_prepare_table_csl93
     *  - trivtrans_prepare_index_csl93
     *
     * The JS script is /assets/js/admin_trivial_prepare.js
     *
     * (more explanations about the trivial transfer can be found at doc/trivial_transfer_protocol.md
     *
     * @Route("/admin/trivial/transfer/prepare/{delBufferSize}/{newBufferSize}/", name="trivtrans_prepare")
     */
    public function prepareDataForTrivialTransfer(Request $request, SessionInterface $session, float $delBufferSize, float $newBufferSize)
    {
        return $this->render(
            'CLCyclabiliteVoteBundle:VoteTransfer:prepare-data-trivial-transfer.html.twig',
            [
                'del_buffer_size' => $delBufferSize,
                'new_buffer_size' => $newBufferSize,
            ]);
    }


    /**
     * Get the number of trivial trasfers for a given buffer size
     *
     * @Route("/admin/trivial/transfer/check/", name="trivtrans_check")
     *
     */
    public function trivialTransferCheck(Request $request, SessionInterface $session)
    {
        $delBufferSize = $session->get('del_buffer_size', -1);
        $newBufferSize = $session->get('new_buffer_size', -1);

        if($session->get('reinit') && $delBufferSize != -1 && $newBufferSize != -1) {
            $limit = 50;

            $em = $this->getDoctrine()->getManager();

            $trivialTransfersId = $em
                ->getRepository('CLCyclabiliteVoteBundle:Segment')
                ->getTrivialTransfer($delBufferSize, $newBufferSize, $limit);


            $deletedSegmentVotesNbr = $em->getRepository('CLCyclabiliteVoteBundle:VoteSegmentDeleted')->count([]);
            $deletedSegementNbr = $em->getRepository('CLCyclabiliteVoteBundle:SegmentDeleted')->count([]);

            $serializer = $this->get('serializer');
            $trivialTransfers = $this->transferIdToTransfer($trivialTransfersId, $serializer);

            return $this->render(
                'CLCyclabiliteVoteBundle:VoteTransfer:trivial-transfer-check.html.twig',
                [
                    'trivial_transfer' => $trivialTransfers,
                    'del_buffer_size' => $delBufferSize,
                    'new_buffer_size' => $newBufferSize,
                ]);
        } else {
            return new Response(
                'PAS BONNE INIT',
                Response::HTTP_BAD_REQUEST,
                array('content-type' => 'text/html')
            );
        }
    }


    private function transferIdToTransfer($trivialTransfersId, $serializer)
    {
        $em = $this->getDoctrine()->getManager();
        $trivialTransfers = [];

        foreach ($trivialTransfersId as $transferId) {
            $deletedSegmentId = $transferId['del_seg_id'];
            $deletedSegment = $em
                ->getRepository('CLCyclabiliteVoteBundle:SegmentDeleted')
                ->find($deletedSegmentId);

            $newSegmentId = $transferId['new_seg_id'];
            $newSegment = $em
                ->getRepository('CLCyclabiliteVoteBundle:Segment')
                ->find($newSegmentId);

            $trivialTransfers[] = [
                'del_seg' => $deletedSegment,
                'del_seg_json' => $serializer->serialize($deletedSegment, 'json', array()),
                'new_seg' => $newSegment,
                'new_seg_json' => $serializer->serialize($newSegment, 'json', array()),
            ];
        }

        return $trivialTransfers;
    }

    /**
     * handle the automatic / trivial vote transfer
     * on a given number of deleted segments
     *
     * @Route("/admin/trivial/transfer/handle/{transferNbr}/", name="trivtrans_handle")
     */
    public function trivialTransferHandle(Request $request, SessionInterface $session, int $transferNbr=100)
    {
        $delBufferSize = $session->get('del_buffer_size', -1);
        $newBufferSize = $session->get('new_buffer_size', -1);

        if($session->get('reinit') && $delBufferSize != -1 && $newBufferSize != -1) {
            $em = $this->getDoctrine()->getManager();

            $trivialTransfersId = $em
                ->getRepository('CLCyclabiliteVoteBundle:Segment')
                ->getTrivialTransfer($delBufferSize, $newBufferSize,  $transferNbr);


            $serializer = $this->get('serializer');
            $trivialTransfers = $this->transferIdToTransfer($trivialTransfersId, $serializer);

            foreach ($trivialTransfers as $trans) {
                $deletedSegment = $trans['del_seg'];
                $newSegment = $trans['new_seg'];

                foreach ($deletedSegment->getVotes() as $deletedVote) {
                    $hasError = False;

                    $newVoteSegment = $deletedVote->transferTo($newSegment);

                    $validator = $this->get('validator');
                    $errors = $validator->validate($newVoteSegment);

                    if (count($errors) === 0) {
                        $em->persist($newVoteSegment);
                        $em->flush();
                    } else {

                        $alreadyVoteExisting = $em
                        ->getRepository('CLCyclabiliteVoteBundle:VoteSegment')
                        ->findBy(array(
                            'user' => $newVoteSegment->getUser(),
                            'profile' => $newVoteSegment->getProfile(),
                            'segment' => $newVoteSegment->getSegment(),
                            'datetime_vote' => $newVoteSegment->getDatetimeVote()
                        ));

                        // check if the vote already exists
                        if(count($alreadyVoteExisting)== 0) {
                            $hasError = True;

                            var_dump('Error : ');
                            foreach ($errors as $key => $value) {
                                var_dump($value->getMessage());
                            }
                            var_dump('Segment id:'.$deletedSegment->getId().' non traité');
                            var_dump('('.$deletedSegment->getId().'->'.$newSegment->getId().')');
                        }
                    }

                    if(! $hasError) {
                        $em->remove($deletedVote);
                        $em->flush();
                    }
                }
            }

            $em->flush();

            return new Response(
                'Ok !',
                Response::HTTP_OK,
                array('content-type' => 'text/html')
            );
        } else {
            return new Response(
                "Aie : pbm d'initialisation.",
                Response::HTTP_BAD_REQUEST,
                array('content-type' => 'text/html')
            );
        }

    }

    /**
     * View to report a votes on a deleted segment to a existing segment (not deleted)
     *
     * This is the 'manual' procedure.
     *
     * @Route("/admin/transfer/segment/{id}", name="deleted_segment_transfer")
     */
    public function deletedSegmentVoteTransfer(Request $request, SegmentDeleted $segment)
    {
        $session = $request->getSession();
        $csrfToken = $this->has('security.csrf.token_manager')
            ? $this->get('security.csrf.token_manager')->getToken('authenticate')->getValue()
            : null;

        $lastUsernameKey = Security::LAST_USERNAME;
        $lastUsername = (null === $session) ? '' : $session->get($lastUsernameKey);

        $view_parameters = array(
            'last_username' => $lastUsername,
            'error' => null,
            'csrf_token' => $csrfToken);

        $view_parameters['segment'] = $segment;

        $segmentJSON = $this->get('serializer')->serialize($segment, 'json');
        $view_parameters['segment_json'] = $segmentJSON;

        $formBuilder = $this->getSelectionFormBuilder();
        $formBuilder->setAction($this->generateUrl('handle_segment_vote_transfer', ['id' => $segment->getId()]));

        $view_parameters['form'] = $formBuilder->getForm()->createView();


        return $this->render('CLCyclabiliteVoteBundle:Admin:vote-transfer.html.twig', $view_parameters);
    }

    /**
     * Handle a vote transfert form a deleted segment to a existing segment (not deleted)
     *
     * @Route("/transfer/segment/{id}/handle", name="handle_segment_vote_transfer")
     */
    public function handleVoteTransfer(Request $request, SegmentDeleted $deletedSegment)
    {
        $form = $this->getSelectionFormBuilder()->getForm();
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            $selectedMarkers = $data['selectedMarkers'];
            $selectedSegments = $data['selectedSegments'];

            $selectedSegmentIds = [];
            foreach (explode(",", $selectedSegments) as $id) {
                $id = intval($id);
                $selectedSegmentIds[] = intval($id);
            }

            $selectedSegmentIds = explode(",", $selectedSegments);

            $newSegments = $em->getRepository('CLCyclabiliteVoteBundle:Segment')->findBy(['id' => $selectedSegmentIds]);

            foreach ($newSegments as $newSegment) {
                foreach ($deletedSegment->getVotes() as $deletedVote) {
                    $newVoteSegment = $deletedVote->transferTo($newSegment);

                    $em->persist($newVoteSegment);
                    $em->remove($deletedVote);
                }
            }

            $em->flush();

            $this->addFlash(
                'notice',
                'Les votes du segment #'.$deletedSegment->getId().' ont bien été transférés'
            );

            return $this->redirectToRoute('deleted_segment_for_transfer_next');
        }


        $responseError = new Response(
            'Formulaire invalide',
            Response::HTTP_BAD_REQUEST,
            array('content-type' => 'text/html')
        );

        return $responseError;
    }

    private function getSelectionFormBuilder()
    {
        $form = $this->createFormBuilder()
            ->add('selectedMarkers', TextType::class, array(
                'label' => 'Segments choisis',
                'constraints' => new NotBlank(),
                'attr' => array(
                    'autofocus' => true
                    )
            ))
            ->add(
                'selectedSegments',
                TextType::class, //HiddenType::class,
                array(
                    'label' => 'Identifiants choisis',
                    'constraints' => new NotBlank(),
                    'attr' => array(
                        'readonly' => true,
                    )
                ));
        return $form;
    }
}
