<?php

namespace CL\Cyclabilite\VoteBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use CL\GeoBundle\Entity\BBox;

/**
 * Segment controller.
 *
 */
class SegmentDeletedController extends Controller
{
    /**
     * Lists all Segment being in the db.
     *
     */
    public function listAllAction($_format)
    {
        $em = $this->getDoctrine()->getManager();

        // $entities = $em->getRepository('CLCyclabiliteVoteBundle:SegmentDeleted')->findAll();

        $query = $em->createQuery('SELECT s FROM '
                . 'CL\Cyclabilite\VoteBundle\Entity\SegmentDeleted s ');
        // $query->setFetchMode('CL\Cyclabilite\VoteBundle\Entity\Segment', 'way', 'EAGER');
        $query->setMaxResults(50);
        $entities = $query->getResult();

        if ($_format === 'html') {
            return $this->render('CLCyclabiliteVoteBundle:Segment:list.html.twig', array(
                        'entities' => $entities,
            ));
        } else {
            return $this->returnJson($entities);
        }
    }

    private function returnJson($results)
    {
        return new Response(
                $this->get('serializer')->serialize($results, 'json')
            );
    }
}
