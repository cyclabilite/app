<?php

namespace CL\Cyclabilite\VoteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;

class DefaultController extends Controller
{
    public function indexAction(Request $request)
    {
        $session = $request->getSession();

        $csrfToken = $this->has('security.csrf.token_manager')
            ? $this->get('security.csrf.token_manager')->getToken('authenticate')->getValue()
            : null;

        $lastUsernameKey = Security::LAST_USERNAME;
        $lastUsername = (null === $session) ? '' : $session->get($lastUsernameKey);

        $view_parameters = array('piwik' => False,
            'last_username' => $lastUsername,
            'error' => null,
            'csrf_token' => $csrfToken);

        if($this->container->hasParameter('cities')) {
            $view_parameters['cities'] = $this->container->getParameter('cities');
        } else {
            $view_parameters['cities'] = [];
        }

        if($this->container->hasParameter('default_city')) {
            $view_parameters['default_city'] = $this->container->getParameter('default_city');
        }

        if($this->container->hasParameter('piwik_url') AND
            $this->container->hasParameter('piwik_site_id')) {
                $view_parameters['piwik'] = True;
                $view_parameters['piwik_url'] = $this->container->getParameter('piwik_url');
                $view_parameters['piwik_site_id'] = $this->container->getParameter('piwik_site_id');
            }

        return $this->render(
                'CLCyclabiliteVoteBundle:Default:index.html.twig', $view_parameters);
    }
}
