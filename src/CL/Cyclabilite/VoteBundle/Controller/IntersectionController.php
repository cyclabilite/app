<?php

namespace CL\Cyclabilite\VoteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use CL\GeoBundle\Entity\BBox;

/**
 * Intersection controller.
 *
 */
class IntersectionController extends Controller
{
    /**
     * Lists all Node being in the db.
     *
     */
    public function listAllAction($_format)
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('CLCyclabiliteVoteBundle:Intersection')->findBy(array(), null, 100);
        
        switch ($_format) {
           case 'html' : 
              return $this->render('CLCyclabiliteVoteBundle:Node:listAll.html.twig', array(
               'entities' => $entities,
                ));
              break;
           case 'json':
              return $this->returnJson($entities);
              break;
        }
        
    }

    /**
     * TODO
     */
    public function detailsOfAction($id, $_format)
    {
        $em = $this->getDoctrine()->getManager();
        $intersection = $em->getRepository('CLCyclabiliteVoteBundle:Intersection')->find($id);

        if ($_format === 'json') {
            return new Response(
                json_encode(
                    $this->get('cl_cyclabilite_vote.normalizer.intersection.detailed')->normalize($intersection, 'json'))
            );
        } else {
            throw new \Exception('Only json format');
        }
    }

    /**
     * List ALL segment being in a bbox
     * 
     * @param type $_format
     * @param type $bbox
     */
    public function listBBOXAction($_format, Request $request)
    {
        try {
            $bbox = BBox::getBBoxFromRequest($request);
        } catch (Exception $ex) {
            $r = new Response($ex->getMessage());
            $r->setStatusCode(Response::HTTP_BAD_REQUEST);
            return $r;
        }

        $repository = $this->getDoctrine()
            ->getRepository('CLCyclabiliteVoteBundle:Intersection');

        $query = $repository->createQueryBuilder('i')
            ->where('intersects(:bbox, i.point) = TRUE')
            ->andWhere('i.voteDefault > 0')
            ->setParameter('bbox', $bbox)
            ->getQuery();
    
        $intersections = $query->getResult();

        $intersections_array = array();
        foreach ($intersections as $intersection) {
            $intersections_array[] = $intersection->getId();
        }

        $qb = $this->getDoctrine()->getManager()->createQueryBuilder();
        $qb ->select('s')
            ->from('CLCyclabiliteVoteBundle:Segment', 's')
            ->andWhere('((s.nodeStart IN (:inter)) OR (s.nodeEnd IN (:inter)))')
            ->setParameter('inter', $intersections_array);

        $ways = $qb->getQuery()->execute();

        // ICI AJOUTER POUR CHAQUE NODE LE WAY QUI PASSE DEDANS


        switch($_format) {
            case 'html' :
                return $this->render('CLCyclabiliteVoteBundle:Node:listAll.html.twig', array(
                    'entities' => $intersections,
                ));
                break;
            case 'json' :
                return $this->returnJson($intersections);
        }       
    }

    private function returnJson($results)
    {
        return new Response(
                $this->get('serializer')->serialize($results, 'json')
            );
    }
}

