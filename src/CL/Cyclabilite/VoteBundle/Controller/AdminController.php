<?php

namespace CL\Cyclabilite\VoteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;


class AdminController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $averageComputer = $this->container->get('cl_cyclabilite_vote.compute_average');
        $deletedSegmentVotesNbr = $em->getRepository('CLCyclabiliteVoteBundle:VoteSegmentDeleted')->count([]);

        $env = getenv('APP_ENV');
        if (empty($env)) {
            $env = 'prod';
        }

        $debug = (getenv('APP_DEBUG') === 'true');

        return $this->render('CLCyclabiliteVoteBundle:Admin:index.html.twig', [
            "averageComputerName" => $averageComputer->getName(),
            "env" => $env,
            "debug" => $debug,
            "deletedSegmentVotesNbr" => $deletedSegmentVotesNbr,
        ]);
    }

    public function recomputeAverageAction(Request $request)
    {
        if ($request->query->has('recomputeAllAverage') && $request->query->get('recomputeAllAverage') == 'yes') {
            ini_set('memory_limit','512M');
            ini_set('max_execution_time', '600');
            $recomputeAverage = $this->get('cl_cyclabilite_vote.recompute_all_average');
            $statusMessage = $recomputeAverage->recomputeAll();
            $this->get('session')->getFlashBag()->add('notice', $this->get('translator')->trans($statusMessage));
            return $this->redirectToRoute('cl_cyclabilite_recompute_average');
        }

        return $this->render('CLCyclabiliteVoteBundle:Admin:recompute-all-the-votes.html.twig');
    }
}
