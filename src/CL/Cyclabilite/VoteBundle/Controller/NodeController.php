<?php

namespace CL\Cyclabilite\VoteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use CL\Chill\Cyclabilite\VoteBundle\Entity\Node;

/**
 * Node controller.
 *
 */
class NodeController extends Controller
{
    /**
     * Lists all Node being in the db.
     *
     */
    public function listAllAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('CLCyclabiliteVoteBundle:Node')->findBy(array(), null, 100);

        return $this->render('CLCyclabiliteVoteBundle:Node:listAll.html.twig', array(
            'entities' => $entities,
        ));
    }

    public function listBBOXAction($_format, Request $request)
    {
        echo "TODO";
    }
}
