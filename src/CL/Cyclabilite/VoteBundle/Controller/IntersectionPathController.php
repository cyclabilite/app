<?php

namespace CL\Cyclabilite\VoteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use CL\Chill\Cyclabilite\VoteBundle\Entity\IntersectionPath;

/**
 * Intersection Paths controller.
 *
 */
class IntersectionPathController extends Controller
{
    /**
     * Lists all the Intersection Paths being in the db.
     *
     */
    public function listAllAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('CLCyclabiliteVoteBundle:IntersectionPath')->findAll();

        return $this->render('CLCyclabiliteVoteBundle:IntersectionPath:listAll.html.twig', array(
            'entities' => $entities,
        ));
    }

    public function listBBOXAction($_format, Request $request)
    {
        echo "TODO";
    }
}
