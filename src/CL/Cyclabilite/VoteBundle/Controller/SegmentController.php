<?php

namespace CL\Cyclabilite\VoteBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


use CL\GeoBundle\Entity\BBox;

/**
 * Segment controller.
 *
 */
class SegmentController extends Controller
{
    /**
     * Lists all Segment being in the db.
     * @Route("/segment/list/all.{_format}", name="segment_list_all")
     */
    public function listAllAction($_format)
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery('SELECT s FROM '
                . 'CL\Cyclabilite\VoteBundle\Entity\Segment s ');
        $query->setFetchMode('CL\Cyclabilite\VoteBundle\Entity\Segment', 'way', 'EAGER');
        $query->setMaxResults(100);
        $entities = $query->getResult();

        if ($_format === 'html') {
            return $this->render('CLCyclabiliteVoteBundle:Segment:list.html.twig', array(
                        'entities' => $entities,
            ));
        } elseif ($_format === 'json') {
            return $this->returnJson($entities);
        }
    }

    /**
     * TODO
     */
    public function detailsOfAction($id, $_format)
    {
        $em = $this->getDoctrine()->getManager();
        $segment = $em->getRepository('CLCyclabiliteVoteBundle:Segment')->find($id);

        if ($_format === 'json') {
            return new Response(
                json_encode(
                    $this->get('cl_cyclabilite_vote.normalizer.segment.detailed')->normalize($segment, 'json'))
            );
        } else {
            throw new \Exception('Only json format');
        }

        // TODO detailed segment et ailleurs simple json
    }

    /**
     * List ALL segment being in a bbox
     * @Route("/segment/list/bbox.{_format}", name="segment_list_box")
     *
     * @param type $_format
     * @param type $bbox
     */
    public function listBBOXAction($_format, Request $request)
    {
        try {
            $bbox = BBox::getBBoxFromRequest($request);
        } catch (Exception $ex) {
            $r = new Response($ex->getMessage());
            $r->setStatusCode(Response::HTTP_BAD_REQUEST);
            return $r;
        }

        $queryString = 'SELECT s '
                . 'FROM CLCyclabiliteVoteBundle:Segment s '
                . 'WHERE s.voteDefault > 0 '
                . 'AND intersects(:bbox, s.line) = TRUE';


        $query = $this->getDoctrine()->getManager()->createQuery($queryString);
        $query->setParameter('bbox', $bbox);
        $segments = $query->getResult();

        $JSONDetailedSegment = $this->get('serializer')->serialize($segments, 'json');

        switch($_format) {
            case 'html' :
                return $this->render('CLCyclabiliteVoteBundle:Segment:list.html.twig', array(
                        'entities' => $segments,
            ));
                break;
            case 'json' :
                return $this->returnJson($segments);
        }

        switch($_format) {
            case 'html' :
                return $this->render('CLCyclabiliteVoteBundle:Segment:list.html.twig', array(
                        'entities' => $segments,
            ));
                break;
            case 'json' :
                return $this->returnJson($segments);
        }
    }

    private function returnJson($results)
    {
        return new Response(
                $this->get('serializer')->serialize($results, 'json')
            );
    }

}
