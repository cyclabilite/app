<?php

namespace CL\Cyclabilite\VoteBundle\Serializer;

use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use CL\Cyclabilite\VoteBundle\Entity\Segment;
use CL\Cyclabilite\VoteBundle\Entity\SegmentDeleted;
use CL\Cyclabilite\VoteBundle\Serializer\NodeNormalizer;

/**
 *
 *
 * @author julien.fastre@champs-libres.coop
 */
class SegmentNormalizer implements NormalizerInterface, ContainerAwareInterface {

    const KEY_NODE_START    = 'node_start';
    const KEY_NODE_END      = 'node_end';
    const KEY_ID            = 'id';
    const KEY_ENTITY_TYPE   = 'type';
    const KEY_WAY_ID        = 'way_id';
    const KEY_NAME          = 'way_name';
    const KEY_LINE          = 'line';
    const KEY_WITH_DETAILS  = 'with_details';

    const SERVICE_NAME = 'cl_cyclabilite_vote.normalizer.segment';
    /**
     *
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    private $container;

    use VotesInformationNormalizerTrait;

   /**
    *
    * @param CL\Cyclabilite\VoteBundle\Entity\Segment $object
    * @param string $format
    * @param array $context
    */
    public function normalize($object, $format = null, array $context = array()) {
        $a = array(
            "type" => "Feature",
            "geometry" => $object->getLine(),
            "properties" => array(
                self::KEY_ENTITY_TYPE => 'segment',
                self::KEY_WITH_DETAILS => false,
                self::KEY_ID => $object->getId(),
                self::KEY_NODE_START => $this->container
                    ->get(NodeNormalizer::SERVICE_NAME)
                    ->normalize($object->getNodeStart()),
                self::KEY_NODE_END => $this->container
                    ->get(NodeNormalizer::SERVICE_NAME)
                    ->normalize($object->getNodeEnd()),
            )
        );

        $a["properties"]  = $this->addVoteInformation($a["properties"], $object);

        return $a;
    }

    public function supportsNormalization($data, $format = null) {
        return $data instanceof Segment || $data instanceof SegmentDeleted;
    }

    public function setContainer(\Symfony\Component\DependencyInjection\ContainerInterface $container = null) {
        $this->container = $container;
    }
}
