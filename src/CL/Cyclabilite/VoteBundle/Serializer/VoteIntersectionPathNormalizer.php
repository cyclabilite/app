<?php

namespace CL\Cyclabilite\VoteBundle\Serializer;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use CL\Cyclabilite\VoteBundle\Serializer\VoteNormalizer;
use CL\Cyclabilite\VoteBundle\Serializer\IntersectionPathNormalizer;
use CL\Cyclabilite\UserBundle\Serializer\UserNormalizer;
use CL\Cyclabilite\UserBundle\Serializer\ProfileNormalizer;
use CL\Cyclabilite\VoteBundle\Entity\VoteIntersectionPath;

/**
 * Serialize a vote on segment
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class VoteIntersectionPathNormalizer extends VoteNormalizer  {
    
    const KEY_INTERSECTION_PATH = 'intersection_path';
    
    private $intersectionPathNormalizer;
    
    
    public function __construct(UserNormalizer $userNormalizer,
        ProfileNormalizer $profileNormalizer, 
        IntersectionPathNormalizer $intersectionPathNormalizer)
    {
       parent::__construct($userNormalizer, $profileNormalizer);
       $this->intersectionPathNormalizer = $intersectionPathNormalizer;
    }
    
    /**
     * 
     * @param \CL\Cyclabilite\VoteBundle\Entity\VoteIntersectionPath $vote
     * @param string $format
     * @param array $context
     * @return array
     */
    public function normalize($vote, $format = null, array $context = array()) {
        $array = parent::normalize($vote, $format, $context);
        
        $array[self::KEY_INTERSECTION_PATH] = $this->intersectionPathNormalizer
                ->normalize($vote->getIntersectionPath());

        return $array;
    }
            
            
    public function supportsNormalization($data, $format = null) {
        return $data instanceof VoteIntersectionPath;
    }


}
