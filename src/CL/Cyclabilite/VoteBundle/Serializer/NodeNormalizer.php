<?php

namespace CL\Cyclabilite\VoteBundle\Serializer;

use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

use CL\Cyclabilite\VoteBundle\Entity\Node;
use CL\Cyclabilite\VoteBundle\Entity\NodeDeleted;
use CL\Cyclabilite\VoteBundle\Entity\Intersection;


/**
 * Serialize a point to
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class NodeNormalizer implements NormalizerInterface{

    const KEY_ID = 'id';
    const KEY_ENTITY_TYPE = 'type';
    const SERVICE_NAME = 'cl_cyclabilite_vote.normalizer.node';

    use VotesInformationNormalizerTrait;

    /**
     *
     * @param CL\Cyclabilite\VoteBundle\Entity\Node $object
     * @param string $format
     * @param array $context
     */
    public function normalize($object, $format = null, array $context = array()) {
        $a = array(
            "type" => "Feature",
            "geometry" => $object->getPoint(),
            "properties" => array(
                self::KEY_ENTITY_TYPE => 'node',
                self::KEY_ID => $object->getId()
            )
        );

        $a["properties"] = $this->addVoteInformation($a["properties"], $object);

        return $a;
    }

    public function supportsNormalization($data, $format = null) {
        return ($data instanceof Node) || ($data instanceof NodeDeleted) || ($data instanceof Intersection);
    }

}
