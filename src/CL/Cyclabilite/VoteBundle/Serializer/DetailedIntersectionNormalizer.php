<?php

namespace CL\Cyclabilite\VoteBundle\Serializer;

use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

use CL\Cyclabilite\VoteBundle\Entity\Node;
use CL\Cyclabilite\VoteBundle\Entity\Intersection;


/**
 * Serialize an node with the details
 *
 */
class DetailedIntersectionNormalizer implements NormalizerInterface
{
    const KEY_ID = 'id';
    const KEY_ENTITY_TYPE = 'type';
    const KEY_WITH_DETAILS  = 'with_details';
    const KEY_VOTES = 'votes';
    const KEY_SEGMENTS = 'segments';

    const SERVICE_NAME = 'cl_cyclabilite_vote.normalizer.intersection.detailed';

    use VotesInformationNormalizerTrait;

    /**
     * @var TOOD
     */
    private $voteIntersectionPathNormalizer;

    /**
     * TODO
     */
    private $segmentNormalizer;

    public function __construct(VoteIntersectionPathNormalizer $voteIntersectionPathNormalizer, SegmentNormalizer $segmentNormalizer)
    {
      $this->voteIntersectionPathNormalizer = $voteIntersectionPathNormalizer;
      $this->segmentNormalizer = $segmentNormalizer;
    }

    /**
     *
     * @param CL\Cyclabilite\VoteBundle\Entity\Node $object
     * @param string $format
     * @param array $context
     */
    public function normalize($object, $format = null, array $context = array()) {
        $a = array(
            "type" => "Feature",
            "geometry" => $object->getPoint(),
            "properties" => array(
                self::KEY_ID => $object->getId(),
                self::KEY_ENTITY_TYPE => 'intersection',
                self::KEY_VOTES => array_map(
                    function($v) {
                       return $this->voteIntersectionPathNormalizer->normalize($v);
                    },
                    $object->getVotes()->toArray()
                ),
                self::KEY_SEGMENTS => array_map(
                    function($s) {
                        return $this->segmentNormalizer->normalize($s);
                    },
                    $object->getSegments()->toArray()
                ),
            )
        );

        $a["properties"] = $this->addVoteInformation($a["properties"], $object);

        return $a;
    }

    public function supportsNormalization($data, $format = null) {
        return ($data instanceof Node) or ($data instanceof Intersection);
    }

}
