<?php

namespace CL\Cyclabilite\VoteBundle\Serializer;

/**
 * Normalize information about  votes
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
trait VotesInformationNormalizerTrait
{
   /**
    * add key with vote information to objects
    * 
    * @param array $a
    * @param class $object object which use VotesInformationTrait
    * @return array the $a with relevant keys added
    */
   protected function addVoteInformation(array $a, $object)
   {
      if ($object->getVoteNumber() !== 0) {
         $a["vote_average"] = $object->getVoteAverage();
      } else {
         $a["vote_default"] = $object->getVoteDefault();
      }
      
      $a["vote_number"] = $object->getVoteNumber();
      
      return $a;
   }
}
