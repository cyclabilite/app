<?php

namespace CL\Cyclabilite\VoteBundle\Serializer;

use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use CL\Cyclabilite\VoteBundle\Entity\Way;
use CL\Cyclabilite\VoteBundle\Serializer\SegmentNormalizer;
use CL\Cyclabilite\VoteBundle\Serializer\NodeNormalizer;


/**
 * @author julien.fastre@champs-libres.coop
 */

class WayNormalizer implements NormalizerInterface
{
   //const KEY_NODE_START    = 'node_start';
   //const KEY_NODE_END      = 'node_end';
   const KEY_ID = 'id';
   const KEY_NAME = 'way_name';
   const KEY_ENTITY_TYPE   = 'type';
   const KEY_HIGHWAY = 'highway';
   const KEY_SEGMENTS = 'segments';

   const SERVICE_NAME = 'cl_cyclabilite_vote.normalizer.way';

   private $segmentNormalizer;
   private $nodeNormalizer;

   public function __construct(SegmentNormalizer $segmentNormalizer, NodeNormalizer $nodeNormalizer)
   {
      $this->segmentNormalizer = $segmentNormalizer;
      $this->nodeNormalizer = $nodeNormalizer;
   }

   /**
    * 
    * @param CL\Cyclabilite\VoteBundle\Entity\Way $object
    * @param string $format
    * @param array $context
    */
   public function normalize($object, $format = null, array $context = array())
   {
      $a = array(
         "type" => "Feature",
         "geometry" => $object->getLine(),
         "properties" => array(
            /*
            self::KEY_NODE_START => $this->nodeNormalizer->normalize(
               $object->getNodeStart()),
            self::KEY_NODE_END => $this->nodeNormalizer->normalize(
               $object->getNodeEnd()),
            */
            self::KEY_ID => $object->getId(),
            self::KEY_NAME => $object->getName(),
            self::KEY_ENTITY_TYPE => 'way',
            self::KEY_HIGHWAY => $object->getHighway(),
            self::KEY_SEGMENTS =>
               array(
                  "type" => "FeatureCollection",
                  "features" =>
                     array_map(
                        function($s) {
                           return $this->segmentNormalizer->normalize($s); 
                        },
                        $object->getSegments()->toArray()
                     )
               )
         )
      );
      
      return $a;
   }

   public function supportsNormalization($data, $format = null)
   {
      return $data instanceof Way;
   }
}
