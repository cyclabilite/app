<?php

namespace CL\Cyclabilite\VoteBundle\Serializer;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use CL\Cyclabilite\VoteBundle\Serializer\VoteNormalizer;
use CL\Cyclabilite\VoteBundle\Serializer\SegmentNormalizer;
use CL\Cyclabilite\UserBundle\Serializer\UserNormalizer;
use CL\Cyclabilite\UserBundle\Serializer\ProfileNormalizer;
use CL\Cyclabilite\VoteBundle\Entity\VoteSegment;
use CL\Cyclabilite\VoteBundle\Entity\VoteSegmentDeleted;

/**
 * Serialize a vote on segment
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class VoteSegmentNormalizer extends VoteNormalizer
{
    //const KEY_SEGMENT = 'segment';
    const KEY_DIRECTION = 'direction';
    // direction_forward
    // direction_backward
    // direction_both

    private $segmentNormalizer;

    public function __construct(UserNormalizer $userNormalizer,
        ProfileNormalizer $profileNormalizer,
        SegmentNormalizer $segmentNormalizer)
    {
       parent::__construct($userNormalizer, $profileNormalizer);
       $this->segmentNormalizer = $segmentNormalizer;
    }

    /**
     *
     * @param \CL\Cyclabilite\VoteBundle\Entity\VoteSegment $vote
     * @param string $format
     * @param array $context
     * @return array
     */
    public function normalize($vote, $format = null, array $context = array()) {
        $array = parent::normalize($vote, $format, $context);

        $direction = $vote->getDirection();

        $array[self::KEY_DIRECTION] = $vote->getDirection();

        if($direction == VoteSegment::DIRECTION_FORWARD) {
            $array['direction_forward'] = True;
        } elseif ($direction == VoteSegment::DIRECTION_BACKWARD) {
            $array['direction_backward'] = True;
        } else {
            $array['direction_both'] = True;
        }

        /*
        $array[self::KEY_SEGMENT] = $this->segmentNormalizer
                ->normalize($vote->getSegment());
        */

        return $array;
    }

    public function supportsNormalization($data, $format = null) {
        return $data instanceof VoteSegment || $data instanceof VoteSegmentDeleted;
    }
}
