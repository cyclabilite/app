<?php

namespace CL\Cyclabilite\VoteBundle\Serializer;

use CL\Cyclabilite\VoteBundle\Serializer\VoteNormalizer;
use CL\Cyclabilite\VoteBundle\Serializer\WayNormalizer;
use CL\Cyclabilite\UserBundle\Serializer\UserNormalizer;
use CL\Cyclabilite\UserBundle\Serializer\ProfileNormalizer;
use CL\Cyclabilite\VoteBundle\Entity\VoteWay;

/**
 * Serialize a vote on segment
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class VoteWayNormalizer extends VoteNormalizer
{

   const KEY_WAY = 'way';
   const KEY_DIRECTION = 'direction';

   /**
    *
    * @var \CL\Cyclabilite\VoteBundle\Serializer\WayNormalizer
    */
   private $wayNormalizer;

   public function __construct(UserNormalizer $userNormalizer, 
           ProfileNormalizer $profileNormalizer, 
           WayNormalizer $wayNormalizer)
   {
      parent::__construct($userNormalizer, $profileNormalizer);
      $this->wayNormalizer = $wayNormalizer;
   }

   /**
    * 
    * @param \CL\Cyclabilite\VoteBundle\Entity\VoteSegment $vote
    * @param string $format
    * @param array $context
    * @return array
    */
   public function normalize($vote, $format = null, array $context = array())
   {
      $array = parent::normalize($vote, $format, $context);
      
      $array[self::KEY_DIRECTION] = $vote->getDirection();
      $array[self::KEY_WAY] = $this->wayNormalizer
              ->normalize($vote->getWay());

      return $array;
   }

   public function supportsNormalization($data, $format = null)
   {
      return $data instanceof VoteWay;
   }

}
