<?php

namespace CL\Cyclabilite\VoteBundle\Serializer;

use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use CL\Cyclabilite\VoteBundle\Entity\Segment;
use CL\Cyclabilite\VoteBundle\Serializer\VoteNormalizer;

/**
 * @author marc.ducobu@champs-libres.coop
 */
class DetailedSegmentNormalizer implements NormalizerInterface, ContainerAwareInterface {
    
    const KEY_WAY           = 'way';
    const KEY_ID            = 'id';
    const KEY_ENTITY_TYPE   = 'type';
    const KEY_VOTES         = 'votes';
    const KEY_WITH_DETAILS  = 'with_details';

    const SERVICE_NAME = 'cl_cyclabilite_vote.normalizer.segment.detailed';

    /**
     * @var \Symfony\Component\DependencyInjection\ContainerInterface 
     */
    private $voteSegmentNormalizer;
    private $container;

    public function __construct(VoteNormalizer $voteSegmentNormalizer)
    {
      $this->voteSegmentNormalizer = $voteSegmentNormalizer;
    }
    
   /**
    * 
    * @param CL\Cyclabilite\VoteBundle\Entity\Segment $object
    * @param string $format
    * @param array $context
    */
    public function normalize($object, $format = null, array $context = array()) {
        $a = array(
            "type" => "Feature",
            "geometry" => $object->getLine(),
            "properties" => array(
                self::KEY_ENTITY_TYPE => 'segment',
                self::KEY_WITH_DETAILS => true,
                self::KEY_ID => $object->getId(),
                self::KEY_WAY =>  $this->container
                    ->get(WayNormalizer::SERVICE_NAME)
                    ->normalize($object->getWay()),
                self::KEY_VOTES =>
                    array_map(
                        function($v) {
                           return $this->voteSegmentNormalizer->normalize($v);
                        },
                        $object->getVotes()->toArray()
                    )
            ),
        );
        
        return $a;
    }

    public function supportsNormalization($data, $format = null) {
        return $data instanceof Segment;
    }

    public function setContainer(
        \Symfony\Component\DependencyInjection\ContainerInterface $container = null) {
        $this->container = $container;
    }
}
