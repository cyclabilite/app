<?php

namespace CL\Cyclabilite\VoteBundle\Serializer;

use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

use CL\Cyclabilite\VoteBundle\Entity\IntersectionPath;
use CL\Cyclabilite\VoteBundle\Serializer\SegmentNormalizer;
use CL\Cyclabilite\VoteBundle\Serializer\NodeNormalizer;

/**
 * Normalize Intersection Path
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class IntersectionPathNormalizer implements NormalizerInterface
{
   
   const KEY_SEGMENT_START = 'segment_start';
   const KEY_SEGMENT_END = 'segment_end';
   const KEY_NODE = 'node';
   const KEY_ID = 'id';
   
   /**
    *
    * @var SegmentNormalizer
    */
   private $segmentNormalizer;
   
   /**
    *
    * @var NodeNormalizer
    */
   private $nodeNormalizer;
   
   use VotesInformationNormalizerTrait;
   
   
   public function __construct(SegmentNormalizer $segmentNormalizer,
            NodeNormalizer $nodeNormalizer)
   {
      $this->segmentNormalizer = $segmentNormalizer;
      $this->nodeNormalizer = $nodeNormalizer;
   }
   
   /**
    * 
    * @param IntersectionPath $object
    * @param string $format
    * @param array $context
    */
   public function normalize($object, $format = null, array $context = array())
   {
      $segmentStartNormalised = null;
      if($object->getSegmentStart()) {
         $segmentStartNormalised = $this->segmentNormalizer
            ->normalize($object->getSegmentStart());
      }

      $segmentEndNormalised = null;
      if($object->getSegmentEnd()) {
         $segmentEndNormalised = $this->segmentNormalizer
            ->normalize($object->getSegmentEnd());
      }      

      $a = array(
          self::KEY_ID => $object->getId(),
          self::KEY_NODE => $this->nodeNormalizer->normalize($object->getNode()),
          self::KEY_SEGMENT_START => $segmentStartNormalised,
          self::KEY_SEGMENT_END   => $segmentEndNormalised,
      );
      $a = $this->addVoteInformation($a, $object);
      
      return $a;
   }

   public function supportsNormalization($data, $format = null)
   {
      return $data instanceof IntersectionPath;
   }
}
