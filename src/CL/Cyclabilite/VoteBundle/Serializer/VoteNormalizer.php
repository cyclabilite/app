<?php

namespace CL\Cyclabilite\VoteBundle\Serializer;

use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use CL\Cyclabilite\UserBundle\Serializer\UserNormalizer;
use CL\Cyclabilite\UserBundle\Serializer\ProfileNormalizer;



/**
 * Common methods for normalizing vote.
 *
 * This class contains method which are common to all kinds of vote
 * (vote on segment, vote on way, ...)
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
abstract class VoteNormalizer implements NormalizerInterface{

    const KEY_AUTHOR = 'author';
    const KEY_VALUE = 'value';
    const KEY_ID = 'id';
    const KEY_VOTE_PROFILE = 'profile';
    const KEY_VOTE_TYPE = 'type';

    /**
     *
     * @var \CL\Cyclabilite\UserBundle\Serializer\UserNormalizer
     */
    protected $userNormalizer;

    /**
     *
     * @var \CL\Cyclabilite\UserBundle\Serializer\ProfileNormalizer
     */
    protected $profileNormalizer;

    public function __construct(UserNormalizer $userNormalizer,
          ProfileNormalizer $profileNormalizer)
    {
       $this->userNormalizer = $userNormalizer;
       $this->profileNormalizer = $profileNormalizer;
    }

    /**
     *
     * @param \CL\Cyclabilite\VoteBundle\Entity\VoteAbstract $vote
     * @param string $format
     * @param array $context
     * @return array
     */
    public function normalize($vote, $format = null, array $context = array()) {
        return array(
            self::KEY_AUTHOR => $this->userNormalizer->normalize(
                  $vote->getUser(),
                  $format
                  ),
            self::KEY_ID => $vote->getId(),
            self::KEY_VALUE => $vote->getValue(),
            self::KEY_VOTE_PROFILE => $this->profileNormalizer
              ->normalize($vote->getProfile(), $format)
        );
    }

}
