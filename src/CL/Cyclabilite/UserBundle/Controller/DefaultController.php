<?php

namespace CL\Cyclabilite\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use CL\Cyclabilite\UserBundle\Entity\User;
use CL\Cyclabilite\UserBundle\Form\UserType;

class DefaultController extends Controller {

    public function registerFormAction(Request $request) {
        $emailRecorded = NULL;#$this->get('session')

        if ($emailRecorded === NULL) {
            $response = new Response($this->get('translator')->trans('c.default.'
                  . 'registerForm.email_is_null'));
            $response->setStatusCode(Response::HTTP_NOT_ACCEPTABLE);
            return $response;
        }

        $user = new User($emailRecorded);

        $profile = new \CL\Cyclabilite\UserBundle\Entity\Profile;
        $user->addProfile($profile);

        $form = $this->createForm(UserType::class, $user);

        if ($request->getMethod() === 'POST') {

            $form->handleRequest($request);

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();

                return $this->redirect(
                      $this->generateUrl('cl_cyclabilite_user.'
                            . 'registration_confirmed'));
            } else {

            }
        }

        return $this->render('CLCyclabiliteUserBundle:Register:form.html.twig', array(
                    'form' => $form->createView()
                        )
        );
    }

    public function registrationConfirmedAction(){
        return $this->render('CLCyclabiliteUserBundle:Register:confirmation.html.twig');
    }

}
