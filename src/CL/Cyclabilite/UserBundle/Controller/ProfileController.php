<?php

namespace CL\Cyclabilite\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use CL\Cyclabilite\UserBundle\Form\UserType;
use CL\Cyclabilite\UserBundle\Form\ProfileType;
use CL\Cyclabilite\UserBundle\Entity\Profile;

/**
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class ProfileController extends Controller
{
   public function createFirstAction(Request $request)
   {
      if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
         throw new AccessDeniedException($this->get('translator')->trans('c.profile.'
               . 'list_by_user.you_must_be_registered'));
      }

      $profile = new Profile();
      $user = $this->get('security.token_storage')->getToken()->getUser();


      $form = $this->createForm(ProfileType::class, $profile);
      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid()) {
         $profile = $form->getData();
         $profile->setUser($user);
         $user->addProfile($profile);
         $em = $this->getDoctrine()->getManager();
         $em->persist($profile);
         $em->persist($user);
         $em->flush();

         $this->get('session')->getFlashBag()->add('notice',
            $this->get('translator')->trans('c.profile.'
               . 'update.update_success'));

            return $this->redirect($this->generateUrl('cl_cyclabilite_user.profile'));
      }

      return $this->render('CLCyclabiliteUserBundle:Profile:create_first.html.twig',
            array('form' => $form->createView()));
   }

   public function listByUserAction()
   {
      if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
         throw new AccessDeniedException($this->get('translator')->trans('c.profile.'
               . 'list_by_user.you_must_be_registered'));
      }

      $user = $this->get('security.token_storage')->getToken()->getUser();

      $profiles = $this->getDoctrine()
         ->getManager()
         ->getRepository('CLCyclabiliteUserBundle:Profile')
         ->findBy(array('user' => $user));

      $response = $this->get('serializer')->serialize($profiles, 'json', array(
         'by_user'
      ));

      return new Response($response);
   }

   public function updateAction(Request $request)
   {
      if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
         throw new AccessDeniedException($this->get('translator')->trans('c.profile.'
               . 'list_by_user.you_must_be_registered'));
      }

      $user = $this->get('security.token_storage')->getToken()->getUser();
      $form = $this->createForm(UserType::class, $user);

      if ($request->getMethod() === 'POST') {
         $originalProfiles = array();

         foreach ($user->getProfiles() as $profile) {
            $originalProfiles[] = $profile;
         }

         $form->handleRequest($request);

         if ($form->isValid()){
            //TODO desactivate profiles, using $originalProfiles
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $this->get('session')->getFlashBag()->add('notice',
                    $this->get('translator')->trans('c.profile.'
                            . 'update.update_success'));

            return $this->redirect($this->generateUrl('cl_cyclabilite_user.profile'));
         } else {
            $this->get('session')->getFlashBag()->add('warning',
                    $this->get('translator')->trans('c.profile'
                            . '.update.invalid_form'));
         }
      }

      return $this->render('CLCyclabiliteUserBundle:Profile:view.html.twig',
            array('form' => $form->createView()));
   }
}
