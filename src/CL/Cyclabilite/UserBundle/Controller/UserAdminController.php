<?php

namespace CL\Cyclabilite\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use CL\Cyclabilite\UserBundle\Entity\User;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Marc Ducobu  <marc@champs-libres.coop>
 */
class UserAdminController extends Controller
{
    /**
     * Change the expert data for a given user :
     * - if the user is an expert, the call of this function will set him as
     *   a normal user / a non-expert
     * - if the user is not an expert, the call of this function will set him
     *  as an expert.
     *
     * @param int $userId The id of the given user.
     * @return Symfony\Component\HttpFoundation\Response The ExpertsEditAction
     * without the GET parameters
     */
    private function ChangeExpertForUser($userId)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('CLCyclabiliteUserBundle:User')->find($userId);
        if($user) {
            $user->setExpert(! $user->isExpert());
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice',
                $this->get('translator')->trans('Experts list updated!'));
        } else {
            $this->get('session')->getFlashBag()->add('warning',
                $this->get('translator')->trans('No user with this id!'));
        }
        return $this->redirectToRoute('cl_cyclabilite_user.admin_experts_edit');
    }

    /**
     * Display the users in two list :
     * - the experts
     * - and the non-experts
     * and allow to set an user as expert / non-expert.
     *
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function ExpertsEditAction(Request $request)
    {
        if($request->query->has('changeForUserId')) {
            return $this->ChangeExpertForUser($request->query->get('changeForUserId'));
        }

        $em = $this->getDoctrine()->getManager();
        $experts = $em->getRepository('CLCyclabiliteUserBundle:User')->findBy(
            array('isExpert' => true),
            array('username' => 'ASC')
        );

        $nonExperts = $em->getRepository('CLCyclabiliteUserBundle:User')->findBy(
            array('isExpert' => false),
            array('username' => 'ASC')
        );

        return $this->render('CLCyclabiliteUserBundle:UserAdmin:editExperts.html.twig', array(
            'experts' => $experts,
            'nonExperts' => $nonExperts
        ));
    }
}
