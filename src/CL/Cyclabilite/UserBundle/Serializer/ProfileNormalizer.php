<?php

namespace CL\Cyclabilite\UserBundle\Serializer;

use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Debug\Exception\ContextErrorException;
use CL\Cyclabilite\UserBundle\Entity\Profile;

/**
 * Normalize profile users into array
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class ProfileNormalizer implements NormalizerInterface
{
   
   /**
    * profiles defined in config.yml
    * 
    * @var array
    */
   private $profiles;
   
   public function __construct(array $profiles)
   {
      $this->profiles = $profiles;
   }
   
   /**
    * 
    * @param Profile $object
    * @param string $format only 'json' implemented
    * @param array $context only 'by_user' is implemented
    */
   public function normalize($object, $format = null, array $context = array())
   {
      $response = array();
      
      $response['daily_ride'] = $object->getDailyRide();
      $response['rapidity_vs_security'] = $object->getRapidityVsSecurity();
      try {
         $response['rapidity_vs_security_label'] = $this->profiles[$object->getRapidityVsSecurity()]['label'];
      } catch (\Exception $e) {
         throw new \Exception('the value '
         . $object->getRapidityVsSecurity() . ' is not configured. '
         . 'See cl_cyclabilite_user.profiles in configuration file.', 0, $e);
      }

      if (in_array('by_user', $context)) {
         $response['id'] = $object->getId();
         $response['label'] = $object->getLabel();
         $response['user']['id'] = $object->getUser()->getId();
         
         
      } 
      
      return $response;
   }

   public function supportsNormalization($data, $format = null)
   {
      return $format === 'json' && $data instanceof Profile;
   }

}
