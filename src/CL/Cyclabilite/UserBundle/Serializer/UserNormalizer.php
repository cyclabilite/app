<?php

namespace CL\Cyclabilite\UserBundle\Serializer;

use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use CL\Cyclabilite\UserBundle\Entity\User;

/**
 * Normalize user infos into array
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class UserNormalizer implements NormalizerInterface, ContainerAwareInterface
{
   /**
    *
    * @var \Symfony\Component\DependencyInjection\ContainerInterface
    */
   private $container;

   /**
    *
    * @param User $object
    * @param string $format only 'json' implemented
    * @param array $context only 'by_user' is implemented
    */
   public function normalize($object, $format = null, array $context = array())
   {
      $response = array();

      $response['id'] = $object->getId();
      $response['username'] = $object->getUsername();

      if (in_array('by_user', $context)) {


      } else {

      }

      return $response;
   }

   public function supportsNormalization($data, $format = null)
   {
      return $format === 'json' && $data instanceof User;
   }

   public function setContainer(\Symfony\Component\DependencyInjection\ContainerInterface $container = null)
   {
      $this->container = $container;
   }

}
