<?php

namespace CL\Cyclabilite\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class CLCyclabiliteUserBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
