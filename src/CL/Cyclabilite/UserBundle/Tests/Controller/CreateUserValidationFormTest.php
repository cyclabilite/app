<?php

namespace CL\Cyclabilite\UserBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Test the validation of information submitted by the registration form.
 *
 * @author julien.fastre@champs-libres.coop
 */
class CreateUserValidationFormsTest extends WebTestCase
{

   private static $form;

   private static $client;

   public static function setUpBeforeClass()
   {
      echo("\ntest CreateUserValidationForm\n");

      static::$client = static::createClient();
      static::$client->restart();

      //get persona assertion and send them to the server
      // TODO LOGIN
      static::$client->request('GET', '/persona_login_check', array(
         'assertion' => Helper::getPersonaAssertion(
                  Helper::getPersonaTestUser(true)
               )
      ));


      //get the registration form
      try {
         $form = static::$client->request('GET', '/register')
               ->selectButton(CreateUserControllerTest::BUTTON_SUBMIT_ID)
               ->form();

         $form[CreateUserControllerTest::INPUT_PROFILE_LABEL_NAME] = 'test profile CreateUserValidationForm one';

         $rapidityVsSecurityValue = CreateUserControllerTest::getRandomProfile();
         $form[CreateUserControllerTest::SELECT_PROFILE_RAPIDITY_VS_SECURITY_NAME]
               = $rapidityVsSecurityValue['value'];



         static::$form = $form;
      } catch (\Exception $e) {
         var_dump(static::$client->getResponse()->getContent());
         throw $e;
      }
   }

   public function testLabelEmpty()
   {
      $form = clone(static::$form);

      $form[CreateUserControllerTest::INPUT_LABEL_NAME] = '';

      static::$client->submit($form);

      $this->assertFalse(static::$client->getResponse()->isRedirect());
      $this->assertEquals(
              1,
              static::$client->getCrawler()->filter('.error.label')->count()
            );
   }

   public function testProfileLabelEmpty()
   {
      $form = clone(static::$form);

      $form[CreateUserControllerTest::INPUT_PROFILE_LABEL_NAME] = '';
      static::$client->submit($form);

      $this->assertFalse(static::$client->getResponse()->isRedirect());

   }

   public function testProfileUserWithoutProfile()
   {
      $form = clone(static::$form);

      unset($form[CreateUserControllerTest::INPUT_PROFILE_DAILY_RIDE_NAME]);
      unset($form[CreateUserControllerTest::INPUT_PROFILE_LABEL_NAME]);
      unset($form[CreateUserControllerTest::SELECT_PROFILE_RAPIDITY_VS_SECURITY_NAME]);

      static::$client->submit($form);

      $this->assertFalse(static::$client->getResponse()->isRedirect());
   }

   public function testLabelLengthLowerThan2Letters()
   {
      $form = clone(static::$form);

      $form[CreateUserControllerTest::INPUT_LABEL_NAME] = 'ab';

      static::$client->submit($form);

      $this->assertFalse(static::$client->getResponse()->isRedirect());
      $this->assertEquals(
              1,
              static::$client->getCrawler()->filter('.error.label')->count()
            );
   }

   public function testLabelMoreThan60Letters()
   {
      $form = clone(static::$form);

      $longString = ''
            . 'abcdefghijklmnopqrstuvwxyz '
            . 'abcdefghijklmnopqrstuvwxyz '
            . 'abcdefghijklmnopqrstuvwxyz ';

      $this->assertGreaterThan(60, strlen($longString));

      $form[CreateUserControllerTest::INPUT_LABEL_NAME] = $longString;

      static::$client->submit($form);

      $this->assertFalse(static::$client->getResponse()->isRedirect());
      $this->assertEquals(
              1,
              static::$client->getCrawler()->filter('.error.label')->count()
            );
   }

}
