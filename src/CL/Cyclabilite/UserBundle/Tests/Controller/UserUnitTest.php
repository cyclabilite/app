<?php

namespace CL\Cyclabilite\UserBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use CL\Cyclabilite\UserBundle\Entity\User;
use CL\Cyclabilite\UserBundle\Entity\Profile;

class UserUnitTest extends WebTestCase
{

   public function setUp()
   {
      $user = new User('julien.fastre@champs-libres.coop');
      $user->setBirthYear(1980)
            ->setLabel('Test ')
            ->setGender(User::GENDER_FEMALE);

      $profile = new Profile();
      $profile->setLabel('test')
            ->setRapidityVsSecurity(10)
            ->setDailyRide(true);

      $this->profile = $profile;

      $user->addProfile($profile);

      $this->user = $user;

      $kernel = static::createKernel();
      $kernel->boot();

      $this->validator = $kernel->getContainer()->get('validator');
   }
   
   /**
    * Test the profile's user is now the user's owner
    */
   public function testProfileConsistency()
   {
      $userFromProfile = $this->profile->getUser();
      
      $this->assertEquals($userFromProfile->getUsername(), $this->user->getUsername());
   }
   
   public function testRemoveProfiles()
   {
      $user = $this->getClonedUser();
      
      $this->assertGreaterThan(0, count($user->getProfiles()));
      
      $profiles = $user->getProfiles();
      
      foreach($profiles as $profile) {
         $user->removeProfile($profile);
      }
      
      $this->assertEquals(0, count($user->getProfiles()));
   }
   
   // !!!!!!!!!!!!!!!!!!!!!!!!!!!
   // 
   //       validation test
   //
   // !!!!!!!!!!!!!!!!!!!!!!!!!!!!

   public function testValidUser()
   {
      $this->assertEquals(0, $this->countErrors($this->user));
   }

   public function testValidProfile()
   {
      $this->assertEquals(0, $this->countErrors($this->profile));
   }

   public function testUserLabelBlank()
   {
      $user = $this->getClonedUser();

      $user->setLabel(null);

      $this->assertEquals(1, $this->countErrors($user));
   }

   public function testUserLabelTooLong()
   {
      $user = $this->getClonedUser();

      $string = 'abcdefghijklmnopqrstuvwxyz';
      $tooLongLabel = $string . $string . $string . $string;

      $user->setLabel($tooLongLabel);

      $this->assertEquals(1, $this->countErrors($user));
   }
   
   public function testUserWithoutProfile()
   {
      $user = $this->getClonedUser();
      
      $profiles = $user->getProfiles();
      
      foreach ($profiles as $profile) {
         $user->removeProfile($profile);
      }
      
      $this->assertEquals(1, $this->countErrors($user));
   }

   public function testProfileLabelBlank()
   {
      $profile = clone $this->profile;

      $profile->setLabel(null);

      $this->assertEquals(1, $this->countErrors($profile));

      return $profile;
   }

   /**
    * 
    * @depends testProfileLabelBlank
    * @param Profile $profile
    */
   public function testProfileLabelBlankOnUser($profile)
   {
      $user = $this->getClonedUser();

      $user->addProfile($profile);

      $this->assertEquals(1, $this->countErrors($user));
   }
   
   
   // !!!!!!!!!!!!!!!!!!!!!!
   //
   //    Helpers
   //
   // !!!!!!!!!!!!!!!!!!!!!!!!

   /**
    * 
    * @return User
    */
   private function getClonedUser()
   {
      $user = clone $this->user;

      return $user;
   }

   /**
    * 
    * @param mixed $entity
    * @return int
    */
   private function countErrors($entity)
   {
      $errors = $this->validator->validate($entity);

      return $errors->count();
   }

}
