<?php

namespace CL\Cyclabilite\UserBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use CL\Cyclabilite\UserBundle\Tests\Helper\AccessHelper;

class UserAdminControllerTest extends WebTestCase
{
    /**
     * Test if the admin user has access to the page
     */
    public function testAccess()
    {
        $client = static::createClient();
        $this->assertEquals(AccessHelper::getStatusCode($client, '/admin/experts', AccessHelper::ADMIN_USER), 200);
        
        $client = static::createClient();
        $this->assertEquals(AccessHelper::getStatusCode($client, '/admin/experts', AccessHelper::UNLOGGED_USER), 401);
        
        $client = static::createClient();
        $kernel = static::createKernel();
        $this->assertEquals(
            AccessHelper::getStatusCode($client, '/admin/experts', AccessHelper::NORMAL_USER, $kernel),
            401
        );
    }

    /**
     * Check if the user is in the good list (ul#experts-list or
     * ul#non-experts-list) regarding to the data $user->isExpert()
     *
     * @param CL\Cyclabilite\UserBundle\Entity\User The user
     */
    private function checkUserInGoodList($user)
    {
        self::bootKernel();
        
        $idList = 'experts-list';
        if (! $user->isExpert()) {
            $idList = 'non-experts-list';
        }

        $client = static::createClient();
        $client->restart();
        $crawler = $client->request(
            'GET',
            '/admin/experts',
            array(),
            array(),
            array(
                'PHP_AUTH_USER' => 'admin',
                'PHP_AUTH_PW' => $client->getContainer()->getParameter('admin_pwd')));
        $this->assertEquals(
            1,
            $crawler->filter('ul#'.$idList.':contains("'.$user->getLabel().'")')->count());
    }

    /**
    * For a given user, change it isExpert parameter using the admin
    * interfacte (route /admin/experts?changeForUserId).
    *
    * @param CL\Cyclabilite\UserBundle\Entity\User The given user
     */
    private function changeUserExpert($user)
    {
        self::bootKernel();
        $client = static::createClient();
        $client->restart();
        $crawler = $client->request(
            'GET',
            '/admin/experts',
            array('changeForUserId' => $user->getId()),
            array(),
            array(
                'PHP_AUTH_USER' => 'admin',
                'PHP_AUTH_PW' => $client->getContainer()->getParameter('admin_pwd')
            )
        );
    }

    /**
     * Test the change of the expert / non-expert parameter of a random selected
     * user. The change is made twice :
     * - expert > non-expert > expert
     * - or non-expert > expert > non-expert
     * for testing the two possibility.
     */
    public function testChangeExpert()
    {
        self::bootKernel();
        $em = $this->em = static::$kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $users = $em->getRepository('CLCyclabiliteUserBundle:User')->findAll();
        $user = $users[array_rand($users)];

        $this->checkUserInGoodList($user);
        $this->changeUserExpert($user);

        $userUpdated = $em->getRepository('CLCyclabiliteUserBundle:User')->find($user->getId());
        $this->assertEquals($user->isExpert(), ! $userUpdated->isExpert());

        $this->checkUserInGoodList($userUpdated);
        $this->changeUserExpert($userUpdated);
        
        $em->refresh($userUpdated);

        $this->checkUserInGoodList($userUpdated);
        $this->assertEquals($user->isExpert(), $userUpdated->isExpert());
    }
}
