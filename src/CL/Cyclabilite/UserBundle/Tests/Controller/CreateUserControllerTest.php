<?php

namespace CL\Cyclabilite\UserBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Test the creation of user
 *
 * @author julien.fastre@champs-libres.coop
 */
class CreateUserControllerTest extends WebTestCase
{
   private static $createdUserLabel;


   public function testUserFirstLogin()
   {
      echo("\n Test CreateUser \n");

      $client = static::createClient();
      $client->restart();

      /* $client->request('GET', '/persona_login_check', array(
          'assertion' => Helper::getPersonaAssertion(static::$arrayCreatedUser)
      )); */

      $response = $client->getResponse()->getContent();

      $responseDecoded = json_decode($response);

      $this->assertContains('/register', $responseDecoded->goTo);

      return $client;
   }


   const INPUT_LABEL_ID = 'input#user_profile_label';
   const SELECT_GENDER_ID = 'select#user_profile_gender';
   const SELECT_AGE_ID = 'select#user_profile_birthYear';
   const INPUT_PROFILE_LABEL_ID = 'input#user_profile_activeProfiles_0_label';
   const INPUT_PROFILE_DAILY_RIDE_ID = 'input#user_profile_activeProfiles_0_dailyRide_0';
   const SELECT_PROFILE_RAPIDITY_VS_SECURITY_ID = 'select#user_profile_activeProfiles_0_rapidityVsSecurity';

   const INPUT_LABEL_NAME = 'user_profile[label]';
   const SELECT_GENDER_NAME = 'user_profile[gender]';
   const SELECT_AGE_NAME = 'user_profile[birthYear]';
   const INPUT_PROFILE_LABEL_NAME = 'user_profile[activeProfiles][0][label]';
   const INPUT_PROFILE_DAILY_RIDE_NAME = 'user_profile[activeProfiles][0][dailyRide]';
   const SELECT_PROFILE_RAPIDITY_VS_SECURITY_NAME = 'user_profile[activeProfiles][0][rapidityVsSecurity]';


   const BUTTON_SUBMIT_ID = 'user_profile_submitbutton';

   /**
    * @depends testUserFirstLogin
    */
   public function testRegisterPage($client)
   {
      $crawler = $client->request('GET', '/register');


      //test form has input with exepected ids
      $this->assertEquals(1,
              $crawler->filter(self::INPUT_LABEL_ID)->count(),
              "find " . self::INPUT_LABEL_ID);


      $this->assertEquals(1,
              $crawler->filter(self::SELECT_AGE_ID)
                      ->count(),
              'find ' . self::SELECT_AGE_ID);

      $this->assertEquals(1,
              $crawler->filter(self::SELECT_GENDER_ID)
                      ->count(),
              'find ' . self::SELECT_GENDER_ID);

      $this->assertEquals(1,
               $crawler->filter(self::INPUT_PROFILE_LABEL_ID)
                     ->count(),
               'find '. self::INPUT_PROFILE_LABEL_ID);

      $this->assertEquals(1,
               $crawler->filter(self::INPUT_PROFILE_DAILY_RIDE_ID)
                  ->count()
            );

      $this->assertEquals(1,
               $crawler->filter(self::SELECT_PROFILE_RAPIDITY_VS_SECURITY_ID)
                  ->count()
            );



      $this->assertEquals(self::INPUT_LABEL_NAME,
              $crawler->filter(self::INPUT_LABEL_ID)->attr('name')
              , "test has select 'label' with appropriate id and appropriate name");

      $this->assertEquals(self::SELECT_AGE_NAME,
              $crawler->filter(self::SELECT_AGE_ID)->attr('name'),
              "test has select 'age' with appropriate id and name");

      $this->assertEquals(self::SELECT_GENDER_NAME,
              $crawler->filter(self::SELECT_GENDER_ID)->attr('name'),
              "test has select 'gender' with appropriate id and name");

      $this->assertEquals(self::INPUT_PROFILE_LABEL_NAME,
               $crawler->filter(self::INPUT_PROFILE_LABEL_ID)->attr('name'));

      $this->assertEquals(self::INPUT_PROFILE_DAILY_RIDE_NAME,
               $crawler->filter(self::INPUT_PROFILE_DAILY_RIDE_ID)->attr('name'));

      $this->assertEquals(self::SELECT_PROFILE_RAPIDITY_VS_SECURITY_NAME,
               $crawler->filter(self::SELECT_PROFILE_RAPIDITY_VS_SECURITY_ID)->attr('name'));

      return $client;
   }

   /**
    *
    * @depends testRegisterPage
    */
   public function testRegisterFormResponse($client)
   {
      $crawler = $client->getCrawler();

      $form = $crawler->selectButton(self::BUTTON_SUBMIT_ID)->form();


      static::$createdUserLabel = static::$arrayCreatedUser['personaId'] . ' label';

      $form[self::INPUT_LABEL_NAME] = static::$createdUserLabel;


      //fill one profile
      $form[self::INPUT_PROFILE_LABEL_NAME] = 'profile test one';
      $form[self::INPUT_PROFILE_DAILY_RIDE_NAME] = 0;

      $rapidityVsSecurityValue = self::getRandomProfile();

      $form[self::SELECT_PROFILE_RAPIDITY_VS_SECURITY_NAME] =
            $rapidityVsSecurityValue['value'];

      $client->submit($form);

      $this->assertTrue($client->getResponse()
                      ->isRedirect('/registration_confirmed'));

      return $client;
   }

   /**
    *
    * @depends testRegisterFormResponse
    */
   public function testRegistrationConfirmedResponse($client)
   {
      $crawler = $client->request('GET', '/registration_confirmed');

      $this->assertEquals(1, $crawler->filter('html:Contains("'
                      . static::$createdUserLabel
                      . '")')->count()
      );
   }

   /**
    *
    * @depends testRegisterFormResponse
    */
   public function testIsRegisteredInDb($client)
   {
      $kernel = static::createKernel();
      $kernel->boot();

      $em = $kernel->getContainer()->get('doctrine')->getManager();

      $user = $em->getRepository('CLCyclabiliteUserBundle:User')
              ->findOneBy(array(
          'label' => static::$createdUserLabel,
          'email' => static::$arrayCreatedUser['personaId']
      ));

      $this->assertNotNull($user);
   }

   public static function getRandomProfile()
   {
      //we need a container to get the possible parameters for rapidity_vs_security
      $kernel = static::createKernel();
      $kernel->boot();

      $rapidityVsSecurityPossibleValues = $kernel->getContainer()
            ->getParameter('cl_cyclabilite_user.profiles');

      return $rapidityVsSecurityPossibleValues
            [array_rand($rapidityVsSecurityPossibleValues)];
   }

}
