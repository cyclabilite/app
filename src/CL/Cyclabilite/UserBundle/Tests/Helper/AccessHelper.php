<?php

namespace CL\Cyclabilite\UserBundle\Tests\Helper;

/**
 * Method to check if an user has access to a given page. The user can be
 * an unlogger user, a normal user (logged) or an admin user.
 */
class AccessHelper
{
    /* The types of the users */
    const UNLOGGED_USER = 'unlogged';
    const NORMAL_USER = 'normal';
    const ADMIN_USER = 'admin';

    /**
     * Return the statusCode of a page for a given user type
     *
     * @param Symfony\Component\BrowserKit\Client $client The client
     * @param String $path The path of the page
     * @param $userType The user type : AccessHelper::UNLOGGED_USER or AccessHelper::NORMAL_USER
     * or AccessHelper::ADMIN_USER
     * @param Symfony\Component\HttpKernel\KernelInterface $kernel The kernel (only needed for NORMAL_USER)
     * @return Integer The statusCode
     */
    public static function getStatusCode($client, $path, $userType, $kernel = null)
    {
        $serverParameters = array();
    
        if ($userType == self::ADMIN_USER) {
            $serverParameters =
                array('PHP_AUTH_USER' => 'admin', 'PHP_AUTH_PW' => $client->getContainer()->getParameter('admin_pwd'));
        } elseif ($userType == self::NORMAL_USER) {
            $kernel->boot();
            $container = $kernel->getContainer();
            LoginHelper::authenticateClient($client, $container);
        }
        
        $crawler = $client->request('GET', $path, array(), array(), $serverParameters);
        return $client->getResponse()->getStatusCode();
    }
}
