<?php

namespace CL\Cyclabilite\UserBundle\Security;

use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * Description of TestUserProvider
 *
 * @author julien
 */
class TestUserProvider  implements UserProviderInterface {
    
    
    public function loadUserByUsername($username) {
        
    }

    public function refreshUser(UserInterface $user) {
        
    }

    public function supportsClass($class) {
        
    }

}
