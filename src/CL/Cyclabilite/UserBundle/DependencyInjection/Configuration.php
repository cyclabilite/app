<?php

namespace CL\Cyclabilite\UserBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{

   /**
    * {@inheritDoc}
    */
   public function getConfigTreeBuilder()
   {
      $treeBuilder = new TreeBuilder();
      $rootNode = $treeBuilder->root('cl_cyclabilite_user');

      $rootNode->children()
              ->arrayNode('profiles')
                  ->requiresAtLeastOneElement()
                  ->useAttributeAsKey('value', false)
                  ->prototype('array')
                     ->children()
                        ->integerNode('value')
                           ->isRequired()
                           ->end()
                        ->scalarNode('label')
                           ->isRequired()
                           ->end()
                     ->end()
                  ->end()
               ->end()
              ->end()
              ;

      return $treeBuilder;
   }

}
