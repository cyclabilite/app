<?php

namespace CL\Cyclabilite\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *
 */
class Profile
{

   /**
    * @var integer
    */
   private $id;

   /**
    * @var string
    */
   private $label;

   /**
    *
    * @var \CL\Cyclabilite\UserBundle\Entity\User
    */
   private $user;

   /**
    * @var boolean
    */
   private $dailyRide;

   /**
    * @var integer
    */
   private $rapidityVsSecurity = 0;

   /**
    *
    * @var boolean
    */
   private $active = true;

   /**
    * Get id
    *
    * @return integer
    */
   public function getId()
   {
      return $this->id;
   }

   /**
    * Set label
    *
    * @param string $label
    * @return VoteUsage
    */
   public function setLabel($label)
   {
      $this->label = $label;

      return $this;
   }

   /**
    * Get label
    *
    * @return string
    */
   public function getLabel()
   {
      return $this->label;
   }

   public function __toString()
   {
      return $this->getLabel();
   }

   /**
    *
    * @param \CL\Cyclabilite\UserBundle\Entity\User $user
    * @return \CL\Cyclabilite\UserBundle\Entity\Profile
    */
   public function setUser(User $user)
   {
      $this->user = $user;

      return $this;
   }

   /**
    * get the user
    *
    * @return User
    */
   public function getUser()
   {
      return $this->user;
   }

   /**
    * Set dailyRide
    *
    * @param boolean $dailyRide
    * @return Profile
    */
   public function setDailyRide($dailyRide)
   {
      $this->dailyRide = $dailyRide;

      return $this;
   }

   /**
    * Get dailyRide
    *
    * @return boolean
    */
   public function getDailyRide()
   {
      return $this->dailyRide;
   }

   /**
    * Set rapidityVsSecurity
    *
    * @param integer $rapidityVsSecurity
    * @return Profile
    */
   public function setRapidityVsSecurity($rapidityVsSecurity)
   {
      $this->rapidityVsSecurity = $rapidityVsSecurity;

      return $this;
   }

   /**
    * Get rapidityVsSecurity
    *
    * @return integer
    */
   public function getRapidityVsSecurity()
   {
      return $this->rapidityVsSecurity;
   }

   /**
    *
    */
   public function desactivate()
   {
      $this->active = false;
   }

   public function isActive()
   {
      return $this->active;
   }

}
