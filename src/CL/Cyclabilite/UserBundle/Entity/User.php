<?php

namespace CL\Cyclabilite\UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\Common\Collections\Criteria;

/**
 * Description of User
 *
 * @author julien
 */
class User extends BaseUser implements \Serializable
{
   /**
    * @var \DateTime
    */
   private $creationDate;


   /**
    * @var ArrayCollection
    */
   private $profiles;


   /**
    * @var string[]
    * TODO
*   private $roles = array('ROLE_USER');
   */

   /**
    * one of the string contained in the constants User::GENDER_MALE or
    * User::GENDER_FEMALE
    *
    * @internal the default value is 'female' and is set by __construct method.
    *
    * @var string
    */
   private $gender;

   /**
    * The vote of an expert user can be more important (have more value)
    * than a normal user.
    *
    * @var boolean
    */
   private $isExpert = false;

   /**
    * the year of birth
    *
    * @var int
    */
   private $birthYear = 0;

   const GENDER_MALE = 'male';
   const GENDER_FEMALE = 'female';


    public function __construct()
    {
        parent::__construct();

        $now = new \DateTime();
        $this->setCreationDate($now);
        $this->profiles = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
       return $this->id;
    }

    public function getLabel()
    {
       return $this->username;
    }


   /**
    * Set creationDate
    *
    * @param \DateTime $creationDate
    * @return User
    */
   public function setCreationDate($creationDate)
   {
      $this->creationDate = $creationDate;

      return $this;
   }

   /**
    * Get creationDate
    *
    * @return \DateTime
    */
   public function getCreationDate()
   {
      return $this->creationDate;
   }


   /**
    * Set preferredVoteUsage
    *
    * @param \CL\Cyclabilite\UserBundle\Entity\Profile $profile
    * @return User
    */
   public function addProfile(\CL\Cyclabilite\UserBundle\Entity\Profile $profile)
   {
      $profile->setUser($this);
      $this->profiles->add($profile);
      return $this;
   }

   public function removeProfile(Profile $profile)
   {
      $this->profiles->removeElement($profile);
      $profile->desactivate();
   }

   /**
    *
    * @return ArrayCollection
    */
   public function getProfiles()
   {
      return $this->profiles;
   }

   public function getActiveProfiles()
   {
      $criteria = Criteria::create()
              ->where(Criteria::expr()->eq('active', true));

      return $this->profiles->matching($criteria);
   }

   public function addActiveProfile(Profile $profile)
   {
      $this->addProfile($profile);
   }

   public function removeActiveProfile(Profile $profile)
   {
      $this->removeProfile($profile);
   }

   public function getGender()
   {
      return $this->gender;
   }

   public function setGender($gender)
   {
      $this->gender = $gender;
      return $this;
   }

   public function isExpert()
   {
       return $this->isExpert;
   }

   public function setExpert($isExpert)
   {
        $this->isExpert = $isExpert;
        return $this;
   }

   public function setBirthYear($birthYear)
   {
      $this->birthYear = $birthYear;
      return $this;
   }

   public function getBirthYear()
   {
      return $this->birthYear;
   }

   public function serialize()
   {
      return serialize(array(
          $this->id,
          $this->email,
              // see section on salt below
              // $this->salt,
      ));
   }

   public function unserialize($serialized)
   {
      list (
              $this->id,
              $this->email,
              // see section on salt below
              // $this->salt
              ) = unserialize($serialized);
   }
}
