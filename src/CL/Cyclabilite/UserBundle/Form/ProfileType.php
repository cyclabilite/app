<?php

namespace CL\Cyclabilite\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use CL\Cyclabilite\UserBundle\Form\RapidityVsSecurity\RapidityVsSecurityType;
use CL\Cyclabilite\UserBundle\Entity\Profile;

class ProfileType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('label')
            ->add('rapidityVsSecurity', RapidityVsSecurityType::class)
            ->add('dailyRide', ChoiceType::class, array(
                'choices' => array(
                    'form.profile_type.daily' => true,
                    'form.profile_type.not_daily' => false
                ),
                'expanded' => true,
                'multiple' => false,
                'choices_as_values' => true
            ))
        ;
    }

    /**
     * @param OptionsResolver$resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' =>  Profile::class
        ));
    }
}
