<?php

namespace CL\Cyclabilite\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;


use CL\Cyclabilite\UserBundle\Entity\User;


class UserType extends AbstractType
{

   /**
    * @param FormBuilderInterface $builder
    * @param array $options
    */
   public function buildForm(FormBuilderInterface $builder, array $options)
   {
      //calculate years possibility
      $today = new \DateTime('now');
      $currentYear = (int) $today->format('Y');
      $intervals = range(5, 120);
      $years = array();

      foreach ($intervals as $interval) {
         $year = $currentYear - $interval;
         $years[$year] = $year;
      }

      $builder
         ->add('username')
         ->add('gender', ChoiceType::class, array(
            'choices' => array(
                'male' => User::GENDER_MALE,
                'female' => User::GENDER_FEMALE
            )
         ))
         ->add('birthYear', ChoiceType::class, array(
            'choices' => $years
         ))
         ->add('activeProfiles', CollectionType::class, array(
            'entry_type' => ProfileType::class,
            'allow_add' => true,
            'allow_delete' => true,
            'by_reference' => false
         ))
         ->add('submitbutton', SubmitType::class)
      ;
   }

   /**
    * @param OptionsResolverInterface $resolver
    */
   public function configureOptions(OptionsResolver $resolver)
   {
      $resolver->setDefaults(array(
          'data_class' => 'CL\Cyclabilite\UserBundle\Entity\User'
      ));
   }
}
