<?php

namespace CL\Cyclabilite\UserBundle\Form\RapidityVsSecurity;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;


/**
 *
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class RapidityVsSecurityType extends AbstractType
{
   private $profiles;

   public function __construct(array $profiles)
   {
      $this->profiles = $profiles;
   }


   public function getParent()
   {
      return ChoiceType::class;
   }

   public function configureOptions(OptionsResolver $resolver)
   {
      //distribute the "value" as index
      $choices = array();

      foreach ($this->profiles as $array) {
         $choices[$array['label']] = $array['value'];
      }

      $resolver->setDefaults(array(
         'choices' => $choices,
         'empty_data' => 'form.rapidity_vs_security.empty_value'
      ));
   }
}
