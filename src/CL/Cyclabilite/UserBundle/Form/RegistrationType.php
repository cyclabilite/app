<?php

namespace CL\Cyclabilite\UserBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use CL\Cyclabilite\UserBundle\Entity\User;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        //calculate years possibility
        $today = new \DateTime('now');
        $currentYear = (int) $today->format('Y');
        $intervals = range(5, 120);
        $years = array();

        foreach ($intervals as $interval) {
            $year = $currentYear - $interval;
            $years[$year] = $year;
        }

        $builder
            ->add(
                'email',
                EmailType::class,
                array(
                    'label' => 'form.email', 'translation_domain' => 'FOSUserBundle',
                    'attr' => array('class' => 'wide input'),
                    'label_attr' => array('class' => 'inline aligned required'),
                ))
            ->add(
                'username', null,
                array(
                    'label' => 'form.username', 'translation_domain' => 'FOSUserBundle',
                    'attr' => array('class' => 'wide input'),
                    'label_attr' => array('class' => 'inline aligned required'),
                ))
            ->add(
                'plainPassword',
                RepeatedType::class,
                array(
                    'type' => PasswordType::class,
                    'options' => array('translation_domain' => 'FOSUserBundle'),
                    'first_options' => array(
                        'label' => 'form.password',
                        'label_attr' => array('class' => 'inline aligned required'),
                        'attr' => array('class' => 'wide input')
                    ),
                    'second_options' => array(
                        'label' => 'form.password_confirmation',
                        'label_attr' => array('class' => 'inline aligned required'),
                        'attr' => array('class' => 'wide input')
                    ),
                    'invalid_message' => 'fos_user.password.mismatch',
            ))
            ->add('gender', ChoiceType::class, array(
                'choices' => array(
                    'male' => User::GENDER_MALE,
                    'female' => User::GENDER_FEMALE
                ),
                'attr' => array('class' => 'wide'),
                'label_attr' => array('class' => 'inline aligned required'),
            ))
            ->add('birthYear', ChoiceType::class, array(
                'choices' => $years,
                'attr' => array('class' => 'wide'),
                'label_attr' => array('class' => 'inline aligned required'),
            ));
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    public function getBlockPrefix()
    {
        return 'cl_cycab_user_registration';
    }
}
