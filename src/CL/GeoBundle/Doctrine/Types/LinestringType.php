<?php

namespace CL\GeoBundle\Doctrine\Types;


use Doctrine\DBAL\Types\Type; 
use Doctrine\DBAL\Platforms\AbstractPlatform;

use CL\GeoBundle\Entity\Linestring;


/**
 * A Type for Doctrine to implement the Geometry linestring type
 * implemented by Postgis on postgis+postgresql databases
 *
 * @author julien.fastre@champs-libres.coop
 */
class LinestringType extends Type {
    
    const LINE = 'linestring';
    
    /**
     *
     * @param array $fieldDeclaration
     * @param AbstractPlatform $platform
     * @return type 
     */
    public function getSqlDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return 'geometry(LINESTRING,'.  Linestring::SRID.')';
    }
    
    /**
     *
     * @param type $value
     * @param AbstractPlatform $platform
     * @return \CL\GeoBundle\Entity\Linestring
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return Linestring::fromGeoJson($value);
    }
    
    public function getName()
    {
        return self::LINE;
    }
    
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return 'ST_GeomFromGeoJSON(\''.$value->toGeoJson().'\')';
    }
    
    public function canRequireSQLConversion()
    {
        return true;
    }
    
    public function convertToPHPValueSQL($sqlExpr, $platform)
    {
        return 'ST_AsGeoJSON('.$sqlExpr.') ';
    }
    
    public function convertToDatabaseValueSQL($sqlExpr, AbstractPlatform $platform)
    {
        return $sqlExpr;
    }
    
    
    
    
    
}

