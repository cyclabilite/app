<?php

namespace CL\GeoBundle\Entity;

use \JsonSerializable;

/**
 * Description of Linestring
 *
 * @author julien.fastre@champs-libres.coop
 */
class Linestring implements JsonSerializable {
    
    private $geojson;
    
    private $geojsonArray = NULL;
    
    const SRID = '4326';
    
    private function __construct($geojson) {
        $this->geojson = $geojson;
    }
    
    public static function fromGeoJson($geojson) {
        return new Linestring($geojson);
    }
    
    public function toGeoJson() {
        return $this->geojson;
    }

    public function jsonSerialize() {
        return $this->toArrayGeoJson();
    }
    
    public function toArrayGeoJson() {
        if ($this->geojsonArray === NULL) {
            $this->decodeJson();
        }
        
        return $this->geojsonArray;   
    }
    
    private function decodeJson() {
        if ($this->geojson === NULL) {
            throw new \Exception('the geojson string should not be empty');
        }
        
        $this->geojsonArray = json_decode($this->geojson);
    }
}
