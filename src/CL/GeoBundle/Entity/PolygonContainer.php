<?php

namespace CL\GeoBundle\Entity;



/**
 * 
 *
 * @author julien.fastre@champs-libres.coop
 */
class PolygonContainer {
    
    private $stringPostgis = '';
    
    private function __construct($string) {
        $this->stringPostgis = $string;
    }
    
    public function __toString() {
        return $this->stringPostgis;
    }
    
    
}


