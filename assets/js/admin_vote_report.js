/*jshint esversion: 6*/

/* TODO
- relire le code
- option pour cacher les labels :
- utiliser segment_id et marker_id (ou center_id) à la place de marker.cyclab_index ou marker_cyclabi_id
- table center avec un bouton
- mettre en gras le segment sélectionné
*/

import geo from './geo.js';

const leaflet = require('leaflet');

let color = [
    '#009', '#309', '#609', '#909', '#C09', '#F09',
    '#030', '#330', '#630', '#930', '#C30', '#F30',
    '#0C9', '#3C9', '#6C9', '#9C9', '#CC9', '#FC9',

    '#C00', '#C03', '#C06', '#C09', '#C0C', '#C0F', //red
];

function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}

function getRandomColor() {
    return color[getRandomInt(color.length)];
}

var selection = [];
let selection_input = document.getElementById('form_selectedMarkers');
    // todo renommer en markers-selection

let id_segment_selection = document.getElementById('form_selectedSegments');
    // todo renommer en segments-selection


let icons = [];

let lines = L.featureGroup();
let centers = L.featureGroup();
let startFG = L.featureGroup();
let endFG = L.featureGroup();

let html_markers = [];

function createMarker(center, segment_id) {
    let next_index = html_markers.length;
    let icon = getIcon(next_index.toString());
    let marker = new L.Marker(center, {icon: icon});
    marker.cyclab_index = next_index;
    marker.cyclab_segment_id = segment_id;
    html_markers[next_index] = marker;
    return marker;
}

function getMaker(cyclab_index) {
    if(!(cyclab_index in html_markers)) {
        console.log('creation of html_marker');
        console.log(cyclab_index);
        return null;
    }
    return html_markers[cyclab_index];
}


function getIcon(icon_index) {
    if (!(icon_index in icons)) {
        let icon = L.divIcon({
            iconSize: [25, 25],
            html: icon_index,
            className: 'intersection_segment_label_icon'
        });
        icons[icon_index] = icon;
        icon.icon_index = icon_index;
    }

    return icons[icon_index];
}


function lonLat2latLon(coords) {
    return coords.map(x => [x[1], x[0]]);
}

function geojsonLine2PolyLine(line, options={}) {
    return L.polyline(lonLat2latLon(line.geometry.coordinates), options);
}


function getStartFromGeojsonLine(line) {
    let latLoncoords = lonLat2latLon(line.geometry.coordinates);
    let coords = latLoncoords[0];
    return L.latLng(coords);
}

function getEndFromGeojsonLine(line) {
    let latLoncoords = lonLat2latLon(line.geometry.coordinates);
    return L.latLng(latLoncoords[latLoncoords.length -1]);
}

function selectionInputChange(event) {
    let ids_string = event.target.value;
    let raw_ids = ids_string.split(',');
    let ids = raw_ids.map(x => parseInt(x, 10));
    selectIcons(ids);
    // You can use “this” to refer to the selected element.
    //if(!event.target.value) alert('Please Select One');
    //else alert('You like ' + event.target.value + ' ice cream.');
}

function updateSelection(icon_ids) {
    selection = [];

    icon_ids.forEach(function(id) {
        if(! isNaN(id)) {
            selection.push(id);
        }
    });
}

function selectIcons(icon_ids) {
    updateSelection(icon_ids);

    // set all the icon as not selected
    html_markers.forEach(function(m) {
        L.DomUtil.addClass(m._icon, 'intersection_segment_label_icon');
        L.DomUtil.removeClass(m._icon, 'intersection_segment_label_icon_selected');
    });

    icon_ids.forEach(function(id) {
        if(! isNaN(id)) {
            let marker = getMaker(id);
            if(marker) {
                L.DomUtil.addClass(marker._icon, 'intersection_segment_label_icon_selected');
                L.DomUtil.removeClass(marker._icon, 'intersection_segment_label_icon');
            } else {
                console.log('not found');
                console.log(id);
            }
        }
    });

    updateSelectedSegmentIds();
}


function getSelectedSegmentIds() {
    let segment_id_selected = [];

    selection.forEach(function(marker_id) {
        let marker = getMaker(marker_id);

        if(marker) {
            segment_id_selected.push(marker.cyclab_segment_id);
        } else {
            console.log('Marker not found for ' + marker_id);
        }
    });

    return segment_id_selected;
}

function updateSelectedSegmentIds() {
    let select_segment_ids = getSelectedSegmentIds();
    id_segment_selection.value = select_segment_ids.join(',');
}


const app = function() {
    function init(deleted_vote) {
        deleted_vote = JSON.parse(deleted_vote);

        selection_input.oninput=selectionInputChange;

        let mapFrom = L.map('map-from');
        let mapTo = L.map('map-to');

        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(mapFrom);


        let bg = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(mapTo);

        mapFrom.on('moveend', function(e) {
            if(typeof(mapFrom.getCenter()) != "undefined" && typeof(mapFrom.getZoom()) != "undefined") {
                mapTo.setZoom(mapFrom.getZoom());
                mapTo.setView(mapFrom.getCenter());
            }
        });


        let icon = {}; // A SUPPRIMER - sur segments


        let v = L.geoJSON(deleted_vote).addTo(mapFrom);
        let b = v.getBounds();

        //let test2 = L.rectangle(b, {color: "#0F0", weight: 1}).addTo(mapTo);


        // let b_05 = b.pad(0.2);
        let dataOnMapTo = false; // pour ne pas faire plein de geojson

        let northWest = b.getNorthWest();
        let northEast = b.getNorthEast();
        let southWest = b.getSouthWest();
        let southEast = b.getSouthEast();


        let northWestB = northWest.toBounds(20); // mettre 5 m de chaquecoté de la bbox du segment (10 / 2 = 5)
        let northEastB = northEast.toBounds(20);
        let southWestB = southWest.toBounds(20);
        let southEastB = southEast.toBounds(20);

        b.extend(northWestB);
        b.extend(northEastB);
        b.extend(southWestB);
        b.extend(southEastB);

        //let test = L.rectangle(b, {color: "#00F", weight: 1}).addTo(mapTo);


        mapFrom.on('moveend', function(){
            if(!dataOnMapTo)  {
                let url_get = '/segment/list/bbox.json';

                lines.addTo(mapTo);
                centers.addTo(mapTo);
                startFG.addTo(mapTo);
                endFG.addTo(mapTo);

                $.get(
                    //url_get + '?bbox=' + mapFrom.getBounds().toBBoxString(), // to do remplacer par cyclab_map.getBBoxString()
                    url_get + '?bbox=' + b.toBBoxString(),
                    function(entities) {
                        $.each(entities, function(i, entity){
                            var line = geojsonLine2PolyLine(entity, {color: getRandomColor()});
                            line.addTo(lines);

                            let center = line.getCenter();
                            let center_marker = createMarker(center, entity.properties.id);

                            center_marker.on('click', function(e) {
                                let marker = e.target;

                                if(selection.indexOf(marker.cyclab_index) != -1) {

                                    L.DomUtil.addClass(e.target._icon, 'intersection_segment_label_icon');
                                    L.DomUtil.removeClass(e.target._icon, 'intersection_segment_label_icon_selected');


                                    let pos = selection.indexOf(marker.cyclab_index);
                                    if (pos > -1) {
                                        selection.splice(pos, 1);
                                    }
                                } else {

                                    L.DomUtil.addClass(e.target._icon, 'intersection_segment_label_icon_selected');
                                    L.DomUtil.removeClass(e.target._icon, 'intersection_segment_label_icon');


                                    // $(icon).removeClass('intersection_segment_label_icon');
                                    // $(icon).addClass('intersection_segment_label_icon_selected');
                                    selection.push(marker.cyclab_index);
                                }

                                selection_input.value = selection.join(', ');
                                updateSelectedSegmentIds();
                            });
                            center_marker.addTo(centers);

                            let start = getStartFromGeojsonLine(entity);
                            let start_marker = L.circleMarker(start, {color: '#000', fillColor: '#000', fillOpacity: 1, radius: 2});

                            start_marker.addTo(startFG);

                            let end = getEndFromGeojsonLine(entity);
                            let end_marker = L.circleMarker(end, {color: '#000', fillColor: '#000', fillOpacity: 1, radius: 2});
                            end_marker.addTo(endFG);
                        });
                    });
                dataOnMapTo = true;
            }
        });


        mapFrom.fitBounds(b);

        // todo a mettre lié à un bouton
        // mapTo.removeLayer(centers);
    }

    return {init: init};
} ();

document.addEventListener('DOMContentLoaded', function() {
    var data = document.querySelector('.js-data');

    if(data) {
        var deleted_vote = data.dataset.deletedVote;
        app.init(deleted_vote);
    } else {
        console.log('Pas de data');
    }
});
