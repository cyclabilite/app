/* jslint vars: true */
/*jslint indent: 3 */
/* global define, current_user_id, current_user_label */


/* Function pr interact with the user */
var user = function($) {
   'use strict';
   var user_profile = null;
   var current_user = null;

   function init(current_user_id_arg, current_user_label_arg) {
      $('#log-out-link').click(function(e) {
         e.preventDefault();

         $.ajax({
            url:$('#log-out-link').attr('href'),
            cache: false,
            success: function(output_json) {
               ui_logout();
               user_profile = null;
               current_user = null;
            }
         });

         return false;
      });

      $('#log-in-link').colorbox({
         inline: true,
         width:'400px',
         height:'400px',
         onComplete: function() {
            $('#p_error_message').hide();
            $('#colorbox form').submit(function(e){
               e.preventDefault();
               $.ajax({
                  type: 'GET',
                  url: '/login',
                  dataType: 'html',
                  success: function(response) {
                     var response_login_form = $(response);
                     var csrf_token = response_login_form.find("input[name='_csrf_token']").val();
                     $("[name=\"_csrf_token\"]").val(csrf_token);
                     $.ajax({
                        type: $('#colorbox form').attr('method'),
                        url: $('div#colorbox form').attr('action'),
                        data: $('div#colorbox form').serialize(),
                        dataType: 'json',
                        success: function(output_json) {
                           if(output_json.success) {
                              setCurrentUser(output_json.user);
                              $.colorbox.close();
                           } else {
                              $('#p_error_message').text(output_json.message);
                              $('#p_error_message').show();
                           }
                        },
                        error: function(data) {
                           $('#p_error_message').text(data);
                           $('#p_error_message').show();
                        },
                     });
                  },
                  error: function(data) {
                     $('#p_error_message').text(data);
                     $('#p_error_message').show();
                  },
               })
               return false;
            })
         },
         onClosed: function() {
            console.log('reload page');
         }
     });

      ui_draw_menu();
   }

   function setCurrentUser(c_user) {
      current_user = c_user;
      ui_draw_menu();
   }

   function ui_login() {
      $('#see-profile-link').html(current_user.username);
      $('#menu_for_logged_user').show();
      $('#menu_for_unlogged_user').hide();
   }

   function ui_logout() {
      $('#menu_for_logged_user').hide();
      $('#menu_for_unlogged_user').show();
   }

   function ui_draw_menu() {
      if(current_user) {
         ui_login();
      } else {
         ui_logout();
      }
   }

   /*
    * Apply a callback on the user profile (if the user is logged)
    */
   function getProfile(callback){
      if(! user_profile && current_user) {
         $.ajax({
            url:'user/profiles.json',
            cache: false,
            success: function(output_json) {
               user_profile = output_json;
               callback(user_profile);
            }
         });
      } else {
         callback(user_profile);
      }
   }

   return {
      init: init,
      setCurrentUser: setCurrentUser,
      getProfile: getProfile,
   };
};

export default user;
