/* jslint vars: true */
/*jslint indent: 3 */
/* global define */


var entity = function($, L, Mustache, cyclab_map, user, geo, select2) {
   'use strict';

   var LAYER_ENTITIES = 'entities';
   var LAYER_VOTE = 'vote';

   var vote_mode = false;

   function load(entity_type, success_callback, fail_callback) {
      var url_get = entity_type + '/list/bbox.json';

      if(entity_type == 'way') {
         url_get = 'segment/list/bbox.json';
      }

      var map = cyclab_map.getMap();

      $.get(url_get + '?bbox=' + map.getBounds().toBBoxString(), // to do remplacer par cyclab_map.getBBoxString()
         function(entities) {
            var options = {
               style: function (feature) {
                  return {
                     color: cyclab_map.getColor(feature),
                     weight: 10,
                     opacity: 0.5,
                  };
                  // marche pas car c'est pour segmet et pas ways
               },
               pointToLayer: function (feature, latlng) {
                  return L.circleMarker(latlng, {color: cyclab_map.getColor(feature)});
               },
               onEachFeature: function (feature, layer) {

                  layer.off('click');
                  layer.on('click', displayPopup);
               },
               weight:8
            };

            cyclab_map.addGeoJsonLayer(LAYER_ENTITIES, entities, options);
         }
      ).done(function() {
         if (success_callback) {
            success_callback();
         }
      }).fail(function() {
         if (fail_callback) {
            fail_callback();
         }
      });
   }

   function displayPopup(e) {
      // load the details of the entity (pour results + vote)
      // afficher les résultats dnas popup
      var entity = e.target.feature.properties;
      var entity_type = entity.type;
      var layer = e.target;

      var popup_content;
      var vote_results = '';
      // TODEL var url_get = 'votes/list/' + entity_type + '/' + entity.id + '.json';
      var url_entity_get_details = 'segment/details/of/' + entity.id + '.json';

      if(entity_type == 'intersection' || entity_type == 'node') {
         url_entity_get_details = 'intersection/details/of/' + entity.id + '.json';
      }

      $.ajax({
         url: url_entity_get_details,
         cache: false,
         success: function(geojson_detailed_entity) {
            var det_entity = geojson_detailed_entity.properties;
            var votes = det_entity.votes;

            if(votes.length === 0) {
               vote_results = $('#no_vote').text();
            } else {
               $.each(votes, function(i, v) {
                  vote_results +=  Mustache.render($('#result-vote-item-list-tpl').text(), v);
               });
            }

            popup_content = Mustache.render(
               $('#result-tpl').text(), {
                  content: vote_results,
                  entity_id: entity.id,
                  entity_type: entity_type
               });

            cyclab_map.addPopup(e.latlng, popup_content, {offset:  cyclab_map.getPopupOffsetFor(entity)});

            if(entity_type == 'segment') { // todo fusioner
               $('#display-vote-form-segment-' + entity.id).click(function() {
                  displayVotePopup(det_entity, layer);
               });
            } else {
               $('#display-vote-form-node-' + entity.id).click(function() {
                  displayVotePopup(det_entity, layer);
               });
            }
         }
      });
   }

   function displayVotePopup(det_entity, layer) {
      user.getProfile(function(user_profile) {
         var popup_content;
         var i;
         var timestamp;
         var vote_profile = '';
         var now = new Date();
         var now_hours = now.getHours();
         var now_minutes = now.getMinutes();
         var id_vote_form;
         var selector_vote_form;
         var template_args;
         var path_options = '<option value=""></option>';

         vote_mode = true;

         if (user_profile) {
            for (i=0;i<user_profile.length;i++) {
               vote_profile = vote_profile + '<option value="' + user_profile[i].id + '" selected="selected">' + user_profile[i].label + '</option>';
            }

            timestamp = new Date().getTime();
            id_vote_form = 'vote-form-' + timestamp;
            selector_vote_form = '#' + id_vote_form;

            if (now_hours <= 9) {
               now_hours = '0' + now_hours;
            }

            if(now_minutes <= 9) {
               now_minutes = '0' + now_minutes;
            }

            template_args = {
               id_vote_form: id_vote_form,
               id: det_entity.id,
               vote_profile: vote_profile,
               entity: det_entity.type,
               now_hours: now_hours,
               now_minutes: now_minutes,
               now_date: now.getDate(),
               now_month: now.getMonth(),
               now_full_year: now.getFullYear()
            };

            if(det_entity.type == 'node' || det_entity.type == 'intersection') {

               $.each(det_entity.segments, function(i, s) {
                  path_options = path_options + '<option value="' +
                     s.properties.id + '">' + i + '</option>';
               });

               template_args.vote_intersection_direction_from = path_options;
               template_args.vote_intersection_direction_to = path_options;
               // voir remaruqe RMQ_GROS_BORDEL dans VoteController à
               // propos de ce nom. On travaille avec des intersection_path
               // même si l'object sur lequel on travaill est une intersection
               template_args.entity = 'intersection_path';
            }
            popup_content = Mustache.render($('#vote-form-tpl').text(), template_args);
         } else {
            popup_content = $('#please_log_in').text();
         }

         if(det_entity.type == 'segment') {
            var lat_lng_A, lat_lng_B;

            var selected_way_segment_layers_number = 0;
            var selected_way_segments_nbr = det_entity.way.properties.segments.features.length;
            var way_segment_layers = [];

            var options = {
               onEachFeature: (function (feature, layer) {
                  layer.feature.properties.selected = true;
                  selected_way_segment_layers_number = selected_way_segment_layers_number + 1;

                  var onClickAction = function(e) {
                     var layer = e.target;

                     if(e.type=='click' && ! layer.feature.properties.selected) {
                        layer.feature.properties.selected = ! layer.feature.properties.selected;
                        selected_way_segment_layers_number = selected_way_segment_layers_number + 1;
                     }

                     else if (e.type=='contextmenu' && layer.feature.properties.selected) {
                        layer.feature.properties.selected = ! layer.feature.properties.selected;
                        selected_way_segment_layers_number = selected_way_segment_layers_number - 1;
                     }

                     if(selected_way_segment_layers_number === selected_way_segments_nbr) {
                        $('.vote_way').show();
                        $('.vote_way_segment').hide();
                     } else {
                        $('.vote_way').hide();
                        $('.vote_way_segment').show();
                     }

                     for(i=0; i < way_segment_layers.length; i++) {
                        if(way_segment_layers[i].feature.properties.selected) {
                           way_segment_layers[i].setStyle({
                              weight: 10,
                              opacity: 0.5,
                              color: cyclab_map.getColor(way_segment_layers[i].feature)
                           });
                        } else {
                           way_segment_layers[i].setStyle({
                              weight: 10,
                              color: cyclab_map.unselected_color,
                              opacity: 0.5,
                           });
                        }
                     }
                  };

                  way_segment_layers.push(layer);


                  layer.on({
                     contextmenu: function(e) {
                        onClickAction(e);
                     },
                     click: function(e) {
                        onClickAction(e);
                     }
                  });
               }),
               style: function (feature) {
                  return {
                     weight: 10,
                     opacity: 0.5,
                     color: cyclab_map.getColor(feature)
                  };
               },
            };

            var way_segments = det_entity.way.properties.segments.features;

            /* Calcul de point debut du way et fin */
            var way_segments_0 = way_segments[0];
            var way_segments_n = way_segments[0];

            way_segments.forEach(function(s) {
               if(way_segments_0.properties.id > s.properties.id) {
                  way_segments_0 = s;
               }

               if(way_segments_n.properties.id < s.properties.id) {
                  way_segments_n = s;
               }
            });

            lat_lng_A = geo.geojson.getLatLngFromPoint(way_segments_0.properties.node_start);
            lat_lng_B = geo.geojson.getLatLngFromPoint(way_segments_n.properties.node_end);

            cyclab_map.hideLayer(LAYER_ENTITIES);

            cyclab_map.addGeoJsonLayer(LAYER_VOTE, det_entity.way.properties.segments, options);

            cyclab_map.updatePopupContent(popup_content); // TODO AMELIORER

            cyclab_map.showMarker('A', lat_lng_A);
            cyclab_map.showMarker('B', lat_lng_B);

            $(selector_vote_form + '-direction-from-to-div').hide();

            $(selector_vote_form + '-direction').select2();
            $(selector_vote_form + '-direction-from').select2();
            $(selector_vote_form + '-direction-to').select2();
            $(selector_vote_form + '-profile').select2();


            $('.vote_way_segment').hide();
            $('.vote_way').show();

            $(selector_vote_form).submit(function() {
                  var selected_segments = [];
                  var segments_features = det_entity.way.properties.segments.features;

                  for(i=0; i < segments_features.length; i++) {
                     if(segments_features[i].properties.selected) {
                        selected_segments.push(segments_features[i].properties.id);
                     }
                  }

                  addVoteFor(selected_segments, 'segment', selector_vote_form);

                  return false;
               });

         } else { // intersection
            var distance_position = {15: 80, 16: 40, 17: 20, 18: 10, 19: 4, 20: 2, 21: 1, 22: 1}; // TODO MOVe
            var segments = det_entity.segments;
            var geojson_to_display = [];
            var icon = {};

            cyclab_map.hideLayer(LAYER_ENTITIES);


            $.each(segments, function(index, segment) {
               var position = geo.getPosition(segment.geometry, layer.feature.geometry.coordinates, distance_position[cyclab_map.getZoom()]);


               if (!(index in icon)) {
                  icon[index]  =  L.divIcon({
                     iconSize: new L.Point(25, 25),
                     html: index,
                     className: 'intersection_segment_label_icon'
                  });
               }

               geojson_to_display.push(segment);

               geojson_to_display.push({
                  'type': 'Feature',
                  'geometry': {
                     'type': 'Point',
                     'coordinates': [position.lng, position.lat]
                  },
                  'properties': {
                     'label': index
                  }
               });

            });

            // function called when creation of a marker
            var pointToLayer = function (feature, latlng) {
               return  L.marker(latlng,{icon: icon[feature.properties.label]});
            };

            cyclab_map.addGeoJsonLayer(
               LAYER_VOTE,
               geojson_to_display,
               {pointToLayer :  pointToLayer, style: {color: cyclab_map.unselected_color}});

            cyclab_map.updatePopupContent(popup_content); // TODO AMELIORER

            $(selector_vote_form + '-direction-div').hide();

            $(selector_vote_form + '-direction').select2();
            $(selector_vote_form + '-direction-from').select2();
            $(selector_vote_form + '-direction-to').select2();
            $(selector_vote_form + '-profile').select2();

            $(selector_vote_form).submit(function() {
               addVoteFor([ det_entity.id ], det_entity.type, selector_vote_form);
               return false;
            });
         }
      });
   }


   /*
    * Add some vote for a given list of entities id
    * @param {int list} entity_id_list The id list of the entity for which we add the vote
    * @param {String} entity_type The type of the entity related to the form (way, segment or intersection)
    * @param {String} selector_voting_form The selector of the voting form
    * @param TODO : target
    * @return No return
    */
   function addVoteFor(entity_id_list, entity_type, selector_voting_form) {
      var form_name = 'vote_' + entity_type;
      if(entity_type == 'intersection') {
         form_name = 'vote_intersection_path';
      }
      if($(selector_voting_form + ' input[name="'+ form_name + '[value]"]:checked').length === 1) {
         if(entity_id_list.length === 0) {
            $(selector_voting_form).hide();
            $(selector_voting_form + '-success').show();
            setTimeout(stopVote,2500);
         } else {
            var entity_id = entity_id_list.pop();
            var url = 'votes/add/' + entity_type  +'/'  + entity_id + '.json';

            $.ajax({
               type: 'POST',
               url: url,
               data: $(selector_voting_form).serialize(), // serializes the form's elements.
               success: function(data) {
                  var feature = data.feature;
                  var vote = data.vote;

                  cyclab_map.updateLayer(
                     LAYER_ENTITIES,
                     function(l) {
                        if(l.feature.properties.id == entity_id) {
                           return true;
                        }
                     },
                     function(l) {
                        l.feature.properties.vote_average = feature.properties.vote_average;
                        l.feature.properties.vote_number = feature.properties.vote_number;
                        l.setStyle({
                           color: cyclab_map.getColor(l.feature),
                           //weight: 10,
                           //opacity: 0.5
                        });
                     });

                  // TODO mettre a jour la couleur du layer segemnt
                  if(vote.id) {
                     addVoteFor(entity_id_list, entity_type, selector_voting_form);// continue to vote in the list
                  } else {
                     $(selector_voting_form + '-error-default').show();
                     $(selector_voting_form + '-error-custom').hide();
                     setTimeout(stopVote,2500);
                  }
               },
               error: function(response) {
                  $(selector_voting_form + '-error-custom').text(response.responseJSON[0]);
                  $(selector_voting_form + '-error-custom').show();
               }
            });
         }
      } else {
         $('.empty-vote-error').show();
      }
   }

   function stopVote() {
      cyclab_map.showLayer(LAYER_ENTITIES);
      cyclab_map.hideLayer(LAYER_VOTE);
      cyclab_map.closePopup();

      vote_mode = false;

      cyclab_map.hideMarker('A');
      cyclab_map.hideMarker('B');
   }

   /*
    * Display the buttons regarding to the zoom level and the entity selection
    */
   function updateActionButtons(map) {
      if (map.getZoom() < cyclab_map.display_entity_min_zoom_level) {
         $('#zoom-message').css('display', 'inline');


      } else {
         $('#zoom-message').hide();
      }
   }

   function loadData() {
      if(cyclab_map.getMap().getZoom() >= cyclab_map.display_entity_min_zoom_level
         && ! vote_mode) {
         $('#loading-message').css('display', 'inline');

         if($('#vote-select').val() == 'segments') {
            load('way', loadingSuccess, loadingError);
         } else {
            load('intersection', loadingSuccess, loadingError);
         }
      }
   }

   function loadingSuccess() {
      $('#loading-message').hide();
   }

   function loadingError() {
      $('#loading-message').hide();
      $('#error-message').css('display', 'inline');

      setTimeout(function() { $('#error-message').hide(); },5000);
   }

   function init() {
      $('#vote-select').select2({minimumResultsForSearch: 99, width: '7.5em'});

      $('#vote-select').on('change', function () {
         loadData();
      });

      cyclab_map.addEvent('zoomend', function(e) {
         updateActionButtons(e.target);
      });

      cyclab_map.addEvent('moveend', function() {
         loadData();
      });

      cyclab_map.addEvent('popupclose', function(e) {
         stopVote();
         return false;
      });

      updateActionButtons(cyclab_map.getMap());
      loadData();
   }

   return {
      load: load,
      stopVote: stopVote,
      init: init,
   };
};

export default entity;
