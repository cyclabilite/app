/*jshint esversion: 6*/

const leaflet = require('leaflet');


document.addEventListener('DOMContentLoaded', function() {
    var maps_containers = document.getElementsByClassName('map-container');

    [].forEach.call(maps_containers, function(maps_container) {
        var map_from_el = maps_container.getElementsByClassName('map-from')[0];
        var map_to_el = maps_container.getElementsByClassName('map-to')[0];

        var map_from = L.map(map_from_el);
        var map_to = L.map(map_to_el);

        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map_from);

        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map_to);


        var json_del_seg = L.geoJSON(JSON.parse(map_from_el.dataset.json));
        json_del_seg.addTo(map_from);

        var json_new_seg = L.geoJSON(JSON.parse(map_to_el.dataset.json));
        json_new_seg.addTo(map_to);

        map_from.fitBounds(json_del_seg.getBounds());
        map_to.fitBounds(json_new_seg.getBounds());
    });
});
