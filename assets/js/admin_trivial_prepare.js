function exec(i) {
    if(i >= reqs.length) {
        let final_msg_ok = document.getElementById("final_msg_ok");
        final_msg_ok.style.display = "block";
    } else {
        let xmlhttp = new XMLHttpRequest();

        xmlhttp.onreadystatechange = function(event) {
            if (this.readyState == XMLHttpRequest.DONE) {   // XMLHttpRequest.DONE == 4
                if (this.status == 200) {
                    let msg = document.getElementById("msg");
                    msg.insertAdjacentHTML('beforeend', reqs[i].msg + ' : ok<br>');
                    exec(i + 1);
                }
                else {
                    msg.insertAdjacentHTML('beforeend', reqs[i].msg + ' : error<br>');
                    let final_msg_error = document.getElementById("final_msg_error");
                    final_msg_error.style.display = "block";
                }
            }
        }

        xmlhttp.open("GET", reqs[i].url, true);
        xmlhttp.send();
    }
};

exec(0);