/* jslint vars: true */
/*jslint indent: 3 */
/* global define */

var cyclab_map = function(L, select2) {

   'use strict';

   var display_entity_min_zoom_level = 15;

   var unselected_color = '#777';
   var colors = {
      'lvl1':'#FF0000', 'lvl2':'#ff9900', 'lvl3':'#ffd700',
      'lvl4':'#94a900', 'lvl5':'#008000'};

   var map;
   var popup;

   var cyclab_map_initialised = false;

   var popup_offset = {15: -42.5 ,16: -45, 17: -47.5, 18: -50, 19: -52.5, 20: -55, 21: -57.5, 22: -60};

   var icon = {}; // A SUPPRIMER

   var layers = {}; // TODO METTRE AU SINGULIER
   var markers = {};
   var icons = {};

   var cities = {};
   var default_city = null;

   var legend = null;
   var button = null;

    /*
     * Set the cities. Each city has a name, a latitude, a longitude and a zoom.
     * It is possible to center the map on each city.
     */
   function setCities(cities_p) {
      cities_p.forEach(function(c) {
         cities[c.name] = c;
      });
   }

   /*
    * If the user did not specify which city he want to view (center the map
    * on it) the map is centered on the default city.
    */
   function setDefaultCity(d_city) {
      default_city = d_city;
   }

   /*
    * Center the map on a given city center. If the map is not alreay
    * initialised (drawn), this function will do it.
    * @param {String} city The name of the city
    * @param {int} zoom_level The zoom level (optional)
    * @return No return
    */
   function goToCity(city, zoom_level) {

      if(!city || (!(city in cities))) {
         if(default_city in cities) {
            city = default_city;
         } else {
            city = null;
         }
      }

      if(!zoom_level) {
         if (city) {
            zoom_level = cities[city].zoom;
         } else {
            zoom_level = 15;
         }
      }

      if(cyclab_map_initialised) {
         if(city) {
            map.setView(cities[city], zoom_level);
         }
      } else {
         if(city) {
            init(cities[city].lat,cities[city].lon,zoom_level);
         } else {
            init(49.8032, 5.2044, zoom_level);
         }
      }

      $('#town_choice_link').colorbox.close();
   }

   /*
    * Center the map on a given longitude/latitude. If the map is not alreay
    * initialised (drawn), this function will do it.
    * @param {float} lat The latitude
    * @param {float} lon The longitude
    * @param {int} zoom_level The zoom level (optional)
    * @return No return
    */
   function gotToLonLatLevel(lat,lon,zoom_level) {
      if(cyclab_map_initialised) {
         if(!zoom_level) {
            zoom_level = map.getZoom();
         }
         map.setView([lat, lon], zoom_level);
      } else {
         if(!zoom_level) {
            zoom_level = display_entity_min_zoom_level;
         }
         init(lat,lon,zoom_level);
      }
   }

   /**
    * Initialization of the cyclab map
    * @param {float} lat The latitude of the center of the map
    * @param {float} lon The longitude of the center of the map
    * @param {integer} zoom The zoom limit (when it is possible to display entities on the map or not)
    */
   function init(lat, lon, min_zoom_level) {
      cyclab_map_initialised = true;

      map = L.map('map', {closePopupOnClick: false}).setView([lat, lon], min_zoom_level);

      L.tileLayer('https://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png', {
         attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://cloudmade.com">CloudMade</a>',
         maxZoom: 22
      }).addTo(map);


      icon.circle = {
         'A' : L.divIcon({
            iconSize: new L.Point(25, 25),
            html: 'A',
            className: 'segments-stop-start-div-icon'
         }),
         'B' : L.divIcon({
            iconSize: new L.Point(25, 25),
            html: 'B',
            className: 'segments-stop-start-div-icon'
         })
      };

      icon.label = {};

   }


   function getColor(feature) {
      if('vote_average' in feature.properties) {
         if (parseFloat(feature.properties.vote_average) < 1.5) {
            return colors.lvl1;
         } else if (parseFloat(feature.properties.vote_average) < 2.5) {
            return colors.lvl2;
         } else if (parseFloat(feature.properties.vote_average) < 3.5) {
            return colors.lvl3;
         } else if (parseFloat(feature.properties.vote_average) < 4.5) {
            return colors.lvl4;
         } else {
            return colors.lvl5;
         }
      } else {
         if (parseFloat(feature.properties.vote_default) < 1.5) {
            return colors.lvl1;
         } else if (parseFloat(feature.properties.vote_default) < 2.5) {
            return colors.lvl2;
         } else if (parseFloat(feature.properties.vote_default) < 3.5) {
            return colors.lvl3;
         } else if (parseFloat(feature.properties.vote_default) < 4.5) {
            return colors.lvl4;
         } else {
            return colors.lvl5;
         }
      }
   }

   /**
    * TODO
    * options is optionnal
    */
   function addGeoJsonLayer(layer_name, data, options) {
      if (typeof options === 'undefined') {
         options = {};
      }

      hideLayer(layer_name);

      layers[layer_name] = L.geoJson(data, options);
      layers[layer_name].addTo(map);
   }

   function returnLayer(layer_name ) // id_elem new_val elem)
   {
      return layers[layer_name];
   }

   /**
    * TODO
    */
   function getMap() {
      return map;
   }

   function closePopup() {
      if(popup && popup._isOpen) {
         popup._close();
         popup = null;
      }

      if (map._popup && map._popup.isOpen()) {
         map._popup._close();
      }
   }

   function addPopup(latlng, popup_content, options) {
      if (typeof options === 'undefined') {
         options = {};
      }

      closePopup();

      if (!('minWidth' in options)) {
         options.minWidth = 180;
      }

      if (!('maxWidth' in options)) {
         options.maxWidth = 180;
      }

      if (!('maxHeight' in options)) {
         options.maxHeight = 300;
      }

      popup = L.popup(options)
         .setLatLng(latlng)
         .setContent(popup_content)
         .openOn(map);
   }

   function movePopup(latlng) {
      if(popup) {
         popup
            .setLatLng(latlng);
      }
   }

   function updatePopupContent(popup_content) {
      if(popup) {
         popup.setContent(popup_content);
      }
   }


   function getPopupOffsetFor(entity) {
      var delta, offset_x, offset_y;
      var node_start_coord, node_end_coord;

      if('type' in entity) {
         if(entity.type == 'segment' || entity.type == 'way') {
            node_start_coord = entity.node_start.geometry.coordinates;
            node_end_coord = entity.node_end.geometry.coordinates;

            delta = (node_start_coord[0] - node_end_coord[0]) / (node_start_coord[1] - node_end_coord[1]);

            //offset_x et offset_y dependent de la taille de la pop-up
            if(delta > 2 || delta < -2) { // ligne horizontale delta_x = 1 et delta_y = 2
               offset_x = 0;
               offset_y = -40;
            } else if (-0.5 < delta && delta < 0.5) { // ligne verticale delta_x = 2 et delta_y = 1
               offset_x = 140;
               offset_y = 100;
            } else if (delta < 0) {
               offset_x = 140;
               offset_y = 0;
            } else {
               offset_x = -140;
               offset_y = 0;
            }

            return L.point(offset_x, offset_y);
         }

         if(entity.type == 'intersection' || entity.type=='node') {
            return L.point(0, popup_offset[map.getZoom()]);
         }
      }

      return L.point(0, 0);
   }

   /**
    * TODO
    */
   function hideLayer(layer_name) {
      if (layer_name in layers) {
         map.removeLayer(layers[layer_name]);
      }
   }


   /**
    *
    * selecteted : fonction qui prend un layer et retourne vrai si on veut le modif
    * change : fonction qui applique le changement sur le layer
    */
   function updateLayer(layer_name, selected, change) {
      if (layer_name in layers) {
         layers[layer_name].getLayers()
            .forEach(function(layer) {
               if(selected(layer)) {
                  change(layer);
               }
            });
      }
   }

   /**
    * TODO
    */
   function showLayer(layer_name) {
      if (layer_name in layers) {
         map.addLayer(layers[layer_name]);
      }
   }

   /* permet d'afficher et de changer de place */
   function showMarker(marker_name, latlng) {
      if(marker_name in markers) {
         markers[marker_name].setLatLng(latlng).update();
      } else {
         if(! (marker_name in icons)) {
            icons[marker_name] = L.divIcon({
               iconSize: new L.Point(25, 25),
               html: marker_name,
               className: 'segments-stop-start-div-icon',
            });
         }
         markers[marker_name] = L.marker(latlng, {icon: icons[marker_name]}).addTo(map);
      }
   }

   function hideMarker(marker_name) {
      if(marker_name in markers) {
         map.removeLayer(markers[marker_name]);
         delete markers[marker_name];
      }
   }

   function getZoom() { // TODO REMOVE
      return map.getZoom();
   }

   function addEvent(event_name, callback) {
      if (map) {
         map.on(event_name, callback);
      } else {
         console.log('Error : map not loaded');
      }
   }

   /*
   * Add legend
   */
   function addLegend() {
       legend = L.control({position: 'bottomleft'});
       legend.onAdd = function (map) {

           var labels = {
               'lvl1': '<b>Dangereux</b>: inadapté aux cyclistes',
               'lvl2': '<b>Déconseillé</b>: pour cyclistes expérimentés',
               'lvl3': '<b>Praticable</b>: peut être désagréable',
               'lvl4': '<b>Conseillé</b>: calme, sécurisant',
               'lvl5': '<b>Très cyclable</b>: pour tout public'
           };

           var div = L.DomUtil.create('div', 'info legend');
           div.setAttribute('id', 'legend');
           div.innerHTML = '<h3>Cyclabilité</h3>';

           var arr = [];
           for (const prop in colors) {
               arr.push(prop);
           }

           var list = document.createElement('ul');
           list.innerHTML = '<ul>';
           for (const key of arr.reverse()) {
               list.innerHTML += '<li><i style="background: ' + colors[key] + ';"></i> ' + labels[key] + '</li>';
           }
           list.innerHTML += '</ul>';

           div.appendChild(list);
           return div;
       };
       addLegendButton('Cacher');
       legend.addTo(map);
    }

    /*
    *  Add toggle button
    */
    function addLegendButton(text) {

       button = L.control({position: 'bottomleft'});
       button.onAdd = function (map) {

          let button = L.DomUtil.create('button', 'toggleLegend');
          button.setAttribute('id', 'toggleLegend');
          button.innerText = text;

          return button;
       };
       button.addTo(map);

       button.getContainer().addEventListener('click', function(e) {
          toggleLegend();
       });
   }

   /*
   *  Toggle legend
   */
   function toggleLegend() {
      if (document.querySelector('#legend') !== null) {
         legend.remove();
         button.remove();
         addLegendButton('Légende');
      } else {
         legend.addTo(map);
         button.remove();
         addLegendButton('Cacher');
      }
   }

   return {
      addLegend: addLegend,
      setCities: setCities,
      setDefaultCity: setDefaultCity,
      display_entity_min_zoom_level: display_entity_min_zoom_level,
      addEvent: addEvent,
      goToCity: goToCity,
      gotToLonLatLevel: gotToLonLatLevel,
      getMap: getMap,
      getColor: getColor,
      closePopup: closePopup,
      addPopup: addPopup,
      movePopup: movePopup,
      updatePopupContent: updatePopupContent,
      addGeoJsonLayer: addGeoJsonLayer,
      getPopupOffsetFor: getPopupOffsetFor, //todo améliorer la position de la popup
      hideLayer: hideLayer,
      showLayer: showLayer,
      updateLayer: updateLayer,
      showMarker: showMarker,
      hideMarker: hideMarker,
      unselected_color: unselected_color,
      getZoom: getZoom,
      returnLayer: returnLayer,
   };
};

export default cyclab_map;
