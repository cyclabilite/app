/*jshint esversion: 6*/

import cyclab_map from './cyclab_map.js';
import user from './user.js';
import entity from './entity.js';
import geo from './geo.js';

const select2 = require('select2');
const leaflet = require('leaflet');
const routie = require('routie-2');
const colorbox = require('jquery-colorbox');
const mustache = require('mustache');

const app = function() {
    function init(cities, default_city, current_user) {
        let cyclab_map_m = cyclab_map(leaflet, select2);

        let cities_p = {};
        cities = JSON.parse(cities);

        cities.forEach(function(c) {
          cities_p[c.name] = [c.lon, c.lat, c.zoom];
        });

        cyclab_map_m.setCities(cities);

        if(default_city) {
            cyclab_map_m.setDefaultCity(default_city);
        }


        if(default_city) {
            cyclab_map_m.goToCity(default_city);
        } else {
            cyclab_map_m.goToCity(null);
        }


        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                cyclab_map_m.gotToLonLatLevel(position.coords.latitude, position.coords.longitude, 17);
            });
        }

        var user_m = user($);

        current_user = JSON.parse(current_user);

        if(current_user) {
            console.log('Set Cur User');
            user_m.setCurrentUser(current_user);
        }

        var geo_m = geo(leaflet);

        cyclab_map_m.addLegend();

        var entity_m = entity($, leaflet, mustache, cyclab_map_m, user_m, geo_m, select2);
        entity_m.init();

        $(document).ready(function() {
                routie({
                   'map/:lat/:lon/:zoom?': cyclab_map_m.gotToLonLatLevel,
                   ':city/:zoom?': cyclab_map_m.goToCity,
                   '': function() {
                      cyclab_map_m.goToCity(null);
                   }
                });

                $('#town_choice_link').colorbox({
                   inline:true,
                   width:'400px',
                   height:'400px',
               });

                user_m.init();

        });
    }
    return {init: init};
}();

document.addEventListener('DOMContentLoaded', function() {
    var data = document.querySelector('.js-data');

    if(data) {
        var cities = data.dataset.cities;
        var current_city = data.dataset.defaultCity;
        var user = data.dataset.user;
        app.init(cities, current_city, user);
    } else {
        console.log('Pas de data');
    }
});
